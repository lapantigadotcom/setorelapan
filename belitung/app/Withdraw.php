<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model {

	protected $table = 'tr_withdraw';
	protected $guarded = ['id'];
	protected $fillable = [
		'ms_user_id',
		'month',
		'year',
		'referral_value',
		'monthly_value',
		'status',
		'withdraw_date',
		'batch'
	];
    public $timestamps = true;
    public function user()
    {
    	return $this->belongsTo('\App\User','ms_user_id','id');
    }
}