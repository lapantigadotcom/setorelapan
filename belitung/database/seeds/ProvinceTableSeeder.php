<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Province;

class ProvinceTableSeeder extends Seeder {

    public function run()
    {
        // Province::truncate();

        Province::create([
            'code' => 'JT',
            'name' => 'Jawa Timur',
        ]);
    }

}