<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CategoryArticle;

class CategoryArticleTableSeeder extends Seeder {

    public function run()
    {
        CategoryArticle::truncate();

        CategoryArticle::create([
            'title' => 'Default',
            'description' => 'Default Article',
            'locked' => '1'
        ]);

    }

}