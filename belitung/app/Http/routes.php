<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'token_admin'], function() 
{
	Route::get('/admin', array('as' => 'ki-admin.auth','uses' =>  'AdminController@getLogin'));
});
Route::post('/admin',array('as' => 'ki-admin.auth.login','uses' => 'AdminController@postLogin'));

Route::get('ki-admin/notif-message/customer/json',array('as' => 'ki-admin.notif-message.customer.getReceiverJson','uses' => 'NotifMessageCustomerController@getReceiverJson'));
Route::get('ki-admin/notif-message/employee/json',array('as' => 'ki-admin.notif-message.employee.getReceiverJson','uses' => 'NotifMessageEmployeeController@getReceiverJson'));
Route::get('ki-admin/notif-email/receiver/json',array('as' => 'ki-admin.notif-email.getReceiverJson','uses' => 'NotifEmailController@getReceiverJson'));
Route::get('ki-admin/notif-sms/receiver/json',array('as' => 'ki-admin.notif-sms.getReceiverJson','uses' => 'NotifSMSController@getReceiverJson'));
Route::group(['middleware' => 'admin'], function()
{
	Route::get('/admin/dashboard',array('as' => 'ki-admin.dashboard', 'uses' => 'AdminController@getDashboard'));
	Route::get('/admin/logout',array('as' => 'ki-admin.logout', 'uses' => 'AdminController@getLogout'));
	Route::get('/admin/changepassword',array('as' => 'ki-admin.getChangePassword', 'uses' => 'AdminController@getChangePassword'));
	Route::post('/admin/changepassword',array('as' => 'ki-admin.postChangePassword', 'uses' => 'AdminController@postChangePassword'));
	Route::get('/admin/reset-database', array('as' => 'ki-admin.getResetDatabase', 'uses' => 'AdminController@getTruncateDatabase'));
	Route::post('/admin/reset-database', array('as' => 'ki-admin.postResetDatabase', 'uses' => 'AdminController@postTruncateDatabase'));
	Route::get('/admin/refresh-bonus',array('as' => 'ki-admin.refresh-bonus', 'uses' => 'AdminController@refreshBonus'));

	Route::get('ki-admin/attribute/{id}/delete',array('as'=> 'ki-admin.attribute.delete','uses' => 'AttributeController@destroy'));
	Route::resource('ki-admin/attribute', 'AttributeController');

	Route::get('ki-admin/brand/{id}/delete',array('as'=> 'ki-admin.brand.delete','uses' => 'BrandController@destroy'));
	Route::resource('ki-admin/brand', 'BrandController');

	Route::get('ki-admin/categoryproduct/{id}/delete',array('as'=> 'ki-admin.categoryproduct.delete','uses' => 'CategoryProductController@destroy'));
	Route::resource('ki-admin/categoryproduct', 'CategoryProductController');

	Route::get('ki-admin/product/{id}/delete',array('as'=> 'ki-admin.product.delete','uses' => 'ProductController@destroy'));
	Route::get('ki-admin/product/{id}/downloadfile',array('as'=> 'ki-admin.product.downloadfile','uses' => 'ProductController@downloadFile'));
	Route::get('ki-admin/product/{id}/deletefile',array('as'=> 'ki-admin.product.deletefile','uses' => 'ProductController@deleteFile'));
	Route::resource('ki-admin/product', 'ProductController');

	Route::get('ki-admin/stock-product/{id}/delete',array('as'=> 'ki-admin.stock-product.delete','uses' => 'StockProductController@destroy'));
	Route::resource('ki-admin/stock-product', 'StockProductController');	

	Route::get('ki-admin/productimage/{id}/delete',array('as'=> 'ki-admin.productimage.delete','uses' => 'ProductImageController@destroy'));
	Route::resource('ki-admin/productimage', 'ProductImageController');

	Route::get('ki-admin/role/admin/{id}/edit',array('as' => 'ki-admin.role.admin.edit', 'uses' => 'RoleController@editAdmin'));
	Route::put('ki-admin/role/admin/{id}/edit',array('as'  => 'ki-admin.role.admin.update', 'uses' => 'RoleController@updateRoleAdmin'));
	Route::resource('ki-admin/role', 'RoleController');

	Route::get('ki-admin/aktif',array('as'=> 'ki-admin.user.aktif','uses' => 'UserController@aktif'));
		Route::get('ki-admin/silver',array('as'=> 'ki-admin.user.silver','uses' => 'UserController@silver'));
		Route::get('ki-admin/gold',array('as'=> 'ki-admin.user.gold','uses' => 'UserController@gold'));

	Route::get('ki-admin/user/{id}/delete',array('as'=> 'ki-admin.user.delete','uses' => 'UserController@destroy'));
	Route::get('ki-admin/user/{id}/reset',array('as'=> 'ki-admin.user.reset','uses' => 'UserController@reset'));
	Route::resource('ki-admin/user', 'UserController');

	Route::get('ki-admin/detailattribute/{id}/delete',array('as'=> 'ki-admin.detailattribute.delete','uses' => 'DetailAttributeController@destroy'));
	Route::resource('ki-admin/detailattribute', 'DetailAttributeController');

	Route::resource('ki-admin/general','GeneralController');
	Route::get('ki-admin/registrationfee',array('as' => 'ki-admin.registrationfee.index','uses' => 'PointController@registrationFee'));
	Route::get('ki-admin/registrationfee/{id}/edit',array('as' => 'ki-admin.registrationfee.edit','uses' => 'PointController@registrationFeeEdit'));
	Route::put('ki-admin/registrationfee/{id}/edit', array('as' => 'ki-admin.registrationfee.update','uses' => 'PointController@registrationFeeUpdate'));
	Route::post('ki-admin/registrationfee/create', array('as' => 'ki-admin.registrationfee.store','uses' => 'PointController@registrationFeeStore'));
	
	Route::resource('ki-admin/point','PointController');
	Route::resource('ki-admin/referral','ReferralController');

	Route::get('ki-admin/notif-sms/{id}/delete',array('as'=> 'ki-admin.notif-sms.delete','uses' => 'NotifSMSController@destroy'));
	Route::resource('ki-admin/notif-sms', 'NotifSMSController');

	Route::get('ki-admin/notif-email/{id}/delete',array('as'=> 'ki-admin.notif-email.delete','uses' => 'NotifEmailController@destroy'));
	Route::resource('ki-admin/notif-email', 'NotifEmailController');

	Route::get('ki-admin/notification/{id}/delete',array('as'=> 'ki-admin.notification.delete','uses' => 'NotificationController@destroy'));
	Route::resource('ki-admin/notification', 'NotificationController');

	Route::get('ki-admin/chatmobile/{id}/delete',array('as'=> 'ki-admin.chatmobile.delete','uses' => 'ChatMobileController@destroy'));
	Route::post('ki-admin/storeadmin',array('as'=> 'ki-admin.chatmobile.storeadmin','uses' => 'ChatMobileController@storeadmin'));
	Route::resource('ki-admin/chatmobile', 'ChatMobileController');

	Route::get('ki-admin/voicebox/{id}/delete',array('as'=> 'ki-admin.voicebox.delete','uses' => 'VoiceBoxController@destroy'));
	Route::resource('ki-admin/voicebox', 'VoiceBoxController');

	Route::get('ki-admin/notif-message/inbox',array('as'=> 'ki-admin.notif-message.inbox','uses' => 'NotifMessageCustomerController@inbox'));
	Route::get('ki-admin/notif-message/outbox',array('as'=> 'ki-admin.notif-message.outbox','uses' => 'NotifMessageCustomerController@outbox'));
	Route::get('ki-admin/notif-message/customer/delete',array('as'=> 'ki-admin.notif-message.customer.delete','uses' => 'NotifMessageCustomerController@destroy'));
	Route::resource('ki-admin/notif-message/customer', 'NotifMessageCustomerController');
	Route::get('ki-admin/notif-message/employee/delete',array('as'=> 'ki-admin.notif-message.employee.delete','uses' => 'NotifMessageEmployeeController@destroy'));
	Route::resource('ki-admin/notif-message/employee', 'NotifMessageEmployeeController');

	Route::get('ki-admin/contact-us/{id}/delete',array('as'=> 'ki-admin.contact-us.delete','uses' => 'ContactUsController@destroy'));
	Route::resource('ki-admin/contact-us', 'ContactUsController');

	Route::get('ki-admin/report/user', array('as' => 'ki-admin.report.user', 'uses' => 'ReportController@user'));
	Route::get('ki-admin/report/afiliasi', array('as' => 'ki-admin.report.afiliasi', 'uses' => 'ReportController@afiliasi'));
	Route::get('ki-admin/report/sales', array('as' => 'ki-admin.report.sales', 'uses' => 'ReportController@sales'));
	Route::get('ki-admin/report/bonus', array('as' => 'ki-admin.report.bonus', 'uses' => 'ReportController@bonus'));

	Route::get('ki-admin/image/{id}/delete',array('as'=> 'ki-admin.image.delete','uses' => 'ImageController@destroy'));
	Route::resource('ki-admin/image', 'ImageController');

	Route::get('ki-admin/image/{id}/delete',array('as'=> 'ki-admin.image.delete','uses' => 'ImageController@destroy'));
	Route::resource('ki-admin/image', 'ImageController');

	Route::get('ki-admin/category-article/{id}/delete',array('as'=> 'ki-admin.category-article.delete','uses' => 'CategoryArticleController@destroy'));
	Route::resource('ki-admin/category-article', 'CategoryArticleController');

	Route::get('ki-admin/article/{id}/delete',array('as'=> 'ki-admin.article.delete','uses' => 'ArticleController@destroy'));
	Route::resource('ki-admin/article', 'ArticleController');

	Route::get('ki-admin/payment/{payment}/delete',array('as'=> 'ki-admin.payment.delete','uses' => 'PaymentController@destroy'));
	Route::resource('ki-admin/payment','PaymentController');

	Route::get('ki-admin/shippingoffice/{id}/change',array('as'=> 'ki-admin.shippingoffice.change','uses' => 'ShippingOfficeController@change'));
	Route::get('ki-admin/shippingoffice/{shippingoffice}/delete',array('as'=> 'ki-admin.shippingoffice.delete','uses' => 'ShippingOfficeController@destroy'));
	Route::resource('ki-admin/shippingoffice','ShippingOfficeController');

	Route::get('ki-admin/order/{id}/delete',array('as'=> 'ki-admin.order.delete','uses' => 'OrderController@destroy'));
	Route::get('ki-admin/order/{id}/change',array('as'=> 'ki-admin.order.change','uses' => 'OrderController@change'));
	Route::resource('ki-admin/order','OrderController');

	Route::get('ki-admin/transfer/{id}/delete',array('as'=> 'ki-admin.transfer.delete','uses' => 'TransferController@destroy'));
	Route::get('ki-admin/transfer/{id}/change',array('as'=> 'ki-admin.transfer.change','uses' => 'TransferController@change'));
	Route::resource('ki-admin/transfer','TransferController');

	Route::get('ki-admin/permission/{id}/delete',array('as'=> 'ki-admin.permission.delete','uses' => 'ArticleController@destroy'));
	Route::resource('ki-admin/permission','PermissionController');
	Route::get('ki-admin/adminmenu/{id}/delete',array('as'=> 'ki-admin.adminmenu.delete','uses' => 'AdminMenuController@destroy'));
	Route::resource('ki-admin/adminmenu','AdminMenuController');
	Route::get('ki-admin/employee/{id}/delete',array('as'=> 'ki-admin.employee.delete','uses' => 'EmployeeController@destroy'));
	Route::get('ki-admin/employee/{id}/reset',array('as'=> 'ki-admin.employee.reset','uses' => 'EmployeeController@reset'));
	Route::resource('ki-admin/employee','EmployeeController');

	Route::get('ki-admin/notif-registrationfee/{id}/delete',array('as'=> 'ki-admin.notif-registrationfee.delete','uses' => 'RegistrationFeeController@destroy'));
	Route::get('ki-admin/notif-registrationfee/{id}/change',array('as'=> 'ki-admin.notif-registrationfee.change','uses' => 'RegistrationFeeController@change'));
	Route::resource('ki-admin/notif-registrationfee','RegistrationFeeController');
	
	Route::get('ki-admin/notif-withdraw/{id}/delete',array('as'=> 'ki-admin.notif-withdraw.delete','uses' => 'WithdrawController@destroy'));
	Route::get('ki-admin/notif-withdraw/{id}/change',array('as'=> 'ki-admin.notif-withdraw.change','uses' => 'WithdrawController@change'));
	Route::resource('ki-admin/notif-withdraw','WithdrawController');
	
	Route::get('ki-admin/notif-review/{id}/delete',array('as'=> 'ki-admin.notif-review.delete','uses' => 'ReviewController@destroy'));
	Route::get('ki-admin/notif-review/{id}/change',array('as'=> 'ki-admin.notif-review.change','uses' => 'ReviewController@change'));
	Route::resource('ki-admin/notif-review','ReviewController');

	Route::get('ki-admin/ppn',array('as' => 'ki-admin.ppn.index','uses' => 'PPNController@index'));
});
Route::get('user/role/json',array('as' => 'ki-admin.role.getjson','uses' => 'RoleController@getjson'));
Route::get('chat/getchat/json',array('as' => 'ki-admin.chat.getjson','uses' => 'ChatMobileController@getjson'));

Route::group(['middleware' => 'maintenance'], function()
{	
	Route::get('/', array('as' => 'home', 'uses' => 'PageController@index'));
	Route::get('/products/{category?}', array('as'=> 'product', 'uses' => 'PageController@products'));
	Route::get('/products/detail/{code}/{name?}', array('as' => 'user.product.detail', 'uses' => 'PageController@detailproduct'));
	Route::get('/page/{page}','PageController@page');
	Route::get('/search',array('as' => 'search', 'uses' => 'PageController@search'));
	Route::get('/api/products/getjson', array('as'=> 'product.getjson', 'uses' => 'ProductController@getjson'));

	Route::get('/partner-area',array('as' => 'partner-area','uses' => 'PageController@partner'));
	Route::get('/partner-area/{code}/show',array('as' => 'partner-show','uses' => 'PageController@partnerShow'));

	Route::get('/articles/{code}',array('as' => 'articles', 'uses' => 'PageController@article'));
	Route::get('/contact-us', array('as' => 'contact-us', 'uses' => 'PageController@contact'));
	Route::post('/contact-us', array('as' => 'contact-us.store', 'uses' => 'PageController@contactPost'));
	// User / Guest Route
	Route::get('login',array('as' => 'login', 'uses' => 'UserController@getLogin'));
	Route::post('login',array('as' => 'login.store', 'uses' => 'UserController@postLogin'));

	Route::get('register',array('as' => 'register', 'uses' => 'UserController@getRegister'));
	Route::post('register',array('as' => 'register.store', 'uses' => 'UserController@postRegister'));
	Route::post('front-register',array('as' => 'register.front.store', 'uses' => 'UserController@postRegisterDepan'));
	Route::get('user/activate/{token}',array('as' => 'register.activate', 'uses' => 'UserController@getActivate'));
	Route::group(['middleware' => 'customer'], function()
	{
		Route::get('logout',array('as' => 'user.logout', 'uses' => 'UserController@logout'));
		Route::get('user/dashboard',array('as' => 'user.dashboard', 'uses' => 'UserController@dashboard'));
		Route::get('user/profil',array('as' => 'user.profil','uses' => 'UserController@dashboard')) ;

		Route::get('user/verified',array('as' => 'user.verified', 'uses' => 'UserController@verified'));
		Route::put('user/verified',array('as' => 'user.verified.update', 'uses' => 'UserController@verifiedUpdate'));
		Route::get('user/registration/fee',array('as' => 'user.registrationfee','uses' => 'RegistrationFeeController@userCreate'));
		Route::post('user/registration/fee',array('as' => 'user.registrationfee.store','uses' => 'RegistrationFeeController@userStore'));

		Route::get('user/profile/edit',array('as' => 'user.profile.edit', 'uses' => 'UserController@profileEdit'));
		Route::put('user/profile/edit',array('as' => 'user.profile.update', 'uses' => 'UserController@profileUpdate'));
		Route::get('user/status', array('as' => 'user.status', 'uses' => 'LogActivityController@indexUser'));
		Route::get('user/affiliate', array('as' => 'user.affiliate', 'uses' => 'UserController@affiliateUser'));
		Route::get('user/withdraw', array('as' => 'user.withdraw', 'uses' => 'WithdrawController@indexUser'));
		Route::get('user/withdraw/get', array('as' => 'user.withdraw.get', 'uses' => 'WithdrawController@getUser'));

		Route::get('user/cart',array('as' => 'user.cart', 'uses' => 'CartController@indexUser'));
		Route::post('user/cart/{id}', array('as' => 'user.cart.store', 'uses' => 'CartController@store'));
		Route::post('user/cart/{id}/change-total', array('as' => 'user.cart.change-total', 'uses' => 'CartController@changeTotal'));
		Route::get('user/cart/{id}/delete', array('as' => 'user.cart.delete', 'uses' => 'CartController@deleteDetail'));
		Route::get('user/cart/{id}/reseller', array('as' => 'user.cart.reseller', 'uses' => 'CartController@checkReseller'));
		Route::get('user/cart/checkout/agent', array('as' => 'user.cart.checkout.agent', 'uses' => 'CartController@checkoutAgent'));
		Route::get('user/cart/checkout/overseas', array('as' => 'user.cart.checkout.overseas', 'uses' => 'CartController@checkoutOverseas'));
		Route::get('user/report',array('as' => 'user.report', 'uses' => 'ReportController@userIndex'));
		Route::get('user/{id}/confirm-perwakilan', array('as' => 'user.confirm-perwakilan', 'uses' => 'NotifMessageCustomerController@perwakilan'));
		Route::post('user/{id}/confirm-perwakilan', array('as' => 'user.confirm-perwakilan.store', 'uses' => 'NotifMessageCustomerController@perwakilanStore'));

		Route::get('api/user/shipphing/courier', array('as' => 'user.shipping.courier', 'uses' => 'ShippingController@getService'));
		Route::get('api/user/shipphing/service', array('as' => 'user.shipping.service', 'uses' => 'ShippingController@setService'));

		

		Route::get('user/checkout/api/normal',array('as' => 'user.checkout.api.normal', 'uses' => 'CartController@apiNormal'));
		Route::get('user/checkout/normal',array('as' => 'user.checkout.normal', 'uses' => 'CartController@checkoutNormal'));

		Route::get('user/transfer/{id}/order', array('as' => 'user.transfer.order', 'uses' => 'TransferController@create'));
		Route::post('user/transfer/{id}/order', array('as' => 'user.transfer.order.store', 'uses' => 'TransferController@store'));

		Route::post('user/review/{id}/store', array('as' => 'user.review.store', 'uses'=> 'ReviewController@store' ));

		Route::get('user/change-password', array('as' => 'user.change.pass', 'uses' => 'UserController@changePassword'));
		Route::post('user/change-password', array('as' => 'user.change.pass.store', 'uses' => 'UserController@submitChangePassword'));

		Route::get('account_bank/store',array('as' => 'user.account_bank.store', 'uses' => 'UserController@addAccountBank'));
		Route::get('account_bank/{id}/delete',array('as' => 'user.account_bank.delete', 'uses' => 'UserController@destroyAccountBank'));

	});
	//Sementara
	Route::get('user/{id}/invoice',array('as' => 'user.invoice', 'uses' => 'ReportController@userInvoice'));
	Route::get('user/{id}/jalan',array('as' => 'user.jalan', 'uses' => 'ReportController@userJalan'));
	Route::get('blogs', array('as' => 'blogs', 'uses' => 'PageController@blogs' ));
	Route::get('blogs/{id}/detail', array('as' => 'blogs.show', 'uses' => 'PageController@blogShow'));

	// tracking JNE
	Route::get('tracking', array('as' => 'tracking', 'uses' => 'ShippingController@trakingJNE'));
	Route::post('tracking', array('as' => 'tracking.store', 'uses' => 'ShippingController@posTraking'));
	
});

Route::get('/maintenance', array('as' => 'maintenance', 'uses' => 'PageController@maintenance'));
Route::controllers([
	'password' => 'PasswordController'
]);

Route::post('/m/login',array('as' => 'mob.login.store', 'uses' => 'MobileController@login'));
Route::post('/m/register',array('as' => 'mob.register.store', 'uses' => 'MobileController@register'));
Route::post('/m/updateprofile', 'MobileController@updateprofile');
Route::post('/m/updateprofiletwo', 'MobileController@updateprofiletwo');

Route::get('/m/init', 'MobileController@initial');
Route::get('/m/init/city', 'MobileController@city');
Route::post('/m/gcmstore', array('as' => 'mob.gcm.store', 'uses' => 'MobileController@gcmstore'));
Route::get('/m/gcmstore', 'MobileController@gcmstore');
Route::get('/m/product/hot', 'MobileController@hotproduct');
// Route::get('/m/transactions/get/{email}', 'MobileController@transaction');
Route::post('/m/transactions/get', 'MobileController@transaction');
Route::post('/m/voicebox/store', 'VoiceBoxController@store');
Route::post('/m/voicebox/storetwo', 'VoiceBoxController@storetwo');
Route::post('/m/chat/store',  array('as' => 'mob.chat.store', 'uses' => 'ChatMobileController@store'));
Route::post('/m/chat/storetwo',  array('as' => 'mob.chat.storetwo', 'uses' => 'ChatMobileController@storetwo'));