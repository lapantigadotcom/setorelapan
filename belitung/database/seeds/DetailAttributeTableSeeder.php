<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\DetailAttribute;

class DetailAttributeTableSeeder extends Seeder {

    public function run()
    {
        DetailAttribute::truncate();

        DetailAttribute::create([
            'ms_attribute_id' => '1',
            'name' => '36'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '1',
            'name' => '37'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '1',
            'name' => '38'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '1',
            'name' => '39'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '1',
            'name' => '40'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Black'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'White'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Red'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Green'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Blue'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Yellow'
        ]);

        DetailAttribute::create([
            'ms_attribute_id' => '2',
            'name' => 'Purple'
        ]);

    }

}