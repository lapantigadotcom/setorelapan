<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Point;

class PointTableSeeder extends Seeder {

    public function run()
    {
        Point::truncate();

        Point::create([
            'ms_role_id' => '5',
            'point' => '3',
            'registrationfee' => '10000000',
            'referral' => '150000'
        ]);
        Point::create([
            'ms_role_id' => '6',
            'point' => '5',
            'registrationfee' => '15000000',
            'referral' => '200000'
        ]);
        Point::create([
            'ms_role_id' => '7',
            'point' => '7',
            'registrationfee' => '25000000',
            'referral' => '400000'
        ]);

    }

}