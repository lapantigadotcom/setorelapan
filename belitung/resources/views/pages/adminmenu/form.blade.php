@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if(isset($data['content']))
                        {!! Form::model($data['content'],array('route' => ['ki-admin.adminmenu.update',$data['content']->id], 'method' => 'PUT')) !!}
                        @else
                        {!! Form::open(array('route' => 'ki-admin.adminmenu.store', 'method' => 'POST')) !!}
                        @endif
                            <div class="box-body">
                                {!! BootstrapForm::text('name') !!}
                                {!! BootstrapForm::text('route') !!}
                                {!! BootstrapForm::text('icon') !!}

                                <div class="form-group">
                                    <?php array_unshift($data['adminmenu'], 'Tidak ada'); ?>
                                    {!! Form::label('parent_id','Parent') !!}
                                    {!! Form::select('parent_id',$data['adminmenu'],null, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('enabled','Enabled') !!}
                                    {!! Form::select('enabled',['0' => 'No', '1' => 'Yes'],null, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection