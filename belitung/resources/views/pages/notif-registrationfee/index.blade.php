@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Registration Fee</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">
          @include('pages.notif-registrationfee.menu')
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Lists</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Customer</th>
                    <th>Role</th>
                    <th>Pembayaran Ke</th>
                    <th>Nominal</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td></td>
                      <td><a href="{!! route('ki-admin.notif-registrationfee.show', [$row->id]) !!}">{!! $row->user->name !!}</a></td>
                      <td>{!! $row->user->roles->first()->name !!}</td>
                      <td>{!! $row->payment->name.' '.$row->payment->account_number.' - a/n '.$row->payment->account_name !!}</td>
                      <td>{!! $row->nominal !!}</td>
                      <td>
                        {!! $row->status=='1'?"<span class='label label-primary'>Approved</span>":($row->status=='2'?"<span class='label label-danger'>Unnaproved</span>":"<span class='label label-warning'>Pending</span>") !!}</td>
                      <td>
                      
                      @if(Auth::user()->hasAccess('ki-admin.notif-registrationfee.change') || Auth::user()->hasAccess('ki-admin.notif-registrationfee.delete'))
                        <div class="btn-group">
                          <button class="btn btn-danger" type="button">Change</button>
                          <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul role="menu" class="dropdown-menu">
                          @if(Auth::user()->hasAccess('ki-admin.notif-registrationfee.change'))
                            <li><a href="{!! route('ki-admin.notif-registrationfee.change',[$row->id]).'?state=1' !!}">Approve</a></li>
                            <li><a href="{!! route('ki-admin.notif-registrationfee.change',[$row->id]).'?state=2' !!}">Unapprove</a></li>
                          @endif
                            <li class="divider"></li>
                            @if(Auth::user()->hasAccess('ki-admin.notif-registrationfee.delete'))
                            <li><a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.notif-registrationfee.delete',[$row->id]) !!}">Delete</a></li>
                            @endif
                          </ul>
                        </div>
                      @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Customer</th>
                    <th>Role</th>
                    <th>Pembayaran Ke</th>
                    <th>Nominal</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection