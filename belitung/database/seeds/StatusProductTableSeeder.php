<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\StatusProduct;

class StatusProductTableSeeder extends Seeder {

    public function run()
    {
        StatusProduct::truncate();

        StatusProduct::create([
            'name' => 'In stock'
        ]);

        StatusProduct::create([
            'name' => 'Out of stock'
        ]);
    }

}