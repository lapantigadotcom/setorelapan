<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Role;
use App\User;
use Auth;

class UserVerifiedRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$role = Role::find(Request::input('ms_role_id'));
		switch($role->require_file)
	    {
	        case '0':
	        {
	            return [
	                
	            ];
	        }
	        case '1':
	        {
	            return [
	                'ktp'	=> 'required|image',
					'siup'	=> 'image',
					'tdp'	=> 'image',
					'npwp'	=> 'image',
					'akte'	=> 'image',
					'company'	=> 'required',
					'ms_role_id' => 'required',
					'ms_bank_id' => 'required',
					'account_bank' => 'required'
	            ];
	        }
	        default:break;
	    }
	}
}
