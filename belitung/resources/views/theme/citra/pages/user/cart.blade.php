@extends('theme.citra.app')
<title>Keiskei Indonesia | Keranjang Belanja</title>

@section('content')
@include('theme.citra.scripts.price-product')
 

					<center><div class="se-pre-con">
  <div style="left: 0px; margin: -100px auto auto; position: absolute; width: 100%; top: 40%;">
				<img src="{!! asset('theme/keiskei/img/logo.png') !!}" class="img-responsive">
    Mohon ditunggu...</div></div></center>

<section id="cart_items">
						@include('errors.session')
	<div class="container">
						 
			<div class="register-req">
				<p>Harap cek terlebih dahulu pembelian Anda, pastikan nama, alamat dan detail pengiriman Anda sudah benar</p>
			</div>
				 

				<!-- TABEL BARANG -->
					<div class="shopper-informations">
							<div class="row">
								<div class="col-sm-3">
								<div class="shopper-info">
									<p><i class="fa fa-check-circle-o"></i>&nbsp;Detail Pesanan</p>
								</div>
								</div>
							</div>
					</div>

				<div class="row">
					<div class="table-responsive cart_info">
										<table class="table table-condensed">
												<thead>
													<tr class="cart_menu">
														<td class="image">Item</td>
														<td class="description"></td>
														<td class="price">Harga</td>
														<td class="quantity">Jumlah</td>
														<td class="total">Total</td>
														<td></td>
													</tr>
												</thead>
												 
										<tbody>
										<?php 
										$no=1;
										$total = 0;
										?>
										@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
										@if($data['content']->detailOrders->count() > 0)
										@foreach($data['content']->detailOrders as $row)
										<?php
										$total_t = priceDisplay($data['default_role'], $row->product->prices); 
										?>
										<tr>
											<td class="cart_product" style="width:190px">
															@if($row->product->productImages()->count() > 0)
															<img src="{!! asset($row->product->productImages->first()->image) !!}" width="100px" alt="{!! $row->product->productImages->first()->caption !!}">
															@endif
											</td>

											<td class="cart_description" style="font-size:14px">
											{!! $row->product->title !!}
											<p>@if($row->detailAttributeProductDetailOrders()->count() > 0)
											@foreach($row->detailAttributeProductDetailOrders as $val)
											<span>{!! $val->detailAttributeProduct->detailAttribute->attribute->name !!}: 
											<b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b></span>
											@endforeach
											@endif</p>
											</td>

											<td class="cart_price" >
											<p style="font-size:14px">{!! money_format('%(#10n', $total_t) !!}</p>
											</td>
														
											<td class="cart_quantity" >
												<div class="cart_quantity_button" style="max-width:80px" >
													{!! Form::open(array('route' => ['user.cart.change-total', $row->id], 'method' => 'POST')) !!}
													{!! Form::selectRange('total', 1, 15, $row->total, ['class' => 'form-control','onchange' => 'this.form.submit()','data-detail-id' => $row->id]) !!}
													{!! Form::close() !!}
													<?php 
														$total_t = $total_t*$row->total;
														$total += $total_t;?>
												</div>
											</td>

											<td class="cart_total">
											<p class="cart_total_price" style="font-size:14px"> Rp.{!! money_format('%(#10n', $total_t) !!}</p>
											</td>
														
											<td class="cart_delete">
												<a style="background:orange" target="_blank" href="{!! route('user.product.detail',$row->product->code, $row->product->title) !!}"><i class="fa fa-eye"></i></a>
												<a style="background:red" class="cart_quantity_delete" href="{!! route('user.cart.delete',[$row->id]) !!}"><i class="fa fa-times"></i></a>
											</td>@endforeach
												
											@else
										</tr>
													
										<tr>
											<td colspan="4">&nbsp;</td>
											<td colspan="2">
											<table class="table table-condensed total-result">		
												<tr>
													<td>Kurir</td>
													<td><b>{!! $data['content']->courier->name !!}</b> - {!! $data['content']->service_courier !!}
													</td>	
												
									
												</tr>
												<tr class="shipping-cost">
													<td>Ongkos Kirim</td>
													<td>{!! money_format('%(#10n', $data['content']->shipping_price) !!}
													</td>										
												</tr>
												<tr>
													<td>Total (IDR)</td>
													<td><span>{!! money_format('%(#10n', $total+$data['content']->shipping_price) !!} </span></td>
												</tr>
										</tr>
												
										<tr>
											<td colspan="4">Tidak ada data.</td>
										</tr>
												@endif
												@else
										<tr>
											<td><center><br><span style="color:red">Tidak ada data. Silahkan beli produk terlebih dahulu</span></center></td>
										</tr>
												@endif
											</table>
											</td>
													</tr>
												</tbody>
											</table>
					</div>
				</div>





						<!-- TABEL Data -->

			<div class="row">
					<div class="shopper-informations">
						@if(Auth::user()->roles->first()->level!='5')
										 <div class="col-sm-3">
											<div class="shopper-info">
												<p>Alamat</p>
												<form>
													@if(Auth::user()->roles->first()->level==5 && Auth::user()->roles->first()->locked != '1')
													<p>
												
													</p>
													@else
													<input id="disabledInput" placeholder="{!! Auth::user()->address !!}" type="text" disabled>
													<input id="disabledInput" placeholder="{!! Auth::user()->city->name !!}" type="text" disabled>
													<input id="disabledInput" placeholder="{!! Auth::user()->city->province->name !!}" type="text" disabled>
													<input id="disabledInput" placeholder="{!! Auth::user()->telephone !!}" type="text" disabled>
												</form>
												@endif
					 						</div>
					 					</div>
					</div>


				<div class="col-sm-8 clearfix">
					<div class="bill-to">
							<p>Kurir</p>
							<div class="form-one">
										@if(Auth::user()->roles->first()->level!='5')
										@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
		 							 
						<div class="form-group">
							{!! Form::select('ms_courier_id',$data['courier'],!empty($data['content']->ms_courier_id)?$data['content']->ms_courier_id:null, array('class' => 'form', 'id' => 'ms_courier_id','onchange' => 'changeCourier(this)')) !!}
							
							<?php 
							$data['service_courier'] = array();
							
							if($data['content']->service_courier != '')
								array_push($data['service_courier'], $data['content']->service_courier);
							else
								array_push($data['service_courier'], 'Tidak ada');
							?>
								<br><br>
							{!! Form::select('service_courier',$data['service_courier'], null, array('class' => 'form','id' => 'service_courier','onchange' => 'changeService(this)')) !!}
 

						</div>
						
			 				</div>

							<div class="form-two">
							  
							<span class='shipping-price'>
							 
							</span>
						 </div>		 
					</div>
				</div>
		</div>



				@if(Auth::user()->roles->first()->default != '1' || (Auth::user()->roles->first()->default=='1' && $data['agen']->count() == 0))
				<div class="sidebar">
					<div class="sidebar-head">
						Pilih Pembayaran
					</div>
					<div class="sidebar-body">
						<div class="col-md-12">
							<div class="form-group">
								<?php 
								$arrPayment = array();
								foreach ($data['payment'] as $row) {
									$arrPayment[$row->id] = $row->name.' '.$row->account_number.' - '.$row->account_name;
								}
								?>
 								{!! Form::select('ms_payment_id',$arrPayment, null, array('class' => 'form-control')) !!}
							</div>
						</div>
					</div>
				</div>
				@else


												<!-- #tabel perwakilan-->
					<div class="shopper-informations">
							<div class="row">
								<div class="col-sm-3">
								<div class="shopper-info">
									<p><i class="fa fa-check-circle-o"></i>&nbsp;Pilih Keiskei Partners</p>
								</div>
								</div>
							</div>
					</div>
								
								<div class="row">
									<div class="table-responsive cart_info">
										<table class="table table-condensed">
											<thead>
												<tr class="cart_menu">
													<td class="image">Nama</td>
													<td class="description">Alamat</td>
													<td class="price">Kota</td>
													<td class="total">Pilih</td>

													<td></td>
												</tr>
											</thead>
											<tbody>
												<tr>
											@foreach($data['agen'] as $row)
												
																		
													<td class="cart_product">
														{!! $row->name !!}
													</td>
													<td class="cart_description" style="font-size:14px">
														{!! $row->address !!}
													</td>
													<td class="cart_price" style="font-size:14px">
													{!! $row->city->name !!} - {!! $row->city->province->name !!}
													</td>
													 
													
													<td class="total">
																		<input type="checkbox" name="ms_reseller_id" id="reseller{!! $row->id !!}" value="{{ $row->id }}" class="reseller" onchange="checkReseller(this)" data-order-id='{!! $data['content']->id !!}' @if($data['content']->ms_reseller_id == $row->id) checked='true' @endif>
													</td>
												</tr>@endforeach
												
											</tbody>
										</table>
									</div>
								</div>@endif
												@endif
												@endif
												@endif
								<!-- #tabel perwakilan-->
				  
				
				@if(Auth::user()->roles->first()->level!='5')
				@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
				@if(Auth::user()->roles->first()->default != '1' || (Auth::user()->roles->first()->default=='1' && $data['agen']->count() == 0))
				<div class="row">
					<div class="col-md-2">
						<a class="btn btn-keiskei-orange" href="{!! route('product') !!}">
							<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
						</a>
					</div>
					<div class="col-md-8 text-right">
						<div class="col-md-6">
							<b>Ongkos Pengiriman</b><br>
							<span class='shipping-price'>
								{!! money_format('%(#10n', $data['content']->shipping_price) !!}
								<p>
									<b>{!! $data['content']->courier->name !!}</b> - {!! $data['content']->service_courier !!}
								</p>
							</span>
						</div>
						<div class="col-md-6">
							<b>Total Pembayaran</b>
							<p id="total-pembayaran">
								{!! money_format('%(#10n', $total*10/100+$total+$data['content']->shipping_price)  !!}
							</p>
						</div>

					</div>
					<div class="col-md-2 text-right">
						<a class="btn btn-keiskei" onclick="checkoutNormal()" href="javascript:void(0)">
							Checkout &nbsp; <i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div>
				@else

				<!-- #tabel TOTAL --><div class="shopper-informations">
							<div class="row">
								<div class="col-sm-3">
								<div class="shopper-info">
									<p><i class="fa fa-check-circle-o"></i>&nbsp;GRAND TOTAL</p>
								</div>
								</div>
							</div>
					</div>
								
								<div class="row">
									<div class="table-responsive cart_info">
										<table class="table table-condensed">
											<thead>
												<tr class="cart_menu">
													<td class="image">Ongkos Pengiriman</td>
													<td class="description">Kurir</td>
													<td class="price">Layanan</td>
													<td class="total">Total</td>

													<td></td>
												</tr>
											</thead>
											<tbody>
												<tr>
											 		<td class="cart_product">
														{!! money_format('%(#10n', $data['content']->shipping_price) !!}
													</td>
													<td class="cart_description" style="font-size:14px">
														{!! $data['content']->courier->name !!}
													</td>
													<td class="cart_price" style="font-size:14px">
													{!! $data['content']->service_courier !!}
													</td>
													 													
													<td class="total">
														{!! money_format('%(#10n', $total+$data['content']->shipping_price) !!}	
													</td>
												</tr> 
												
											</tbody>
										</table>
									</div>
								</div>

								<div class="row">
					<div class="col-md-2">
						<a class="btn btn-primary" href="{!! route('product') !!}">
							<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
						</a>
					</div>
					<div class="col-md-8 text-center">
						<h2>Rp. {!! money_format('%(#10n', $total+$data['content']->shipping_price) !!}	</h2>
					</div>
					<div class="col-md-2 text-right">
						<a class="btn btn-primary" href="javascript:void(0)" onclick="checkReseller2()">
							Checkout &nbsp; <i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div><br><br><br>


				@endif
				@endif
				@else
				@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)

								<!-- #Buton Bawah -->
								<div class="row">
										<div class="col-md-2">
											<a class="btn btn-primary" href="{!! route('product') !!}">
												<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
											</a>
										</div>
										
										<div class="col-md-8 text-right">
											<center> <span id="total-pembayaran" class="btn btn-primary" style="background:#F89406">
										<i class="fa fa-check-circle-o"></i>&nbsp;TOTAL PEMBAYARAN : {!! money_format('%(#10n', $total) !!}</span></center>	
											
										</div>

										<div class="col-md-2 text-right">
											<a class="btn btn-primary" href="{!! route('user.cart.checkout.overseas') !!}">
												Checkout &nbsp; <i class="fa fa-chevron-right"></i>
											</a>
										</div>@endif
										@endif
										 
								</div>									<!-- #Buton Bawah -->
			
</section>





 		@endsection

		@section('custom-footer')

		<script type="text/javascript">
			$(document).ready(function() {
				$("#product-slider").owlCarousel({
					items : 5,
					lazyLoad : true,
					navigation : true,
					pagination : false,
					navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
				});
			});

			function changeCourier(x)
			{
				var courier_id = $(x).val();
				$('#service_courier').empty();
				$.ajax(
				{
					url:"{{ route('user.shipping.courier') }}",
					type: "GET",
					data:{
						courier_id : courier_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						shippingPrice = [];
						var html = '<option selected disabled> Pilih Layanan </option>';
						$.each(data, function(key, value){
							html += "<option value='" + value[0] + "'>" + value[0] + "</option>";
							shippingPrice[value[0]] = value[1];
						});
						$('#service_courier').append(html);
					}
				});
			}

			@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
			var shippingPrice = new Array();
			var total = {!! $total+$data['content']->shipping_price !!};
			var shippingPriceOld = {!! $data['content']->shipping_price !!};
			function checkReseller(x){
				var ms_reseller_id = $(x).val();
				var order_id = x.getAttribute("data-order-id");
				$.ajax(
				{
					url:"{{ route('user.cart.reseller') }}",
					type: "GET",
					data:{
						ms_reseller_id : ms_reseller_id,
						order_id : order_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						$('.reseller').prop("checked", false); 
						x.checked = true;
					}
				});
			}
			function checkReseller2(){
				var x =  $("input[name='ms_reseller_id']:checked").val();
				console.log(x);
				if(typeof x === "undefined")
				{
					alert('Pilih Perwakilan terlebih dahulu');
				}else{
					// alert(' dahulu');
					window.location.replace("{!! route('user.cart.checkout.agent') !!}");
				}
			}
			function changeService(x)
			{
				var service = $(x).val();
				var price = shippingPrice[service];
				var courier_id = $('#ms_courier_id').val();
				console.log(service);
				console.log(price);
				$.ajax(
				{
					url:"{{ route('user.shipping.service') }}",
					type: "GET",
					data:{
						service : service,
						price : price,
						courier_id : courier_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						var total_t = total-shippingPriceOld;
						var shippingPriceOld_t = price;
						total_t += price;
						var html = shippingPriceOld_t + "<p><b>" + data.courier.name + "</b> - " + data.service_courier + "</p>";
						$('#total-pembayaran').empty();
						$('#total-pembayaran').append(total_t)
						$('.shipping-price').empty();
						$('.shipping-price').append(html)
					}
				});	
			}
			function checkoutNormal()
			{
				var order_id = {!! $data['content']->id !!};
				var payment_id = $('#ms_payment_id').val();
				$.ajax(
				{
					url:"{{ route('user.checkout.api.normal') }}",
					type: "GET",
					data:{
						order_id : order_id,
						payment_id : payment_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						window.location.replace("{!! route('user.checkout.normal') !!}");
					}else{
						alert('error');
						return false;
					}
				});	
			}
			@endif
		</script>
		<script type="text/javascript">

		//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});

	</script>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(../img/preloader.gif) center no-repeat ;
	background-color: rgba(255, 255, 255, 0.7);
}
</style>

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

		@endsection

		@section('custom-head')

		@endsection