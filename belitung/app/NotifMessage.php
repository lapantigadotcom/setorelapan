<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class NotifMessage extends Model {

	protected $table = 'tr_notif_messages';
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}

	public function recipient()
	{
		return $this->belongsTo('App\User', 'ms_user_recipient_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('App\NotifMessage', 'parent_id', 'id');
	}

	public function child()
	{
		return $this->hasMany('App\NotifMessage', 'parent_id', 'id');
	}

}
