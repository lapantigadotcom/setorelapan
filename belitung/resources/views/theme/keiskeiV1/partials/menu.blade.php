<style type="text/css">
	.goog-te-banner-frame.skiptranslate {
		display: none !important;
	} 
	body {
		top: 0px !important; 
	}

	.goog-te-gadget-simple
	{
		display: none !important;
	} 

	.goog-tooltip {
		display: none !important;
	}
	.goog-tooltip:hover {
		display: none !important;
	}
	.goog-text-highlight {
		background-color: transparent !important;
		border: none !important; 
		box-shadow: none !important;
	}

</style>


<div class="col-md-12">
	<div class="row header-nav" id="top-head">
		<div class="col-md-offset-3 col-md-2">
			<ul class="top-social">
				<li>
					<i class="fa fa-phone-square"></i>
					{!! $general->telephone !!}
				</li>
			</ul>
		</div>
		<div class="col-md-3" style="width:30%;padding-left:3px;padding-right:3px; font-size:11px">
			<ul class="top-auth">
				@if(Auth::check() == true)
				<li>
					<i class="fa fa-dashboard fa-lg"></i>&nbsp;
					<a href="{!! route('user.dashboard') !!}"> Beranda</a>
				</li>
				<li>
					<i class="fa fa-power-off fa-lg"></i>&nbsp;
					<a href="{!! route('user.logout') !!}"> Keluar</a>
				</li>

				@else
				<li>
					<i class="fa fa-lock fa-lg"></i>&nbsp;
					<a href="javascript:void(0)" id="login-trigger"> Masuk</a>
					<div id="login-content">
						{!! BootstrapForm::open(array('store' => 'login.store','method' => 'POST','route' => 'login.store')) !!}
						<fieldset id="inputs">
							<input id="username" type="text" name="username" placeholder="Username" required>   
							<input id="password" type="password" name="password" placeholder="Password" required>
						</fieldset>
						<fieldset id="actions">
							{!! Form::submit('masuk', array('class' => 'btn btn-sm btn-keiskei tombol') ) !!}
							<label>

								<p style="font-size:12px; ">	<input type="checkbox" checked="checked" name="remember_me" value="true">&nbsp;Ingat Saya</p>

							</label>
						</fieldset>

						{!! BootstrapForm::close() !!}

					</div>  
				</li>
				<li>
					<i class="fa fa-user fa-lg"></i>&nbsp;
					<a href="{!! route('register') !!}"> Daftar</a>
				</li>

				<li>
					<a href="{{ url('/password/email') }}">  <i class="fa fa-key"></i>&nbsp; 
						Lupa Password?</p></a>
					</li>
					@endif
				</ul>
			</div>
			<div class="col-md-3">
				<ul class="top-social">
					<li><a href="https://www.facebook.com/KeiskeiIndonesia?ref=hl" target="_blank"><i  title="FB: keiskeiashitaba" class="fa fa-facebook-square fa-lg" style="color:#3b5998"></i></a></li>
					<li><a href="https://twitter.com/keiskeiashitaba" target="_blank"><i  title="@keiskeiashitaba" class="fa fa-twitter-square fa-lg" style="color:#00aced"></i></a></li>
					<li><a href="https://plus.google.com/101701632456013203883/posts" target="_blank"><i  title="keiskeiashitaba" class="fa fa-google-plus fa-lg" style="color:#dd4b39"></i></a></li>
					<li><a href="https://id.linkedin.com/pub/keiskei-indonesia/b7/1/b90" target="_blank"><i  title="keiskei-indonesia" class="fa fa-linkedin fa-lg" style="color:#007bb6"></i></a></li>
					<li><a href="https://instagram.com/keiskeiindonesia/" target="_blank"><i title="keiskei_indonesia" class="fa fa-instagram fa-lg" style="color:#517fa4"></i></a></li>


					<!-- Use CSS to replace link text with flag icons -->
					<ul class="translation-links">
						<li><a href="#" class="notranslate" data-lang="Indonesian">
							<img src="{!! asset('theme/keiskei/img/id.png') !!}">
						</a></li>
						<li><a href="#" class="English" data-lang="English">
							<img src="{!! asset('theme/keiskei/img/en.png') !!}">
						</a></li>
						<li><a href="#" class="Japanese" data-lang="Japanese">
							<img src="{!! asset('theme/keiskei/img/jp.png') !!}">
						</a></li>
					</ul>

					<!-- Code provided by Google -->
					<div id="google_translate_element"></div>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
						}
					</script>
					<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

					<!-- Flag click handler -->
					<script type="text/javascript">
						$('.translation-links a').click(function() {
							var lang = $(this).data('lang');
							var $frame = $('.goog-te-menu-frame:first');
							if (!$frame.size()) {
								alert("Error: Could not find Google translate frame.");
								return false;
							}
							$frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
							return false;
						});
					</script>
				</ul>

			</div>
		</div>
	</div>
	<div class="col-md-offset-1 col-md-10" id="slide-nav-head">
		<nav class="navbar"  id="slide-nav">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle navbar-keiskei collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>

					</button>
					<a class="navbar-brand visible-lg visible-md" href="{!! URL::to('/') !!}">
						<img src="{!! asset('theme/keiskei/img/logo.png') !!}" class="img-responsive hidden-xs main-logo">
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="slide-active"  id="slidemenu">
					<ul class="nav navbar-nav navbar-right main-navbar-keiskei" style="background:none">
						<li class="{!! Request::is('/')?'active':'' !!}"><a href="{!! URL::to('/') !!}"><i class="fa fa-home" style="font-size:18px"></i></a></li>
						@if(Auth::check() == true)
						<li id="second-login" style="display:none;"><a href="{!! route('user.dashboard') !!}"><i class="fa fa-dashboard fa-lg"></i>&nbsp;Beranda</a></li>
						<li id="second-register" style="display:none;"><a href="{!! route('user.logout') !!}"><i class="fa fa-power-off fa-lg"></i>&nbsp;Logout</a></li>
						@else
						
						<li id="second-login" style="display:none;"><a href="{!! route('login') !!}"><i class="fa fa-lock fa-lg"></i>&nbsp;Login</a></li>
						<li id="second-register" style="display:none;"><a href="{!! route('register') !!}"><i class="fa fa-user fa-lg"></i>&nbsp;Daftar</a></li>
						@endif
						@if(isset($articles[0]['id']))<li><a href="{!! route('articles',[$articles[0]['id'].'-'.$articles[0]['title']]) !!}">Tentang Kami</a></li> @endif
						<li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}">Produk</a></li>
						<li><a href="{!! route('partner-area') !!}">Partner Area</a></li>
						@if(isset($articles[2]['id']))<li><a href="{!! route('articles',[$articles[2]['id'].'-'.$articles[2]['title']]) !!}">Bisnis K-Partner</a></li>@endif

						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Panduan <b class="caret"></b></a>
							<ul class="dropdown-menu" style="font-size:12px;color:#50C92C">
								@if(isset($articles[11]['id']))<li><a href="{!! route('articles',[$articles[11]['id'].'-'.$articles[11]['title']]) !!}">Pendaftaran</a></li>@endif
								<li class="divider"></li>
								@if(isset($articles[9]['id']))<li><a href="{!! route('articles',[$articles[9]['id'].'-'.$articles[9]['title']]) !!}">Pemesanan</a></li>@endif
								<li class="divider"></li>
								@if(isset($articles[10]['id']))<li><a href="{!! route('articles',[$articles[10]['id'].'-'.$articles[10]['title']]) !!}">Pembayaran</a></li>@endif
								<li class="divider"></li>
								@if(isset($articles[12]['id']))<li><a href="{!! route('articles',[$articles[12]['id'].'-'.$articles[12]['title']]) !!}">Konfirmasi</a></li>@endif
								<li class="divider"></li>
								@if(isset($articles[13]['id']))<li><a href="{!! route('articles',[$articles[13]['id'].'-'.$articles[13]['title']]) !!}">Withdraw</a></li>@endif

							</ul>
						</li>


						<li><a href="{!! route('blogs') !!}">Blog</a></li>
						<li><a href="{!! route('contact-us') !!}">Hubungi Kami</a></li>
						<li id="chart"><a href="{!! route('user.cart') !!}"><i title="Keranjang Belanja" class="fa fa-shopping-cart"></i></a></li>
						<li id="second-chart" style="display:none;"><a href="{!! route('user.cart') !!}">Keranjang Belanja</a></li>
						@if(isset($articles[4]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[4]['id'].'-'.$articles[4]['title']]) !!}">Kebijakan Privasi</a></li>@endif
						@if(isset($articles[5]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[5]['id'].'-'.$articles[5]['title']]) !!}">FAQ</a></li>@endif
						@if(isset($articles[6]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[6]['id'].'-'.$articles[6]['title']]) !!}">Keanggotaan</a></li>@endif
						@if(isset($articles[7]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[7]['id'].'-'.$articles[7]['title']]) !!}">Peta Situs</a></li>@endif
						@if(isset($articles[3]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[3]['id'].'-'.$articles[3]['title']]) !!}">Dukungan</a></li>@endif
						@if(isset($articles[8]['id']))<li class="footer-menu-in-head" style="display:none"><a href="{!! route('articles',[$articles[8]['id'].'-'.$articles[8]['title']]) !!}">Karir</a></li>@endif
					</ul>

				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>