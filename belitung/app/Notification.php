<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Notification extends Model {

	protected $table = 'tr_notifications';
	protected $guarded = ['id'];
	protected $fillable = ['title', 'description', 'photo','ms_role_id'];
	public $timestamps = true;
	public function recipient()
	{
		return $this->belongsTo('App\Role', 'ms_role_id', 'id');
	}


}
