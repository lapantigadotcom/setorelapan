<?php

use App\Adminmenu;
use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class AuthTableSeeder extends Seeder {

    public function run()
    {
        Permission::truncate();
        $sp = \App\Role::where('code','SP')->first();
        $mg = \App\Role::where('code','MG')->first();
        $of = \App\Role::where('code','OF')->first();
        $data_t = Permission::create([
            'route' => 'ki-admin.dashboard',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.logout',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        // attribute
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.attribute.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        // brand
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.brand.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        
        // categoryproduct
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.categoryproduct.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        // product
        $data_t = Permission::create([
            'route' => 'ki-admin.product.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.product.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        
        // productimage
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.productimage.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);

        // user
        $data_t = Permission::create([
            'route' => 'ki-admin.user.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.user.reset',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);

        // detailattribute
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.detailattribute.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);

        // general
        $data_t = Permission::create([
            'route' => 'ki-admin.general.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.general.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.general.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.general.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.general.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.general.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        // registrationfee
        $data_t = Permission::create([
            'route' => 'ki-admin.registrationfee.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.registrationfee.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.registrationfee.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.registrationfee.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        
        // point
        $data_t = Permission::create([
            'route' => 'ki-admin.point.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.point.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.point.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        // referral
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.referral.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.referral.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        // notif-sms
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-sms.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        // notif-email
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-email.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        // image
        $data_t = Permission::create([
            'route' => 'ki-admin.image.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.image.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.image.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);

        // payment
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.payment.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.payment.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        // permission
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.permission.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.permission.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        // adminmenu
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.adminmenu.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        // employee
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.employee.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.employee.reset',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        
        // category-article
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.category-article.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        // article
        $data_t = Permission::create([
            'route' => 'ki-admin.article.index',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.create',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.store',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.show',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        $of->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.article.update',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.delete',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.article.destroy',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        $mg->permission()->attach($data_t->id);
        unset($data_t);

        // registrationfee
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.index',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.create',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.store',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.show',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.update',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.delete',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.destroy',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.notif-registrationfee.change',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);


        // transfer
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.index',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.create',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.store',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.edit',
            'enabled' => '1'
        ]);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.show',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);

        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.update',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.delete',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.destroy',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);
        $data_t = Permission::create([
            'route' => 'ki-admin.transfer.change',
            'enabled' => '1'
        ]);
        $mg->permission()->attach($data_t->id);
        $sp->permission()->attach($data_t->id);
        unset($data_t);




        Adminmenu::truncate();
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Dashboard',
            'route' => '',
            'order_item' => '1',
            'icon' => 'dashboard'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Catalog',
            'route' => '',
            'order_item' => '2',
            'icon' => 'tags'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Customers',
            'route' => '',
            'order_item' => '3',
            'icon' => 'users'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Sales',
            'route' => '',
            'order_item' => '4',
            'icon' => 'shopping-cart'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Report',
            'route' => '',
            'order_item' => '5',
            'icon' => 'bar-chart-o'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Notification',
            'route' => '',
            'order_item' => '6',
            'icon' => 'bell'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Blog',
            'route' => '',
            'order_item' => '7',
            'icon' => 'wrench'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'Preferences',
            'route' => '',
            'order_item' => '8',
            'icon' => 'wrench'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        unset($data_t);

        $data_t = Adminmenu::create([
            'parent_id' => '0',
            'name' => 'User Management',
            'route' => '',
            'order_item' => '9',
            'icon' => 'wrench'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        unset($data_t);



        $data_t = Adminmenu::create([
            'parent_id' => '2',
            'name' => 'Products',
            'route' => 'ki-admin.product.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '2',
            'name' => 'Categories',
            'route' => 'ki-admin.categoryproduct.index',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '2',
            'name' => 'Brand',
            'route' => 'ki-admin.brand.index',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '2',
            'name' => 'Product Attributes',
            'route' => 'ki-admin.attribute.index',
            'order_item' => '4',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '3',
            'name' => 'Customers',
            'route' => 'ki-admin.user.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '4',
            'name' => 'Order',
            'route' => 'ki-admin.order.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '5',
            'name' => 'Total Penjualan',
            'route' => '',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '5',
            'name' => 'Bonus Member',
            'route' => '',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '5',
            'name' => 'Afiliasi',
            'route' => '',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'SMS Gateway',
            'route' => 'ki-admin.notif-sms.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Email',
            'route' => 'ki-admin.notif-email.index',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Konfirmasi Transfer',
            'route' => 'ki-admin.transfer.index',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Konfirmasi Registration Fee',
            'route' => 'ki-admin.notif-registrationfee.index',
            'order_item' => '4',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Message Inbox/Outbox',
            'route' => 'ki-admin.notif-message.inbox',
            'order_item' => '5',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Message Customer',
            'route' => 'ki-admin.notif-message.customer.create',
            'order_item' => '6',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Message Employee',
            'route' => 'ki-admin.notif-message.employee.create',
            'order_item' => '7',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '6',
            'name' => 'Contact Us',
            'route' => 'ki-admin.contact-us.index',
            'order_item' => '8',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '8',
            'name' => 'General',
            'route' => 'ki-admin.general.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '8',
            'name' => 'Point',
            'route' => 'ki-admin.point.index',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '8',
            'name' => 'Registration Fee',
            'route' => 'ki-admin.registrationfee.index',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '8',
            'name' => 'Referral',
            'route' => 'ki-admin.referral.index',
            'order_item' => '4',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '8',
            'name' => 'Payment',
            'route' => 'ki-admin.payment.index',
            'order_item' => '5',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);

        $data_t = Adminmenu::create([
            'parent_id' => '9',
            'name' => 'Permission',
            'route' => 'ki-admin.permission.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        unset($data_t);

        $data_t = Adminmenu::create([
            'parent_id' => '9',
            'name' => 'Admin Menu',
            'route' => 'ki-admin.adminmenu.index',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '9',
            'name' => 'Role',
            'route' => 'ki-admin.role.index',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '9',
            'name' => 'Employee',
            'route' => 'ki-admin.employee.index',
            'order_item' => '4',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);

        
        unset($data_t);

        $data_t = Adminmenu::create([
            'parent_id' => '7',
            'name' => 'Categories',
            'route' => 'ki-admin.category-article.index',
            'order_item' => '1',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '7',
            'name' => 'Articles',
            'route' => 'ki-admin.article.index',
            'order_item' => '2',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
        $data_t = Adminmenu::create([
            'parent_id' => '7',
            'name' => 'Image',
            'route' => 'ki-admin.image.index',
            'order_item' => '3',
            'icon' => 'angle-double-right'
        ]);
        $sp->adminmenu()->attach($data_t->id);
        $mg->adminmenu()->attach($data_t->id);
        $of->adminmenu()->attach($data_t->id);
        unset($data_t);
    }
}