<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Session;
use Hash;
use File;
use DB;
use Image;


class MobileController extends BasicController {
	public function initial(){
		$data = array();
		$data['RESP'] = "SCSINIT";
		$data['MESSAGE'] = 'sukses inisialisasi.';
		return json_encode($data);
	}
	public function city(){
		$data = array();
		$data["CITY"] = \App\City::orderBy('id', 'asc')->select('id','name', 'ms_province_id')->get()->toArray();
		return json_encode($data);
	}
	public function login(Request $request)
	{
		$username = $request->input('username');
		$password = $request->input('password');
		$gcm_id = $request->input('gcm_id');
		$remember_me = false;
		if($request->input('remember_me'))
		{
			$remember_me = true;
		};

		if(Auth::attempt(['username' => $username, 'password' => $password,'enabled' => 1],$remember_me) || Auth::attempt(['username' => $username, 'password' => $password,'enabled' => '0'],$remember_me))
		{
			$user = \App\User::whereHas('orders', function($q)
			{
				$q->where('ms_status_order_id', '4')->orWhere('ms_status_order_id', '8');

			})->find(Auth::id());
			$user->gcm_id = $gcm_id;
			$user->save();
			$gcm = \App\GCM::where('gcm_id', $gcm_id)->first();
			if(empty($gcm)){
				$gcm = new \App\GCM;
				$gcm->gcm_id = $gcm_id;
				$gcm->is_anonymous = "0";
				$gcm->ms_user_id = Auth::id();
				$gcm->save();
			}else{
				$gcm->gcm_id = $gcm_id;
				$gcm->is_anonymous = "0";
				$gcm->ms_user_id = Auth::id();
				$gcm->save();
			}
			// $data = array();
			// $total_buy = $user->orders()->count();
			// $data['RESP'] = "SCSLOGIN";
			// $data['MESSAGE'] = 'Anda berhasil masuk ke sistem.';
			// $data['DATA'] = $user->toArray();
			// $data['TOTAL_BUY'] = $total_buy;
			// return json_encode($data);

			$total = $this->bonus($user->email);
			$user_arr = $user->toArray();
			$user_arr['bonus'] = $total;
			$data = array();
			$data['RESP'] = "SCSLOGIN";
			$data['MESSAGE'] = 'Anda berhasil masuk ke sistem.';
			$total_buy = $user->orders()->count();
			$data['TOTAL_BUY'] = $total_buy;
			$data['DATA'] = $user_arr;
			return json_encode($data);
		}

		$data = array();
		$data['RESP'] = "FLDINIT";
		$data['MESSAGE'] = 'Kombinasi username dan password tidak cocok.';
		return json_encode($data);
	}
	public function register(Request $request)
	{		
		
		$temp_user = \App\User::where('username', $request->input('username'))->first();
		
		if(!empty($temp_user))
		{
			$data = array();
			$data['RESP'] = "FLDRGSTR";
			$data['MESSAGE'] = 'Username telah digunakan, silakan gunakan username lain.';
			return json_encode($data);
		}
		$temp_user = \App\User::where('email', $request->input('email'))->first();
		
		if(!empty($temp_user))
		{
			$data = array();
			$data['RESP'] = "FLDRGSTR";
			$data['MESSAGE'] = 'Email telah digunakan, silakan gunakan email lain.';
			return json_encode($data);
		}

		$role = \App\Role::where('default','1')->first();
		$data = new \App\User;
		$data->email = $request->input('email');
		$data->username = $request->input('username');
		$data->mobile = $request->input('handphone');
		$data->ms_country_id = '1';
		$data->ms_city_id = $request->input('ms_city_id');
		$data->enabled = '0';
		$data->first_login = '0';
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data->roles()->attach($role->id);
		$data->code = $data->roles->first()->code.'00'.$data->id;

		
		$data->save();
		$data['password'] = $password;
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah melakukan registrasi.'
			]);
		ob_start();
		$this->sendEmail('1',$data->email,$data);
		
		if(true)
		{
			$this->sendSMS('2',$data->mobile,$data);
		}
		ob_end_clean();

		$data = array();
		$data['RESP'] = "SCSRGSTR";
		$data['MESSAGE'] = 'Pendaftaran berhasil, silakan login.';
		return json_encode($data);
	}
	public function updateprofile(Request $request)
	{		
		$data= \App\User::find($request->input('id'));

		$email = $request->input('email');
		if($data->email != $email){
			$temp_user = \App\User::where('email', $request->input('email'))->first();
			
			if(!empty($temp_user))
			{
				$data = array();
				$data['RESP'] = "FLDSUPDTPRFL";
				$data['MESSAGE'] = 'Email telah digunakan, silakan gunakan email lain.';
				return json_encode($data);
			}
		}

		$data->name = $request->input('name');		
		$data->email = $request->input('email');
		$data->mobile = $request->input('handphone');
		$data->pinbb = $request->input('pinbb');
		$data->website = $request->input('website');
		$data->instagram = $request->input('instagram');
		$data->facebook = $request->input('facebook');
		$data->save();
		$user = $data;
		$total = $this->bonus($user->email);
		$user_arr = $user->toArray();
		$user_arr['bonus'] = $total;
		$data = array();
		$data['RESP'] = "SCSUPDTPRFL";
		$data['MESSAGE'] = 'Profilberhasil diperbarui.';
		$total_buy = $user->orders()->count();
		$data['TOTAL_BUY'] = $total_buy;
		$data['DATA'] = $user_arr;
		return json_encode($data);
	}
	public function updateprofiletwo(Request $request)
	{

		$data= \App\User::find($request->input('id'));
		

		$email = $request->input('email');
		if($data->email != $email){
			$temp_user = \App\User::where('email', $request->input('email'))->first();
			
			if(!empty($temp_user))
			{
				$data = array();
				$data['RESP'] = "FLDSUPDTPRFL";
				$data['MESSAGE'] = 'Email telah digunakan, silakan gunakan email lain.';
				return json_encode($data);
			}
		}

		$data->name = $request->input('name');		
		$data->email = $request->input('email');
		$data->mobile = $request->input('handphone');
		$data->pinbb = $request->input('pinbb');
		$data->website = $request->input('website');
		$data->instagram = $request->input('instagram');
		$data->facebook = $request->input('facebook');
		$data->save();


		if ($request->hasFile('photo'))
		{
			if(File::exists('./data/user/photo/'.$data->photo))
				File::delete('./data/user/photo/'.$data->photo);

			$file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "_user_".date('Ymd_Hsi').'.'.$extension;
			$file->move('data/user/photo/',$fileName);
			$img = Image::make('data/user/photo/'.$fileName);
			$img->save('data/user/photo/'.$fileName, 60);
			$data->photo = $fileName;		
			$data->save();
		}
		

		// $photo_flag = $request->input('photo_flag');

		// if($photo_flag == '1')
		// {
		// 	if(File::exists('./data/user/photo/'.$data->photo))
		// 		File::delete('./data/user/photo/'.$data->photo);
		// 	$photo = $request->input('photo');
		// 	$photo = urldecode($photo);
		// 	$decoded=base64_decode($photo);
		// 	$filename = $data->code."_photo_".date('Ymd_Hsi').'.JPEG';
		// 	file_put_contents('./data/user/photo/'.$filename,$decoded);
		// 	$data->photo = $filename;		
		// 	$data->save();
		// }
		$user = $data;
		$total = $this->bonus($user->email);
		
		$user_arr = $user->toArray();
		$user_arr['bonus'] = $total;
		$data = array();
		$data['RESP'] = "SCSUPDTPRFL";
		$data['MESSAGE'] = 'Profilberhasil diperbarui.';
		$total_buy = $user->orders()->count();
		$data['TOTAL_BUY'] = $total_buy;
		$data['DATA'] = $user_arr;
		return json_encode($data);
	}

	public function gcmstore(Request $request){
		
		$gcm_id = $request->input('gcm_id');
		$anonymous = $request->input('is_anonymous');
		$user_id = $request->input('user_id');

		$data = \App\GCM::where('gcm_id', $gcm_id)->first();
		if(empty($data)){
			$data = new \App\GCM;
			$data->gcm_id = $gcm_id;
			$data->ms_user_id = $user_id;
			$data->is_anonymous = $anonymous;
			$data->save();
		}else{
			$data->gcm_id = $gcm_id;
			$data->ms_user_id = $user_id;
			$data->is_anonymous = $anonymous;
			$data->save();
		}
		$data = array();
		$data['RESP'] = "SCSGCM";
		$data['MESSAGE'] = 'GCM berhasil disimpan.';
		return json_encode($data);
	}
	public function hotproduct(){
		$arr_hot = DB::select("SELECT p.id FROM ms_products as p, tr_detail_orders as do WHERE p.id = do.ms_product_id GROUP BY do.ms_product_id ORDER BY total DESC LIMIT 3");
		$arr_hotie = array();
		foreach ($arr_hot as $row) {
			array_push($arr_hotie, $row->id);
		}
		$data = \App\Product::with('productImages')->whereIn('id', $arr_hotie)->get();
		$product = array();
		foreach ($data as $row) {
			$temp = array();
			$temp['id'] = $row->id;
			$temp['code'] = $row->code;
			$temp['title'] = $row->title;
			$temp['image'] = $row->productImages()->first()->image;
			$temp['description'] = preg_replace('/\s+/', ' ', trim(strip_tags($row->description)));
			array_push($product, $temp);
		}

		$json = array();
		$json['RESP'] = "SCSPRDCT";
		$json['MESSAGE'] = 'GCM berhasil disimpan.';
		$json['DATA'] = $product;

		return json_encode($json);
	}

	public function transaction(Request $request)
	{
		$email = $request->get('email');
		$user = \App\User::where('email', $email)->first();
		if(empty($user))
		{
			$json = array();
			$json['RESP'] = "FLDTRNSCTN";
			$json['MESSAGE'] = 'Permintaan gagal dilayani.';
			return json_encode($json);
		}
		
		$order = \App\Order::
		with(
			'detailOrders',
			'detailOrders.product',
			'detailOrders.product.prices'
			)
		->orWhere(function($query) use($user)
		{
			$query->where('ms_user_id', $user->id)
			->where('ms_status_order_id', '<>', '1');
		})
		->orWhere(function($query) use($user)
		{
			$query->where('ms_reseller_id', $user->id)
			->where('ms_status_order_id', '<>', '1');
		})
		->orderBy('created_at','desc')
		->get();
		$default_role = \App\Role::where('default','1')->first();
		$data = array();
		foreach($order as $row){
			$total = 0; 
			$total_t = 0;
			foreach($row->detailOrders as $val)
			{
				$total_t = $this->priceDisplay($user, $default_role, $val->product->prices); 
				if(!empty($val->product->discount))
				{
					$discount_t = ($total_t/100)*intval($val->product->discount);
					$total_t = $total_t - $discount_t;
				}
				$total_t = $total_t*$val->total;
				$total += $total_t;
			}
			$total = $total + $row->shipping_price+ ($total*10/100);
			$temp = array();
			$temp['id'] = $row->id;
			$temp['code'] = $row->code;
			$temp['ship'] = $row->service_courier;
			$temp['total'] = $total;
			$temp['tgl'] = $row->updated_at;
			array_push($data, $temp);
		}
		$json = array();
		$json['RESP'] = "SCSTRNSCTN";
		$json['DATA'] = $data;
		$json['MESSAGE'] = 'Permintaan gagal dilayani.';
		return json_encode($json);
	}

	function priceDisplay($user, $data, $prices)
	{
		if($user->roles->first()->level != '5' ){
			$default= $prices->filter(function($v) use($data) {
				if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
					return true;
				}
			});
			if($user->roles->first()->default!='1' && $user->roles->first()->locked!='1')
			{
				$price_t= $prices->filter(function($v) use($data) {
					if($v->ms_role_id==$user->roles->first()->id && $v->ms_currency_id=='1'){
						return true;
					}
				});
				return $price_t->first()->price;
			}
			else
				return $default->first()->price;
		}else{

			$default= $prices->filter(function($v) use($data) {
				if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
					return true;
				}
			});
			return $default->first()->price;
		}
	}
	function bonus($email){
		// $username = $request->get('username');
		$user = \App\User::where('email', $email)->first();
		// if(empty($user))
		// {
		// 	$json = array();
		// 	$json['RESP'] = "FLDTRNSCTN";
		// 	$json['MESSAGE'] = 'Permintaan gagal dilayani.';
		// 	return json_encode($json);
		// }
		$this->refreshWithdraw($user->id);
		$temp_t= \App\Withdraw::where('ms_user_id',$user->id)->where('status','<>','1')->where('status','<>','3')->sum('referral_value');
		$temp_t2= \App\Withdraw::where('ms_user_id',$user->id)->where('status','<>','1')->where('status','<>','3')->sum('monthly_value');
		$total = intval($temp_t)+intval($temp_t2);
		return $total;
	}
}
