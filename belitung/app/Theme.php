<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ThemeMenu;

class Theme extends Model {

	protected $table = 'ms_themes';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function themeMenus()
    {
    	return $this->hasMany('App\ThemeMenu', 'ms_theme_id', 'id');
    }

}
