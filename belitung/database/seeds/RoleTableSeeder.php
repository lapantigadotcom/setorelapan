<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RoleTableSeeder extends Seeder {

    public function run()
    {
        Role::truncate();

        Role::create([
            'code' => 'SP',
            'name' => 'Super Admin',
            'locked' => '1',
            'enabled' => '1',
            'require_file' => '0',
            'level' => '1'
        ]);
        Role::create([
            'code' => 'MG',
            'name' => 'Manager',
            'locked' => '1',
            'enabled' => '1',
            'require_file' => '0',
            'level' => '2'
        ]);
        Role::create([
            'code' => 'OF',
            'name' => 'Officer',
            'locked' => '1',
            'enabled' => '1',
            'require_file' => '0',
            'level' => '3'
        ]);

        Role::create([
            'code' => 'US',
            'name' => 'User',
            'locked' => '0',
            'enabled' => '1',
            'default' => '1',
            'require_file' => '0',
            'level' => '4'
        ]);

        Role::create([
            'code' => 'AG',
            'name' => 'Agent',
            'locked' => '0',
            'enabled' => '1',
            'require_file' => '1',
            'level' => '4'
        ]);

        Role::create([
            'code' => 'DT',
            'name' => 'Distributor',
            'locked' => '0',
            'enabled' => '1',
            'require_file' => '1',
            'level' => '4'
        ]);

        Role::create([
            'code' => 'PT',
            'name' => 'Perusahaan',
            'locked' => '0',
            'enabled' => '1',
            'require_file' => '1',
            'level' => '4'
        ]);
        Role::create([
            'code' => 'OV',
            'name' => 'Overseas',
            'locked' => '0',
            'enabled' => '1',
            'require_file' => '0',
            'level' => '5'
        ]);
    }

}