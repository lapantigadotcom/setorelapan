<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_prices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('price')->nullable();
			$table->integer('ms_product_id')->index('fk_ms_prices_ms_products1_idx');
			$table->integer('ms_role_id')->index('fk_ms_prices_ms_roles1_idx');
			$table->integer('ms_currency_id')->index('fk_ms_prices_ms_currency1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_prices');
	}

}
