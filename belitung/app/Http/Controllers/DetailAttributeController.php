<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DetailAttribute;
use App\Http\Requests\DetailAttributeRequest;
use App\Attribute;

use Session;
class DetailAttributeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(DetailAttributeRequest $request)
	{
		$data['ms_attribute_id'] = $request->input('attribute');
		$data['content'] = DetailAttribute::where('ms_attribute_id', '=', $data['ms_attribute_id'])->get();
		return view('pages.detailattribute.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(DetailAttributeRequest $request)
	{
		$data['ms_attribute_id'] = $request->input('attribute');
		$data['attribute'] = Attribute::lists('name', 'id');
		return view('pages.detailattribute.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(DetailAttributeRequest $request)
	{
		DetailAttribute::create($request->all());
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('ki-admin.detailattribute.index', ['attribute' => $request->input('ms_attribute_id')]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = DetailAttribute::find($id);
		$data['ms_attribute_id'] = $data['content']->ms_attribute_id;
		return view('pages.detailattribute.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(DetailAttributeRequest $request, $id)
	{
		$data = DetailAttribute::find($id);
		$data->update($request->all());
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.detailattribute.index', ['attribute' => $request->input('ms_attribute_id')]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$detailAttribute = DetailAttribute::find($id);
		$attribute_id = $detailAttribute->ms_attribute_id;
		foreach ($detailAttribute->detailAttributeProducts as $val) {
			foreach ($val->detailAttributeProductDetailOrders as $vall) {
				$vall->delete();
			}
			$val->delete();
		}
		$detailAttribute->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.detailattribute.index', ['attribute' => $attribute_id]);
	}

}
