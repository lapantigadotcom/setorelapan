<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('caption', 128)->nullable();
			$table->string('description', 512)->nullable();
			$table->string('image', 256)->nullable();
			$table->string('mime', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_images');
	}

}
