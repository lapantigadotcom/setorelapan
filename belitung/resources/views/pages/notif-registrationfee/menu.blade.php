<div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{!! route('ki-admin.notif-registrationfee.index').'?status=pending&state=0' !!}"><i class="fa fa-credit-card"></i> Pending</a></li>
                <li><a href="{!! route('ki-admin.notif-registrationfee.index').'?status=approved&state=1' !!}"><i class="fa fa-credit-card"></i> Approved</a></li>
                <li><a href="{!! route('ki-admin.notif-registrationfee.index').'?status=unnaproved&state=2' !!}"><i class="fa fa-credit-card"></i> Unnaproved</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->