<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\NotifMessage;
use App\User;
use App\Role;
use Input;
use Auth;
use Session;
use App\Http\Requests\NotifMessageCustomerRequest;

class NotifMessageCustomerController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function inbox()
	{
		$data['content'] = NotifMessage::where('ms_user_recipient_id', '=', Auth::id())->get();
		return view('pages.notif-message-customer.inbox', compact('data'));
	}

	public function outbox()
	{
		$data['content'] = NotifMessage::with('recipient')->get();
		return view('pages.notif-message-customer.outbox', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('0' => 'Individual', '1' => 'Group');
		$data['receiver'] = User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('name', 'id');
		return view('pages.notif-message-customer.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotifMessageCustomerRequest $request)
	{
		$data['ms_user_id'] = Auth::user()->id;
		$data['group'] = $request->input('group');
		$data['subject'] = $request->input('subject');
		$data['content'] = $request->input('content');
		if($data['group'] == '0') {
			$data['sent'] = '1';
			$data['draft'] = '0';
			$data['ms_user_recipient_id'] = $request->input('receiver');
			NotifMessage::create($data);
		}
		else if($data['group'] == '1') {
			$receivers = User::whereHas('roles', function($q) use($request) {
				$q->where('ms_roles.id', '=', $request->input('receiver'));
			})->get();
			foreach($receivers as $receiver) {
				$data['sent'] = '1';
				$data['draft'] = '0';
				$data['ms_user_recipient_id'] = $receiver->id;
				NotifMessage::create($data);
			}
		}
		return redirect()->route('ki-admin.notif-message.outbox');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = NotifMessage::with('recipient')->find($id);
		return view('pages.notif-message-customer.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		NotifMessage::destroy($id);
		return redirect()->route('ki-admin.notif-message.outbox');
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}
	public function perwakilan($id = null)
	{
		if(empty($id))
			return redirect()->route('home');
		$data['content'] = \App\Order::find($id);

		if($data['content']->ms_user_id != Auth::id())
			return redirect()->route('home');
		return view('theme.'.$this->theme->code.'.pages.user.confirm-perwakilan',compact('data'));
	}
	public function perwakilanStore(Request $request, $id = null)
	{
		if(empty($id))
			return redirect()->route('home');
		$order = \App\Order::find($id);
		if($order->ms_user_id != Auth::id())
			return redirect()->route('home');
		$data = [
			'subject' => $request->input('subject'),
			'content' => $request->input('content'),
			'ms_user_id' => Auth::id(),
			'ms_user_recipient_id' => $order->ms_reseller_id,
			'sent' => '1'
		];
		\App\NotifMessage::create($data);
		Session::flash('success','Konfirmasi berhasil dikirimkan.');
		return redirect()->route('user.report');
	}
}
