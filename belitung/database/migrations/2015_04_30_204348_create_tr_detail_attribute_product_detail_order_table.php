<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrDetailAttributeProductDetailOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_detail_attribute_product_detail_order', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_detail_attribute_product_id')->index('fk_tr_detail_attribute_product_detail_order_ms_detail_attri_idx');
			$table->integer('tr_detail_order_id')->index('fk_tr_detail_attribute_product_detail_order_tr_detail_order_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_detail_attribute_product_detail_order');
	}

}
