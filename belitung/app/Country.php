<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;

class Country extends Model {

	protected $table = 'ms_country';
	protected $guarded = ['id'];
    public $timestamps = false;
}
