<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRegistrationFeeRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'ms_payment_id' => 'required|numeric',
			'nominal' => 'required|numeric',
			'account_name' =>'required',
			'account_number' => 'required',
			'transfer_date' => 'required',
			'information' => 'required',
			'file' => 'image'
		];
	}

}
