<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Courier extends Model {

	protected $table = 'ms_couriers';
	protected $guarded = ['id'];
    public $timestamps = false;

	public function orders()
	{
		return $this->hasMany('App\Order', 'ms_courier_id', 'id');
	}

}
