   <!--brands-area start-->
        <div class="brands-area margin-top-60 section-padding" style="background:#fff">
            <div class="container">
                <div class="brands-inner">
                    <div class="row">
                        <div class="section-title">
                        <h2>Klien Kami</h2>
                     </div>
                        <div class="brans-carousel">
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/astra.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/cimb.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/its.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/dexamed.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/danamon.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/pos.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/hypermart.jpg') !!}" alt="" /></a>
                                    </a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                            <!-- single-brand start -->
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#"><img src="{!! asset('theme/citraflorist/img/klien/upn.jpg') !!}" alt="" /></a>
                                </div>
                            </div>
                            <!-- single-brand end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--brands-area end-->