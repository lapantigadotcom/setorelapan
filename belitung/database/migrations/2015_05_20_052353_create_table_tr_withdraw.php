<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrWithdraw extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_withdraw', function($table)
		{
			$table->increments('id');
			$table->integer('ms_user_id');
			$table->string('month');
			$table->string('year');
			$table->integer('referral_value');
			$table->integer('monthly_value');
			$table->integer('batch');
			$table->char('status',1);
			$table->datetime('withdraw_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tr_withdraw');
	}

}
