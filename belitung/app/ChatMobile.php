<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GCM;
use App\User;

class ChatMobile extends Model {

	protected $table = 'tr_chats';
	protected $guarded = ['id'];
	protected $filled = ['name','email','handphone','description', 'photo', 'ms_gcm_id','parent','read_flag','is_admin','ms_user_admin_id','path_user'];
	public $timestamps = true;
	public function gcm()
	{
		return $this->belongsTo('App\GCM', 'ms_gcm_id', 'id');
	}
	public function admin()
	{
		return $this->belongsTo('App\User', 'ms_user_admin_id', 'id');
	}


}
