@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Report
                        <small>Afiliasi</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Top Contributor Lists &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Total Afiliasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['top_contributor'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
@if(Auth::user()->hasAccess('ki-admin.user.show'))
  <a href="{!! route('ki-admin.user.show', [$row->id]) !!}">{!! $row->name . " (" . $row->username . ")" !!}</a>
@else
  $row->name . " (" . $row->username . ")"
@endif
                                                </td>
                                                <td>{{ $row->total }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Total Afiliasi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Afiliasi Lists &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Referraled by</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                  {!! $row->name . " (" . $row->username . ")" !!}
                                                </td>
                                                <td>
                                                {!! isset($row->referraledBy->name) ? $row->referraledBy->name . " (" . $row->username . ")" : '-' !!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Referraled by</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->

@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true
                });
                $('#dataTable2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true
                });
            });
    </script>
@endsection