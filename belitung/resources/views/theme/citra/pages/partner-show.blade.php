
<title>
	citra Indonesia | &nbsp;{!! $data['content']->roles->first()->name !!} | &nbsp; {!! $data['content']->company !!}</title>
@extends('theme.citra.app')
@section('content')

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>{!! $data['context'] !!}</h2>
			<h5>{!! $data['content']->roles->first()->name !!} | &nbsp; {!! $data['content']->city->name !!}, {!! $data['content']->city->province->name !!}

			</h5>
		</div>
	</div>
</div>
<div class="row">

	<div class="row product-detail-container">

		<div class="col-md-offset-1 col-md-10 filter-product">
			<div class="row">
				@include('errors.session')
			</div>
			

<!---sidebar-->

<div class="col-md-4" style="max-width:24%;margin-right:20px">
				<div class="sidebar">
					<div class="sidebar-head">
						<i class="fa fa-reorder"></i>&nbsp; | &nbsp; citra Main Menu
					</div>
					<div class="sidebar-body">
						<ul>
			<li class="{!! Request::is('/')?'active':'' !!}"><a href="{!! URL::to('/') !!}"><i class="fa fa-caret-right"></i>&nbsp;Home</a></li>
	        <li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}"><i class="fa fa-caret-right"></i>&nbsp;Produk</a></li>
	        <li><a href="{!! route('contact-us') !!}"><i class="fa fa-caret-right"></i>&nbsp;Hubungi Kami</a></li>
												</ul>
					</div>
				</div>
				<div class="sidebar">
					<div class="sidebar-head">
						<i  class="fa fa-rss"></i>&nbsp; | &nbsp; BLOG TERBARU
					</div>
					<div class="sidebar-body">
						<ul>
							<li><a href="#"><i class="fa fa-caret-right"></i> &nbsp;citra Indonesia </a></li>
							<li><a href="#"><i class="fa fa-caret-right"></i> &nbsp;Theraphy by Ashibata</a></li>
							<li><a href="#"><i class="fa fa-caret-right"></i> &nbsp;Penyebab Kebotakan</a></li>
							<li><a href="#"><i class="fa fa-caret-right"></i> &nbsp;Our land and Plantation</a></li>
						</ul>
					</div>
				</div>
			
			</div>




			<div class="col-md-8" >
				<div class="row">
					<div class="col-md-12">
						<h3 class="text-center">Detail Partner </h3>
						<hr>
						<div class="row">
							<div class="col-md-4">
								@if(File::exists('data/user/photo/'.$data['content']->photo) and $data['content']->photo != '')
								<img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" class="img-responsive img-thumbnail img-lazy" style="padding:10px;">
								@else
								<img src="{!! asset('theme/citra/img/user.png') !!}" class="img-responsive img-thumbnail img-lazy">
								@endif
							</div>
							<div class="col-md-8">
								<h4>
									<a href="{!! route('partner-show',[$data['content']->code]) !!}">{!! $data['content']->name !!}</a>
								</h4>
								<table class="table">
									<tbody>
										<tr>
											<td>Perusahaan / Outlet</td>
											<td>:</td>
											<td>{!! $data['content']->company !!}</td>
										</tr>
										<tr>
											<td>Tipe</td>
											<td>:</td>
											<td>
												{!! $data['content']->roles->first()->name !!}
											</td>
										</tr>
										<tr>
											<td>Telepon</td>
											<td>:</td>
											<td>
												{!! $data['content']->code!=''?$data['content']->code:'-' !!}
											</td>
										</tr>
										<tr>
											<td>Mobile</td>
											<td>:</td>
											<td>
												{!! $data['content']->mobile !!}
											</td>
										</tr>
										<tr>
											<td>Alamat</td>
											<td>:</td>
											<td>
												{!! $data['content']->address !!}<br>
												{!! $data['content']->city->name !!}, {!! $data['content']->city->province->name !!}
											</td>
										</tr>
									</tbody>
								</table>								
							</p>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<a href="{!! route('partner-area') !!}" class="btn btn-citra-orange">Kembali</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('theme.citra.partials.best-products')
@endsection

@section('custom-footer')
{!! HTML::script('theme/citra/plugin/selectize/js/standalone/selectize.js') !!}
{!! HTML::script('theme/citra/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
{!! HTML::script('theme/citra/plugin/lazyload/jquery.unveil.js') !!}
<script type="text/javascript">
	$(document).ready(function() {
		$('.img-lazy').unveil();
		$("#category").selectize();
		$("#product-slider").owlCarousel({
			items : 5,
			lazyLoad : true,
			navigation : true,
			pagination : false,
			navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
		}); 
	});
</script>
@endsection
@section('custom-head')
{!! HTML::style('theme/citra/plugin/selectize/css/selectize.css') !!}
{!! HTML::style('theme/citra/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection