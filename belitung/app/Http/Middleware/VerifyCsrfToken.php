<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 private $openRoutes = ['m/login', 'm/register', 'm/updateprofile', 'm/updateprofiletwo','m/gcmstore','m/voicebox/store','m/voicebox/storetwo','m/chat/store','m/chat/storetwo','m/transactions/get']; 

	public function handle($request, Closure $next)
	    {
	        //add this condition 
	    foreach($this->openRoutes as $route) {
	
	      if ($request->is($route)) {
	        return $next($request);
	      }
	    }
	    
	    return parent::handle($request, $next);
	  }
  /*
	public function handle($request, Closure $next)
	{
		return parent::handle($request, $next);
	}
*/

}
