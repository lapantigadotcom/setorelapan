<!-- Modal -->
<div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="reviewLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="reviewLabel">Ganti Password Anda</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('route' => 'user.change.pass.store', 'method' => 'POST', 'id' => 'form-password')) !!}
        {!! BootstrapForm::password('oldpassword','Password Lama') !!}
        {!! BootstrapForm::password('password','Password Baru', array('id' => 'password')) !!}
        {!! BootstrapForm::password('cpassword','Konfirmasi Password Baru') !!}
        <div class="form-group">
          {!! Form::submit('Ganti Password', array('class' => 'btn btn-keiskei')) !!}
        </div>
        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
{!! HTML::script('theme/keiskei/plugin/validation/jquery.validate.min.js') !!}
<script type="text/javascript">
  $(document).ready(function() {
    $('#form-password').validate({
          errorClass : "help-block error",
          rules: {
            oldpassword : {
              required: true,
              minlength: 3
            },
            password :{
              required: true,
              minlength: 6
            },
            cpassword :{
              required: true,
              equalTo: "#password"
            }
          },
          messages: {
            oldpassword : {
              required : "Mohon masukkan password Lama Anda",
              minlength : $.validator.format("password Lama Anda harus minimal {0} karakter")
            },
            password : {
              required : "Mohon masukkan password baru Anda",
              minlength : $.validator.format("Password baru Anda harus minimal {0} karakter")
            },
            cpassword : {
              required : "Mohon masukkan konfirmasi password Anda",
              equalTo : "Konfirmasi password tidak sama"
            }
          }
       });
  });
</script>