<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\NotifSMS;
use Auth;
use Input;
use App\User;
use App\Role;
use App\Http\Requests\NotifSMSRequest;

class NotifSMSController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = NotifSMS::with('recipient')->where('ms_user_id', '=', Auth::id())->get();
		return view('pages.notif-sms.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('0' => 'Individual', '1' => 'Group');
		$data['receiver'] = User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('mobile', 'mobile');
		return view('pages.notif-sms.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotifSMSRequest $request)
	{
		$data['ms_user_id'] = Auth::user()->id;
		$data['group'] = $request->input('group');
		$data['content'] = $request->input('content');

		if($data['group'] == '0') {
			$data['receiver'] = User::find($request->input('receiver'))->mobile;
			if($this->sendNotif($data)) {
				$data['sent'] = '1';
			}
			else {
				$data['sent'] = '0';
			}
			$data['draft'] = '0';
			$data['ms_user_recipient_id'] = $request->input('receiver');
			NotifSMS::create($data);
		}
		else if($data['group'] == '1') {
			$receivers = User::whereHas('roles', function($q) use($request) {
				$q->where('ms_roles.id', '=', $request->input('receiver'));
			})->get();
			foreach($receivers as $receiver) {
				$data['receiver'] = $receiver->mobile;
				if($this->sendNotif($data)) {
					$data['sent'] = '1';
				}
				else {
					$data['sent'] = '0';
				}
				$data['draft'] = '0';
				$data['ms_user_recipient_id'] = $receiver->id;
				NotifSMS::create($data);
			}
		}
		return redirect()->route('ki-admin.notif-sms.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = NotifSMS::find($id);
		return view('pages.notif-sms.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		NotifSMS::destroy($id);
		return redirect()->route('ki-admin.notif-sms.index');
	}

	private function sendNotif($data)
	{
		$url = "http://www.freesms4us.com/kirimsms.php?";
		$userpass = "user=keiskei&pass=ZXCasd123&";
		$content="isi=".$data['content'];
		$no="no=".$data['receiver']."&";
		$smsUrl = $url.$userpass.$no.$content;
		$smsUrl = str_replace(" ", "%20", $smsUrl);
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $smsUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);

		// grab URL and pass it to the browser
		$response = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		$firstWord = explode(" ", $response)[0];
		if($firstWord == "MAAF") {
			return false;
		}
		else {
			return true;
		}
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}

}
