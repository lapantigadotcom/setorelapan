@extends('theme.citra.app')

<title>citra Indonesia | Laporan Anggota</title>


@section('content')
@include('theme.citra.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>citra Indonesia</h2>
			<h5>Beauty | Health | Cure </h5>
		</div>
	</div>
</div>


<section id="cart_items">
 	<div class="container">
	<!-- #tabel perwakilan-->

						<div class="shopper-informations">
							<div class="row">						@include('errors.session')

								<div class="col-sm-3">
								<div class="shopper-info">
									<p>History Pembelian</p>
								</div>
								</div>
							</div>
						</div>

					<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Kode Invoice</td>
							<td class="description">Waktu</td>
							<td class="price">Keterangan</td>
							<td class="price">Total</td>
							<td class="price">Opsi</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr><?php $no=1; ?>
							@foreach($data['content'] as $row)
					 
						
												
							<td class="cart_product">
								<a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
										<b>{!! $row->code !!}</b>
									</a>
							</td>
							<td class="cart_description" style="font-size:14px">
								{!! date('j M Y',strtotime($row->created_at)) !!}
							</td>
							<td class="cart_price" style="font-size:14px">
							{!! $row->statusOrder->name !!}
							@foreach($row->transfers as $val)
									<p>
										<em>Konfirmasi pembayaran telah dilakukan: {!! $val->nominal !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
									</p>
									@endforeach
							</td>
							 
							
							<td>
								<?php 
									$total = 0; ?>
									@foreach($row->detailOrders as $val)
									<?php $total_t = priceDisplay($data['default_role'], $val->product->prices); 
									if(!empty($val->product->discount))
									{
										$discount_t = ($total_t/100)*intval($val->product->discount);
										$total_t = $total_t - $discount_t;
									}
									$total_t = $total_t*$val->total;
									$total += $total_t;
									?>
									@endforeach
									<?php 
									$total = $total + $row->shipping_price+ ($total*10/100)  ;
									?>
									{!! money_format('%(#10n', $total) !!}
								</td>
								<td>
									@if($row->ms_status_order_id=='2')
									<a href="{!! route('user.transfer.order',[$row->id]) !!}?invoice={!! $row->code !!}" class="btn btn-citra-orange"> 
										Bayar
									</a>
									@elseif($row->ms_status_order_id=='8' and $row->ms_user_id == Auth::id())
									<a href="{!! route('user.confirm-perwakilan',[$row->id]) !!}" class="btn btn-citra-orange"> 
										Konfirmasi
									</a>
									@endif
								</td>
						</tr>@endforeach
						
					</tbody>
				</table>
			</div>
			   
	</div>
</section>

 @endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection

@section('custom-head')
	
@endsection