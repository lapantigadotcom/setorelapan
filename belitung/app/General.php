<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class General extends Model {

	protected $table = 'ms_generals';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function city()
    {
    	return $this->belongsTo('App\City', 'ms_city_id', 'id');
    }

}
