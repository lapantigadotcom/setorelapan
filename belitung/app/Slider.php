<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Slider extends Model {

	protected $table = 'ms_sliders';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function image()
    {
    	return $this->belongsTo('App\Image', 'ms_image_id', 'id');
    }

}
