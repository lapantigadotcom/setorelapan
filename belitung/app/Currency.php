<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Price;

class Currency extends Model {

	protected $table = 'ms_currencies';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function prices()
    {
        return $this->hasMany('App\Price', 'ms_currency_id', 'id');
    }

}
