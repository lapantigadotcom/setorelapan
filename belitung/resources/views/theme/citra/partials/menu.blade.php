  <header>
            <!-- header-top start -->
            <div class="header-top-area" style="background:#90133B">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <!-- header-top-left start -->
                            <div class="header-top-left res1">
                                <div class="top-menu lang-select floatleft">
                                     
                                </div>
                                 
                            <div class="phone-number floatleft">
                            <i class="fa fa-phone"></i>Phone :&nbsp;{!! $general->telephone !!}
                            </div>
                            </div>
                            <!-- header-top-left end -->
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <!-- header-top-right start -->
                            <div class="header-top-right">
                                <div class="top-menu floatright">
                                    <ul>
                                            <li><a href="#">Akun Saya <i class="fa fa-angle-down"></i></a>
                                                <ul>
                                                     @if(Auth::check() == true)
                                                    <li>
                                                    <i class="fa fa-dashboard fa-lg"></i>&nbsp;
                                                    <a href="{!! route('user.dashboard') !!}"> Beranda</a>
                                                    </li>
                                                    <li>
                                                    <i class="fa fa-power-off fa-lg"></i>&nbsp;
                                                    <a href="{!! route('user.logout') !!}"> Keluar</a>
                                                    </li>@else
                                                    <li><a href="#">Log In</a></li>
                                                    <li><a href="{!! route('register') !!}"> Daftar</a>
                                                    @endif
                                                    <li><a href="{{ url('/password/email') }}">Lupa Password?</p></a></li>
                                                 </ul>
                                            </li>
                                        </ul>
                                </div>
                            </div>
                            <!-- header-top-right end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-top end -->
            <!-- header-bottom start -->
            <div class="header-bottom-area">
                <div class="container">
                    <div class="header-bottom-inner">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <!--logo start-->
                                <div class="logo r1">
                                    <a href="{!! URL::to('/') !!}"><img src="{!! asset('theme/citraflorist/img/logo/logocitra-florist.png') !!}" alt="" />
                                    </a>
                                </div>
                                <!--logo end-->
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="mobile-menu-area">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mobile-menu">
                                                <nav id="dropdown">
                                                    <ul class="menu">
                                                    <!-- <li><a class="active" href="index.html">home</a> </li>  -->                                                    
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <!--mainmenu start-->
                                        <div class="mainmenu">
                                            <nav>
                                                <ul>
                                        <li class="{!! Request::is('/')?'active':'' !!}"><a href="{!! URL::to('/') !!}"><i class="fa fa-home" style="font-size:18px"></i> Home</a></li>
                                        @if(isset($articles[0]['id']))<li><a href="{!! route('articles',[$articles[0]['id'].'-'.$articles[0]['title']]) !!}">About Us</a></li> @endif
                                        <li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}">Our Catalog</a></li>                                                   
                                         <li><a href="#">Find Us</a>
                                                <ul class="mega-menu mega-menu-two">
                                                    <li>
                                                        <a href="#" class="mega-menu-title">Gresik</a>
                                                        <a href="http://www.tokobungagresik.com/" target="_blank">Visit Store</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="mega-menu-title">Sidoarjo</a>
                                                        <a href="http://www.tokobungamalangonline.net/" target="_blank">Visit Store</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="mega-menu-title">Mojokerto</a>
                                                        <a href="http://www.tokobungamojokerto.net/" target="_blank">Visit Store</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="mega-menu-title">Malang</a>
                                                        <a href="http://www.toko-bunga-sidoarjo.com/" target="_blank">Visit Store</a>
                                                    </li>
                                                </ul>
                                         </li>
                                        <li><a href="{!! route('blogs') !!}">Blogs</a></li>
                                        <li><a href="{!! route('contact-us') !!}">Contact Us</a></li>

                                        
                                                </ul>
                                            </nav>
                                        </div>
                                        <!--mainmenu end-->
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="cart-and-search floatright">
                                    
                                    <!--total-cart-start-->
                                    <div class="total-cart floatleft">
                                                <a href="{!! route('user.cart') !!}">
                                            Cart | <i class="fa fa-shopping-cart"></i>
                                         </a>
                                         
                                    </div>
                                    <!--total-cart-end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-bottom end -->
        </header>