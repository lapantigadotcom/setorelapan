<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsThemeMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_theme_menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 64)->nullable();
			$table->integer('ms_theme_id')->index('fk_ms_theme_support_ms_theme1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_theme_menus');
	}

}
