@extends('theme.citra.app')

<title>citra Indonesia | Afilisi Anggota citra </title>


@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>citra Indonesia</h2>
			<h5>Halaman Afilisi Anggota citra</h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12s">
						<table class="table">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Tipe Member</th>
									<th>Email</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; ?>
							@foreach($data['content'] as $row)
								<tr>
									<td>{!! $no++ !!}</td>
									<td>{!! $row->name !!}</td>
									<td>{!! $row->roles->first()->name !!}</td>
									<td>{!! $row->email !!}</td>
									<td>{!! $row->approved=='1'?"<span class='label label-primary'>Active</span>":"<span class='label label-default'>Inactive</span>" !!}</td>
								</tr>
							@endforeach
							@if($data['content']->count()==0)
								<tr>
									<td colspan="5">Tidak ada data</td>
								</tr>
							@endif
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-12">
								{!! $data['content']->render() !!}
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<a data-target="#myModal" data-toggle="modal" class="btn btn-citra-default">Dapatkan Link Affiliate</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('theme.citra.include.user-sidebar')
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Referral Link</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
        		<div class="form-group">
        			<label class="form-label" for="link-referrak">Link Referral</label>
        			<textarea class="form-control" readonly="true" id="link-referral" rows="1"><a href="{!! URL::to('/register?affiliate_token='.Auth::user()->code) !!}" target="_blank">Daftar</a></textarea>
        		</div>
        		<div class="form-group">
        			<label class="form-label" for="image-referrak">Widget Image Referral</label>
        			<textarea class="form-control" readonly="true" id="image-referral" rows="2"><a href="{!! URL::to('/register?affiliate_token='.Auth::user()->code) !!}" target="_blank"><img src="{!! asset('data/general/ref-link.gif') !!}"></a></textarea>
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@include('theme.citra.partials.best-products')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection

@section('custom-head')
	
@endsection