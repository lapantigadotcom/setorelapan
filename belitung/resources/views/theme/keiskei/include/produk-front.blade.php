<div class="col-sm-12 padding-right ">
					<div class="features_items"><!--features_items-->
						<center><h2 class="title text-center">Produk Katalog</h2>	
						<h5>Toko Bunga Surabaya Online</h5></center>
						@foreach($data['latest'] as $row)

							<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
									@if($row->productImages()->count() > 0)
									@if(File::exists($row->productImages->first()->image))
									<img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
									@endif
									@endif
										<h2></h2>
										<p>{!! $row->title !!}</p>
											<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Lihat</a>
									</div>
								<!--	<div class="product-overlay">
										<div class="overlay-content">
 											<p>{!! $row->title !!}</p>s
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
									</div>-->
								</div> 
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-check"></i>Citra Florist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>{!! ucwords($row->categoryproduct->name) !!}</a></li>
									</ul>
								</div>
							</div>
						</div>

												

					
						
					</div><!--features_items-->
					
					 <!-- single-product start-->						@foreach($data['latest'] as $row)

                                    <div class="col-lg-3">
                                        <div class="single-product">
                                            <a data-toggle="modal" title="Quick View" href="#productModal" class="view-single-product"><i class="fa fa-expand"></i></a>
                                            <div class="product-thumb">
                                                <span class="label-pro-sale">-9%</span>
                                                <span class="new">new</span>
									@if($row->productImages()->count() > 0)
									@if(File::exists($row->productImages->first()->image))
									<img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
									@endif
									@endif                                            </div>
                                            <div class="product-desc">
                                                <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>
                                                <div class="price-box floatleft">
                                                    <p class="normal-price"><?php
								if(Auth::check())
								if(Auth::user()->roles->first()->level == '5'){
							      setlocale(LC_ALL, "id_ID");
							    }else{
							      setlocale (LC_MONETARY, 'id_ID');
							    }
								?>
								Harga: {!! money_format('%(#10n', priceDisplay($data['default_role'], $data['content']->prices))  !!}</p>
                                                </div>
                                                <div class="product-action floatright">
                                                    <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                                    <a href="#"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#"><i class="fa fa-exchange"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>@endforeach
                                    <!-- single-product end-->