(function ($) {
 "use strict";
 
    
    // tooltip
     $('.ratings a').tooltip();
    $('[data-toggle="tooltip"]').tooltip();
    
      
    
    /*------------------------------------
    single-carousel
    ------------------------------------- */  
    $(".single-carousel").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,2],
        itemsMobile : [479,1],
    });

    /*------------------------------------
    single-carousel
    ------------------------------------- */  
        $(".single-carousel7").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,1],
        itemsMobile : [479,1],
    });
    /*------------------------------------
    single-carousel
    ------------------------------------- */  
        $(".single-carousel8").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,1],
        itemsMobile : [479,1],
    });
    /*------------------------------------
    single-carousel
    ------------------------------------- */  
        $(".single-carousel9").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [980,1],
        itemsTablet: [768,1],
        itemsMobile : [479,1],
    });
    /*------------------------------------
    single-carousel
    ------------------------------------- */  
        $(".single-carousel21").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 2,
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [980,1],
        itemsTablet: [768,2],
        itemsMobile : [479,1],
    });
    
    /*------------------------------------
    single-carousel
    ------------------------------------- */  
        $(".single-carousel33").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,2],
        itemsMobile : [479,1],
    });

    /*------------------------------------
    special-products-carousel
    ------------------------------------- */  
        $(".special-products-carousel").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:true,
        navigationText: ["<i class='fa fa-long-arrow-left'></i>","<i class='fa fa-long-arrow-right'></i>"],  
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,2],
        itemsMobile : [479,1],
    });
    /*------------------------------------
    top-sales-products-carousel
    ------------------------------------- */  
        $(".top-sales-products-carousel").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 5
    });
    /*------------------------------------
    brand-carousel
    ------------------------------------- */  
        $(".brans-carousel").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 6,
        itemsDesktop : [1199,6],
        itemsDesktopSmall : [980,6],
        itemsTablet: [768,3],
        itemsMobile : [479,2],
    });
    /*------------------------------------
    blogpost
    ------------------------------------- */  
        $(".blogpost-carousel").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:false,
        navigation:false,	  
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,2],
        itemsMobile : [479,1],
    });
    /*------------------------------------
    testimonial
    ------------------------------------- */  
        $(".testimonial-inner").owlCarousel({
        autoPlay: false, 
        slideSpeed:2000,
        pagination:true,
        paginationText: ["<span><i class='fa fa-angle-left'></i></span>","<span><i class='fa fa-angle-right'></i></span>"], 
        navigation:false,	  
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [980,1],
        itemsTablet: [768,1],
        itemsMobile : [479,1],
    });
    /*----------------------------
     price-slider active
    ------------------------------ */  
    $( "#slider-range" ).slider({
        range: true,
        min: 40,
        max: 600,
        values: [ 60, 570 ],
        slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
    " - $" + $( "#slider-range" ).slider( "values", 1 ) );  

    /*--------------------------
    scrollUp
    ---------------------------- */	
    $.scrollUp({
        scrollText: '.',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });
    
    /*--
    Pro Slider for Pro Details
    ------------------------*/
        $(".pro-img-tab-slider").owlCarousel({
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [768,4],
        itemsTablet: [767,3],
        itemsMobile : [479,2],
        slideSpeed : 1500,
        paginationSpeed : 1500,
        rewindSpeed : 1500,
        navigation : true,
        navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        pagination : false,
        addClassActive: true,
    });
    
    
  
 
})(jQuery); 