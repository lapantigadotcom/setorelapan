<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCode extends Model {

	protected $table = 'ms_transaction_code';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function transaction()
    {
    	return $this->hasMany('App\Transaction', 'ms_transaction_code_id', 'id');
    }

}
