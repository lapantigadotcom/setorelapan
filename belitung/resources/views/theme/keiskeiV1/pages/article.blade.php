@extends('theme.keiskei.app')

@section('content')

  
	<title>
	KeisKei Indonesia | {!! $data['content']->title !!}

	</title>
	<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@keiskeiashitaba" />
<meta name="twitter:creator" content="@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="{!! $data['content']->meta_description !!}" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="{!! $data['content']->meta_description !!}" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="{!! $data['content']->meta_description !!}" />
<meta name="DC.subject" content="{!! $data['content']->meta_keyword !!}" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="{!! $data['content']->meta_description !!}">
<meta name="keywords" content="{!! $data['content']->meta_keyword !!}"/>
<meta name='robots' content='INDEX, FOLLOW' />


  
 	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>{!! $data['content']->title !!}</h2>
			<h6> Published date:&nbsp; {!! date('l, j F Y') !!} &nbsp; | &nbsp; Author:&nbsp; KEISKEI INDONESIA


			</h6>
		</div>
	</div>
 
	<div class="row product-detail-container">

		<div class="col-md-offset-1 col-md-10 filter-product">
			<div class="row">
				@include('errors.session')
			</div>
			<div class="col-md-4">
				@include('theme.keiskei.include.sidebar-default')

				<div class="sidebar">
		<!-- <div class="sidebar-body">
						<img src="{!! asset('data/general/ref-link.gif') !!}" class="img-responsive">
		</div> -->
	</div>

	
			</div>
			<div class="col-md-8">
				<div style="padding-right:10px;">
						{!! $data['content']->description !!}					
					</p></div>
			
			<br>
			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			Share This: &nbsp; <div class="addthis_sharing_toolbox"></div>

			</div>

		</div>

	</div>
		
 

		@include('theme.keiskei.partials.best-products')
		@endsection

		@section('custom-footer')
		{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
		{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
		{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}


		<script type="text/javascript">
			$(document).ready(function() {
				$('.img-lazy').unveil();
				$("#category").selectize();
				$("#product-slider").owlCarousel({
					items : 5,
					lazyLoad : true,
					navigation : true,
					pagination : false,
					navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
				}); 
			});
		</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

		@endsection
		@section('custom-head')
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
		@endsection