@extends('theme.keiskei.app')

<title>Keiskei Indonesia | Konfirmasi Pembayaran</title>


@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Halaman Konfirmasi Pembayaran</h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-3 col-md-6 form-container">
						<div class="form-head text-center">
							<p>&nbsp;</p>
							<h4>Konfirmasi Pembayaran KEISKEI PATNERS</h4>
							<p>Silahkan Masukkan Data Pembayaran Anda.</p>
							<hr>
						</div>
						<div class="form-body">
						{!! Form::open(array('route' => ['user.transfer.order.store',$data['order']->id], 'method' => 'POST','files' => 'true')); !!}
                            <div class="form-group {!! $errors->any()?($errors->first('file')?' has-error':''):'' !!}">
							{!! Form::label('payment','Rekening Tujuan') !!}
							<?php 
								$arrPayment = array();
								foreach ($data['payment'] as $row) {
									$arrPayment[$row->id] = $row->name.' '.$row->account_number.' - a/n '.$row->account_name;
								}
							?>
							{!! Form::select('ms_payment_id',$arrPayment, $data['order']->ms_payment_id, array('class' => 'form-control','id' => 'ms_payment_id')) !!}
							</div>
                        	{!! BootstrapForm::text('invoice','Kode Invoice', $data['order']->code) !!}
                        	{!! BootstrapForm::text('nominal','Nominal (Contoh: 1500000)') !!}
                            {!! BootstrapForm::text('account_name', 'Nama Akun Bank') !!}
                            {!! BootstrapForm::text('account_number', 'No. Rekening') !!}
                            {!! BootstrapForm::text('transfer_date', 'Tanggal Transfer',null, array('id' => 'datepicker','readonly' => 'readonly')) !!}
                            {!! BootstrapForm::textarea('information', 'Catatan Tambahan') !!}
                        <div class="form-group {!! $errors->any()?($errors->first('file')?' has-error':''):'' !!}">
		                        {!! Form::label('file','Bukti Transfer', array('class' => 'control-label')) !!}
		                        {!! Form::file('file', array('class' => 'form-control')) !!}
		                        <span class="help-block">{!! $errors->any()?($errors->first('file')?$errors->first('file'):''):'' !!}</span>
		                    </div>
						<div class="form-group text-align form-action" style="">
							{!! BootstrapForm::submit('Simpan',array('class' => 'btn btn-keiskei')) !!}
							<a href="{!! route('user.report') !!}" class="btn btn-default">Kembali</a>
						</div>
						{!! BootstrapForm::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
	{!! HTML::style('theme/keiskei/css/jquery-ui.css') !!}
	{!! HTML::script('theme/keiskei/js/jquery-ui.js') !!}

	<script>
		$(function() {
		  $( "#datepicker" ).datepicker({"dateFormat": "yy-mm-dd" });
		});
	</script>
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	<script type="text/javascript">
		$(document).ready(function(){
			$("#ms_payment_id").selectize();
			$(':file').filestyle({size: "sm",buttonText: "Unggah"});
		});
	</script>
@endsection

