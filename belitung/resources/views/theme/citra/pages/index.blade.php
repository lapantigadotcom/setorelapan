
@extends('theme.citra.app')

@section('content')
<title>Toko Bunga Surabaya Online | Citra Florist Group</title> 
@include('theme.citra.include.slider') 		   				 
@include('theme.citra.include.produk_depan')
@include('theme.citra.partials.service')
@include('theme.citra.include.top_sales')
@include('theme.citra.include.service-area')
@include('theme.citra.include.blog-area')
@include('theme.citra.include.klien')
 				
@endsection

 

