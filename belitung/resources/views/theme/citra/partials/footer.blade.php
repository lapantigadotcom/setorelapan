<!--footer start-->
        <footer>
            <div class="footer-area footer-area-3">
                <!--footer-top-area start-->
                <div class="footer-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="footer-about-us">
                                <div class="footer-about-us-inner">
                                    <div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
                                        <div class="footer-about-us-thumb">
                                            <img src="{!! asset('theme/citraflorist/img/about/gambar_bawah.png') !!}" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <div class="footer-about-us-content">
                                            <h1>Citra Florist</h1>
                                            <p>Berdiri sejak tahun 2008, bisnis kami berawal dari toko bunga di Surabaya (florist) yang menjual kreasi aneka bunga segar seperti Papan Bunga Ucapan, Bunga Parcel, Bunga Buket, Bunga Salib, Bunga Peti Mati, Bunga Artificial, Bunga Forhand, Bunga Krans, dll.</p>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="footer-contact">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p class="adress">
                                                                    <i class="fa fa-map-marker"></i> Pasar Bunga Kayoon St. C25
Surabaya - Jawa Timur
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p class="phone">
                                                                    <i class="fa fa-phone"></i>
                                                                    <span class="mobile">HP. 08179618432, 081232565897</span>
                                                                  </p>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p class="email">
                                                                    <i class="fa fa-envelope-o"></i> Email:
                                                                    <br/>citra.florist@yahoo.com
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--footer-top-area end-->
                <!--footer-widgets-area start-->
                <div class="footer-widgets-area">
                    <div class="container">
                        <div class="row">
                            <!-- single-footer-widget start -->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <div class="single-footer-widget">
                                    <h3>Tentang Kami</h3>
                                    <ul>
                                        @if(isset($articles[4]['id']))<li><a href="{!! route('articles',[$articles[4]['id'].'-'.$articles[4]['title']]) !!}">Kebijakan Privasi</a></li>@endif
                                        @if(isset($articles[5]['id']))<li><a href="{!! route('articles',[$articles[5]['id'].'-'.$articles[5]['title']]) !!}">FAQ</a></li>@endif
                                        @if(isset($articles[6]['id']))<li><a href="{!! route('articles',[$articles[6]['id'].'-'.$articles[6]['title']]) !!}">Keanggotaan</a></li>@endif
                                    </ul>
                                </div>
                            </div>
                            <!-- single-footer-widget end -->
                            <!-- single-footer-widget start -->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <div class="single-footer-widget res1">
                                    <h3>Support</h3>
                                    <ul>
                                        @if(isset($articles[7]['id']))<li><a href="{!! route('articles',[$articles[7]['id'].'-'.$articles[7]['title']]) !!}">Peta Situs</a></li>@endif
                                        @if(isset($articles[3]['id']))<li><a href="{!! route('articles',[$articles[3]['id'].'-'.$articles[3]['title']]) !!}">Dukungan</a></li>@endif
                                        @if(isset($articles[8]['id']))<li><a href="{!! route('articles',[$articles[8]['id'].'-'.$articles[8]['title']]) !!}">Karir</a></li>@endif
                            
                                    </ul>
                                </div>
                            </div>
                            <!-- single-footer-widget end -->
                            <!-- single-footer-widget start -->
                            <div class="col-lg-2 col-md-2 hidden-sm col-xs-12">
                                <div class="single-footer-widget res1">
                                    <h3>Tautan Lain</h3>
                                    <ul>
                                        <li><a href="#">Sitemap</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- single-footer-widget end -->
                             <!-- single-footer-widget start -->
                            <div class="col-lg-2 col-md-2 hidden-sm col-xs-12">
                                <div class="single-footer-widget res1">
                                    <h3>Sosial Media</h3>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i> &nbsp;| Facebook</a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i>  &nbsp;| Twitter</a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i> &nbsp;| Instagram</a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- single-footer-widget end -->
                            <!-- single-footer-widget start -->
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="single-footer-widget res11 up">
                                    <h3>JAM KERJA</h3>
                                    <div class="store-opening-time">
                                        <p>
                                            <span class="day">Senin - Jumat</span>
                                            <span class="time">9:00 - 20:00</span>
                                        </p>
                                        <p>
                                            <span class="day">Sabtu - Minggu</span>
                                            <span class="time">10:00 - 15:00</span>
                                        </p>
                                         
                                    </div>
                                    <div class="payment">
                                        <label>Pembayaran</label>
                                        <a href="#"><img src="{!! asset('theme/citraflorist/img/logo_bank.png') !!}" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- single-footer-widget end -->
                        </div>
                    </div>
                </div>
                <!--footer-widgets-area end-->
                <!--footer-copyright-area start-->
                <div class="footer-copyright-area footer-copyright-area-3">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer-copyright">
                                    <p>Copyright &copy; 2016 CV Citra Kreasindo. All Rights Reserved</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--footer-copyright-area end-->
            </div>
        </footer>
        <!--footer end-->
  

            {!! HTML::script('theme/citraflorist/js/vendor/jquery-1.12.0.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/bootstrap.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/jquery.nivo.slider.pack.js') !!}
            {!! HTML::script('theme/citraflorist/js/jquery.countdown.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/owl.carousel.min.js') !!}
                                    {!! HTML::script('theme/citraflorist/js/jquery.meanmenu.js') !!}

            {!! HTML::script('theme/citraflorist/js/img-zoom/jquery.simpleLens.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/jquery-ui.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/wow.min.js') !!}
            {!! HTML::script('theme/citraflorist/js/plugins.js') !!}
            {!! HTML::script('theme/citraflorist/js/main.js') !!}




 