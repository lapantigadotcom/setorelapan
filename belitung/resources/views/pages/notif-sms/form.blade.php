<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Kirim SMS</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('group', 'Tipe') !!}
				{!! Form::select('group', $data['group'], null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('receiver', 'To') !!}
				{!! Form::select('receiver', $data['receiver'], null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('content', 'Message') !!}
				{!! Form::textarea('content', null, array('class' => 'form-control', 'placeholder' => 'Message ...')) !!}
			</div>
		</div><!-- /.box-body -->
		<div class="box-footer">
			<div class="pull-right">
				<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
			</div>
			<a href="{!! route('ki-admin.notif-sms.index') !!}" class="btn btn-default"><i class="fa fa-times"></i> Discard</a>
		</div><!-- /.box-footer -->
	</div><!-- /. box -->
</div><!-- /.col -->