<?php namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserProfileRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserVerifiedRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserDepanRegisterRequest;
use App\Role;
use App\Theme;
use App\User;
use App\UserFile;
use Auth;
use File;
use Hash;
use Illuminate\Http\Request;
use Image;
use Input;
use Session;

class UserController extends BasicController {
	protected $modelName;
	protected $path;
	function __construct() {
		$this->modelName = '\App\User';
		$this->path = 'data/user/';

		parent::__construct();
	}
	public function index()
	{
		$data['content'] = User::whereHas('roles', function($q)
		{
		    $q->where('locked', '<>', '1');

		})->get();
		return view('pages.user.index',compact('data'));
	}

	public function aktif()
	{
			$data['content'] = User::whereHas('aktif', function($q)
		{
		    $q->where('ms_role_id', '4' );		    $q->where('approved', '1' );$q->where('enabled', '1' );
		    $q->where('required_step', '1' );


		})->get();
		return view('pages.user.aktif',compact('data'));
	}


public function silver()
	{
			$data['content'] = User::whereHas('silver', function($q)
		{
		    $q->where('ms_role_id', '5' );		     


		})->get();
		return view('pages.user.silver',compact('data'));
	}

	public function gold()
	{
			$data['content'] = User::whereHas('gold', function($q)
		{
		    $q->where('ms_role_id', '6' );		     


		})->get();
		return view('pages.user.gold',compact('data'));
	}


	public function getLogin()
	{
		$data['theme'] = $this->theme;
		$data['general'] = $this->general;
		return view('theme.'.$this->theme->code.'.pages.auth.login',compact('data'));
	}

	public function getRegister()
	{
		$data['country'] = Country::lists('name','id');
		$data['city'] = City::lists('name','id');
		$data['theme'] = $this->theme;
		$data['general'] = $this->general;
		$code = Input::get('affiliate_token');
		if(!empty($code))
			$user_t = \App\User::where('code',$code)->first();
			if(!empty($user_t))
				$data['affiliate_token'] = $user_t->id;
		return view('theme.'.$this->theme->code.'.pages.auth.register',compact('data'));
	}
	public function postRegister(UserRegisterRequest $request)
	{
		$role = Role::where('default','1')->first();
		$data = new User;
		$overseas = false;
		if($this->general->ms_country_id != $request->input('ms_country_id'))
		{
			$data->required_step = '1';
			$role = Role::where('level','5')->first();
			$overseas = true;
		}
		$referraled_by = $request->input('referraled_by');
		if(!empty($referraled_by)){
			$data->referraled_by = $referraled_by;
			// $user = \App\User::find($referraled_by);

			// if(!empty($user))
			// {
			// 	$role_id_t = $user->roles->first()->id;
			// 	if($role_id_t=='5')
			// 	{
			// 		$user->roles()->detach();
			// 		$user->roles->attach(6);
			// 		\App\LogActivity::create([
			// 			'ms_user_id' => $user->id,
			// 			'description' => 'Role User '.$user->username.' telah berubah menjadi Gold Partner.'
			// 		]);
			// 	}
				
			// }
		}
		$data->email = $request->input('email');
		$data->username = $request->input('username');
		$data->mobile = $request->input('mobile');
				$data->pinbb = $request->input('pinbb');

		$data->ms_country_id = $request->input('ms_country_id');
		$data->ms_city_id = $request->input('ms_city_id');
		$data->enabled = '0';
		$data->first_login = '0';
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data->roles()->attach($role->id);
		$data->code = $data->roles->first()->code.'00'.$data->id;
		$data->save();
		$data['password'] = $password;
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah melakukan registrasi.'
		]);
		Session::flash('success', 'Pendaftaran berhasil. Detail User dan Password Anda, Terkirim via SMS.');
		$this->sendEmail('1',$data->email,$data);
		if(!$overseas)
		{
			Session::flash('success', 'Pendaftaran berhasil. Petunjuk aktivasi member akan dikirimkan ke handphone anda. Jika tidak, silakan cek email anda.');
			$this->sendSMS('1',$data->mobile,$data);
		}
		return redirect()->route('register');
	}
	public function postRegisterDepan(UserDepanRegisterRequest $request)
	{		
		$role = Role::where('default','1')->first();
		$data = new User;
		$overseas = false;
		if($this->general->ms_country_id != $request->input('ms_country_id'))
		{
			$data->required_step = '1';
			$role = Role::where('level','5')->first();
			$overseas = true;
		}
		$referraled_by = $request->input('referraled_by');
		if(!empty($referraled_by))
			$data->referraled_by = $referraled_by;
		$data->email = $request->input('email');
		$data->username = $request->input('username');
		$data->mobile = $request->input('mobile');
				$data->pinbb = $request->input('pinbb');

		$data->ms_country_id = $request->input('ms_country_id');
		$data->ms_city_id = $request->input('ms_city_id');
		$data->enabled = '0';
		$data->first_login = '0';
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data->roles()->attach($role->id);
		$data->code = $data->roles->first()->code.'00'.$data->id;
		$data->save();
		$data['password'] = $password;
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah melakukan registrasi.'
		]);
		Session::flash('success', 'Pendaftaran berhasil. Silakan cek email anda untuk aktivasi member.');
		$this->sendEmail('1',$data->email,$data);
		if(!$overseas)
		{
			Session::flash('success', 'Pendaftaran berhasil. Petunjuk aktivasi member akan dikirimkan ke handphone anda. Jika tidak, silakan cek email anda.');
			$this->sendSMS('1',$data->mobile,$data);
		}
		return redirect()->route('register');
	}
	public function getActivate($token=null)
	{
		if(empty($token))
			return redirect()->to('/');
		$username = Input::get('username');
		if(sha1($username) != $token)
			return redirect()->to('/');
		$data = User::where('username', $username)->first();
		if(empty($data))
			return redirect()->to('/');
		$data->enabled = '1';
		$data->save();
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah melakukan verifikasi email.'
		]);
		return redirect()->route('login');
	}
	public function postLogin(Request $request)
	{
		$username = $request->input('username');
		$password = $request->input('password');
		$remember_me = false;
		if($request->input('remember_me'))
		{
			$remember_me = true;
		};

		if(Auth::attempt(['username' => $username, 'password' => $password,'enabled' => 1],$remember_me) || Auth::attempt(['username' => $username, 'password' => $password,'enabled' => '0'],$remember_me))
		{

			if(Auth::user()->first_login)
			{
				$data = \App\User::find(Auth::id());
				$data->first_login = 1;
				$data->enabled = 1;
				$data->save();
			}
			return redirect()->route('user.dashboard');
		}
		Session::flash('danger', 'Username dan password tidak cocok');
		return redirect()->route('login');
	}
	public function logout()
	{
		Auth::logout();
		return redirect()->route('home');
	}
	public function dashboard()
	{
		$data['content'] = User::with('roles','registrationfee','logActivity')->find(Auth::id());
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['report'] = \App\Order::
			with(
				'detailOrders',
				'detailOrders.product',
				'detailOrders.product.prices'
			)
			->orWhere(function($query)
            {
                $query->where('ms_user_id', Auth::id())
                      ->where('ms_status_order_id', '<>', '1');
            })
            ->orWhere(function($query)
            {
                $query->where('ms_reseller_id', Auth::id())
                      ->where('ms_status_order_id', '<>', '1');
            })
            ->orderBy('created_at','desc')
			->take(5)->get();
		$data['message_outbox'] = \App\NotifMessage::
			with('user','recipient')
			->where('ms_user_id', Auth::id())
            ->where('parent_id', '0')
            ->orderBy('created_at', 'desc')
			->take(5)
			->get();
		$data['message_inbox'] = \App\NotifMessage::
			with('user','recipient')
			->where('ms_user_recipient_id', Auth::id())
            ->where('parent_id', '0')
            ->orderBy('created_at', 'desc')
			->take(5)
			->get();
		if($data['content']->required_step == 0)
			return redirect()->route('user.verified');
		if(Auth::user()->roles->first()->require_file == '1')
		{
			$this->refreshWithdraw(Auth::id());
			$temp_t= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','<>','1')->sum('referral_value');
			$temp_t2= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','<>','1')->sum('monthly_value');
			$data['total_active'] = intval($temp_t)+intval($temp_t2);
		}
		
		return view('theme.'.$this->theme->code.'.pages.user.dashboard',compact('data'));
	}
	public function verified()
	{
		$data['perusahaan'] = Role::where('require_file','1')->lists('name','id');
		$data['perorangan'] = Role::where('default','1')->first()->id;
		$data['city'] = City::lists('name','id');
		$data['bank'] = \App\Bank::lists('name','id');
		$data['content'] = User::with('roles','userFiles')->find(Auth::id());
		return view('theme.'.$this->theme->code.'.pages.user.verified',compact('data'));
	}
	public function verifiedUpdate(UserVerifiedRequest $request)
	{
		$data = User::with('userFiles')->find(Auth::id());
		$data->update($request->all());
		$data->required_step = '1';
		$data->save();
		$role = Role::find($request->input('ms_role_id'));
		$data->code = $role->code.'00'.$data->id;
		$data->save();
		$data->roles()->detach();
		$data->roles()->attach($role->id);
		if($data->roles->first()->default == '1')
		{
			$data->approved = '1';
			$data->save();
			$this->sendEmail('3', $data->email, $data);
		}
		if($role->require_file=='1')
		{
			$data_t = null;
			if($data->userFiles()->count()>0)
			{
				$data_t = $data->userFiles;
			}else{
				$data_t = new UserFile;
			}
			if ($request->hasFile('ktp'))
			{
			    $file = $request->file('ktp');
				$extension = $file->getClientOriginalExtension();
				$fileName = $data->code."_ktp_".date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				$data_t->ktp = $fileName;
			}
			if ($request->hasFile('siup'))
			{
			    $file = $request->file('siup');
				$extension = $file->getClientOriginalExtension();
				$fileName = $data->code."_siup_".date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				$data_t->siup = $fileName;
			}
			if ($request->hasFile('npwp'))
			{
			    $file = $request->file('npwp');
				$extension = $file->getClientOriginalExtension();
				$fileName = $data->code."_npwp_".date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				$data_t->npwp = $fileName;
			}
			if ($request->hasFile('tdp'))
			{
			    $file = $request->file('tdp');
				$extension = $file->getClientOriginalExtension();
				$fileName = $data->code."_tdp_".date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				$data_t->tdp = $fileName;
			}
			if ($request->hasFile('akte'))
			{
			    $file = $request->file('akte');
				$extension = $file->getClientOriginalExtension();
				$fileName = $data->code."_akte_".date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				$data_t->akte = $fileName;
			}
			$data_t->ms_user_id = Auth::id();
			$data->ms_bank_id = $request->input('ms_bank_id');
			$data->account_bank = $request->input('account_bank');
			$data->save();
		}
		Session::flash('success','verifikasi berhasil dilakukan.');
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah melakukan verifikasi keanggotaan.'
		]);
		return redirect()->route('user.dashboard');

	}
	public function edit($id)
	{
		$data['role'] = Role::where('locked','<>','1')->lists('name','id');
		$data['city'] = City::lists('name','id');
		$data['content'] = User::with('roles','userFiles')->find($id);
		return view('pages.user.form',compact('data'));
	}
	public function update($id, UserRequest $request)
	{
		$data = User::find($id);
		$data->update($request->all());
		$data->roles()->detach();
		$data->roles()->attach($request->input('ms_role_id'));
		if ($request->hasFile('photo'))
		{
			if(File::exists($this->path.'photo/'.$data->photo))
				File::delete($this->path.'photo/'.$data->photo);
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = $data->code."_photo_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path.'photo/',$fileName);
			$img = Image::make($this->path.'photo/'.$fileName);
			$img->save($this->path.'photo/'.$fileName, 60);
			$data->photo = $fileName;
			$data->save();
		}
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.user.index');
	}
	public function create()
	{
		$data['role'] = Role::where('locked','<>','1')->lists('name','id');
		$data['city'] = City::lists('name','id');
		$data['content'] = null;
		return view('pages.user.form',compact('data'));
	}
	public function store(UserRequest $request)
	{
		$data = User::create($request->all());
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data->roles()->attach($request->input('ms_role_id'));
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = $data->code."_photo_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path.'photo/',$fileName);
			$img = Image::make($this->path.'photo/'.$fileName);
			$img->save($this->path.'photo/'.$fileName, 60);
			$data->photo = $fileName;
			$data->save();
		}
		$data['password'] = $password;
		Session::flash('success', 'Please Check Email');
		$this->sendEmail('1',$data->email,$data);
		return redirect()->route('ki-admin.user.index');
	}
	public function destroy($id)
	{
		$data = User::
			with(
				'articles',
				'userFiles',
				'logs',
				'orders',
				'orders.detailOrders',
				'orders.detailOrders.detailAttributeProductDetailOrders',
				'reviews',
				'transfers',
				'notifEmails',
				'referral',
				'registrationfee',
				'logActivity',
				'withdraw',
				'reseller',
				'reseller.detailOrders',
				'reseller.detailOrders.detailAttributeProductDetailOrders',
				'notifMessages',
				'messageRecipients'
			)->
		find($id);
		foreach ($data->articles as $row) {
			$row->delete();
		}
		foreach ($data->orders as $row) {
			foreach ($row->detailOrders as $val) {
				foreach ($val->detailAttributeProductDetailOrders as $x) {
					$x->delete();
				}
				$val->delete();
			}
			$row->delete();
		}
		foreach ($data->reseller as $row) {
			foreach ($row->detailOrders as $val) {
				foreach ($val->detailAttributeProductDetailOrders as $x) {
					$x->delete();
				}
				$val->delete();
			}
			$row->delete();
		}
		if($data->userFiles()->count() > 0)
			$data->userFiles->delete();
		if($data->registrationfee()->count() > 0)
			$data->registrationfee->delete();
		foreach ($data->logs as $row) {
			$row->delete();
		}
		foreach ($data->reviews as $row) {
			$row->delete();
		}
		foreach ($data->transfers as $row) {
			$row->delete();
		}
		foreach ($data->notifEmails as $row) {
			$row->delete();
		}
		foreach ($data->logActivity as $row) {
			$row->delete();
		}
		foreach ($data->notifMessages as $row) {
			$row->delete();
		}
		foreach ($data->messageRecipients as $row) {
			$row->delete();
		}
		foreach ($data->messageRecipients as $row) {
			$row->delete();
		}

		$data->roles()->detach();
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.user.index');
	}
	public function reset($id)
	{
		$data = User::find($id);
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data['password'] = $password;
		Session::flash('success', 'Please Check Email');
		$this->sendEmail('2',$data->email,$data);
		\App\LogActivity::create([
			'ms_user_id' => $data->id,
			'description' => 'User '.$data->username.' telah mereset password.'
		]);
		Session::flash('success','Password berhasil direset');
		return redirect()->route('ki-admin.user.index');
	}
	public function profileEdit()
	{
		$data['country'] = Country::lists('name','id');
		$data['city'] = City::lists('name','id');
		$data['content'] = Auth::user();
		$data['general'] = \App\General::first();
		$data['bank'] = \App\Bank::lists('name','id');
		return view('theme.'.$this->theme->code.'.pages.user.profile-form',compact('data'));
	}
	public function profileUpdate(UserProfileRequest $request)
	{
		$data = User::find(Auth::id());
		$data->update($request->all());
		if ($request->hasFile('photo'))
		{
			if(File::exists($this->path.'photo/'.$data->photo))
				File::delete($this->path.'photo/'.$data->photo);
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = $data->code."_photo_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path.'photo/',$fileName);
			$img = Image::make($this->path.'photo/'.$fileName);
			$img->save($this->path.'photo/'.$fileName, 60);
			$data->photo = $fileName;
			$data->save();
		}
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('user.dashboard');
	}
	public function affiliateUser()
	{
		$data['content'] = User::with('roles')->where('referraled_by',Auth::id())->paginate(15);
		return view('theme.'.$this->theme->code.'.pages.user.affiliate',compact('data'));
	}
	public function show($id)
	{
		$page = 'show';
		$data['content'] = User::with('reseller','roles','logActivity','ordersReport','referral','referral.roles','registrationfee','city','country','withdraw')->find($id);
		$type = Input::get('type');
		if(!empty($type))
		{
			$page = $type;
			if($type=='withdraw')
			$this->refreshWithdraw($id);
		}
		$data['default_role'] = \App\Role::where('default','1')->first();
		return view('pages.user.'.$page,compact('data'));
	}
	public function submitChangePassword(ChangePasswordRequest $request)
	{
		$user = \App\User::find(Auth::id());
		if (Hash::check($request->input('oldpassword'), $user->password))
		{
		    $user->password = Hash::make($request->input('password'));
		    $user->save();
		    Session::flash('success','Password berhasil diperbarui');
		}else{
			Session::flash('danger','Password lama tidak cocok');
		}
		return redirect()->route('user.dashboard');
	}


	public function addAccountBank(){
		$ms_bank_id = Input::get('bank_id');
		$account_name = Input::get('account_name');
		if(empty($ms_bank_id) || empty($account_name))
			exit();
		$bank = \App\Bank::find($ms_bank_id);
		if(empty($bank))
			exit();
		$data = \App\AccountBank::create([
			'ms_bank_id' => $ms_bank_id,
			'ms_user_id' => Auth::id(),
			'account_name' => $account_name
		]);
		Session::flash('success', 'Akun Rekening bank berhasil ditambahkan');
		return redirect()->route('user.profile.edit');
	}
	public function destroyAccountBank($id = null)
	{
		if(empty($id))
			exit();
		$data = \App\AccountBank::find($id);
		if($data->ms_user_id != Auth::id())
			exit();
		$data->delete();
		Session::flash('success', 'Akun Rekening bank berhasil dihapus');
		return redirect()->route('user.profile.edit');
	}
}