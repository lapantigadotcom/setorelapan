 <!--blogposts-area start-->
        <div class="blogposts-area blogposts-area-2 testimonial-area" style="padding:0px" >
            <div class="container ">
                <div class="row">
                    <div class="section-title">
                        <h2>Blog Kami</h2>
                        <h1>Artikel Terbaru Citra Florist</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="blogpost-carousel">
                        <!--single-blogpost start-->
                           @foreach($data['latest_article'] as $row)
                        <div class="col-lg-12">
                            <div class="single-blogpost">
                                
                                <div class="blogpost-desc">
                                    <h3> <a title="{{ strlen($row->title) > 50?substr($row->title,0,50).'...':$row->title }}" href="{!! route('articles',[$row->id.'-'.$row->title]) !!}">{{ strlen($row->title) > 25?substr($row->title,0,25).'...':$row->title }}</a> </h3>
 <p>
                        {!! strlen(strip_tags($data['latest_one']->description))>100?substr(strip_tags($data['latest_one']->description),0,100).'...':strip_tags($data['latest_one']->description) !!}
                    </p>                                    <a href="{!! route('articles',[$data['latest_one']->id.'-'.$data['latest_one']->title]) !!}" class="readmore">Read more</a>
                                </div>
                            </div>
                        </div>
                            @endforeach
                         <!--single-blogpost end-->

 
                </div>
            </div>
        </div>
        <!--blogposts-area end-->