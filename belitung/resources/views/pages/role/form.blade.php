<div class="box-body">
		{!! BootstrapForm::text('name') !!}
	<div class="form-group">
		{!! Form::label('enabled','Enabled') !!}
		{!! BootstrapForm::checkbox('enabled', ' Enabled', '1', null) !!}
	</div>
	<div class="form-group">
		{!! Form::label('require_file','Require File') !!}
		{!! BootstrapForm::checkbox('require_file', ' Require File', '1', null) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>