<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsProductImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_product_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_product_id')->index('fk_ms_product_image_ms_products1_idx');
			$table->string('image', 256)->nullable();
			$table->string('caption', 128)->nullable();
			$table->boolean('featured')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_product_images');
	}

}
