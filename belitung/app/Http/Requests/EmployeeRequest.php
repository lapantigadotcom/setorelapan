<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmployeeRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$employee = \App\User::find($this->route()->getParameter('employee'));
	    switch($this->method())
	    {
	        case 'GET':
	        case 'DELETE':
	        {
	            return [];
	        }
	        case 'POST':
	        {
	            return [
	                'name' => 'required',
	                'username' => 'required|unique:ms_users',
					'email' => 'required|email|unique:ms_users',
					'password' => 'required|min:6',
					'ms_city_id' => 'required',
					'photo' => 'image'
	            ];
	        }
	        case 'PUT':
	        case 'PATCH':
	        {
	            return [
	                'username' 		=> 'required|unique:ms_users,username,'.$employee->id,
					'email' 		=> 'required|email|unique:ms_users,email,'.$employee->id,
					'photo' 		=> 'image'
	            ];
	        }
	        default:break;
	    }
	}

}
