
@extends('theme.citra.app')
@section('content')
<title>
	citra Indonesia | Branch Patner, Gold Patner, Silver Patner, Bronze Patner</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@citraashitaba" />
<meta name="twitter:creator" content="@@citraashitaba" />
<meta name="twitter:url" content="https://www.citra.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut citra" />
<meta name="twitter:description" content="citra tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner" />
<meta property="og:image" content="https://www.citra.co.id/theme/citra/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.citra.co.id" />
<meta name="og:description" content=" citra tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner" />
<meta property="fb:admins" content="https://www.facebook.com/citraIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="citra Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Branch Patner, Gold Patner, Silver Patner, Bronze Patner,Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="citra Indonesia" />
<meta name="DC.publisher" content="citra Indonesia" />
<meta name="description" content="citra tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner">
<meta name='robots' content='INDEX, FOLLOW' />

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>{!! $data['context'] !!}</h2>
			<h5>citra INDONESIA | The Real Ashitaba
			</h5>
		</div>
	</div>
</div>
<div class="row">

	<div class="row product-detail-container">

		<div class="col-md-offset-1 col-md-10 filter-product">
			<div class="row">
				@include('errors.session')
			</div>
			
 

			<div class="col-md-11" >
				<div class="row">
					<div class="col-md-12">
						{!! Form::open(array('route'=> 'partner-area', 'method' => 'GET', 'class' => 'form-inline')) !!}
						<div class="form-group">
							<label class="form-label" for="group_type">
								Pilih Tipe
							</label>
							<select name="group_type" class="form-control" id="group_type">
								<option value="ALL" @if(Session::get('group_type')=='ALL') selected="" @endif>Semua</option>
								@foreach($data['role'] as $key => $value)
								<option value="{!! $key !!}" @if(Session::get('group_type')==$key) selected="" @endif>{!! $value !!}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							{!! Form::submit('Tampilkan',array('class'=>'btn btn-citra')) !!}
						</div>
						{!! Form::close() !!}
						<hr>
					</div>
				</div>
				<div class="row">
					@foreach($data['content'] as $row)
					<div class="col-md-3 partner-boundary partner-item">
						<div class="col-md-12">
							<div class="img-responsive img gambar-produk hidden-xs hidden-ms" >
							@if(File::exists('data/user/photo/'.$row->photo) and $row->photo != '')
							<img src="{!! asset('data/user/photo/'.$row->photo) !!}" class="img-responsive img-thumbnail img-lazy" style="width:200px; height:150px;padding:10px;">
							@else
							<img src="{!! asset('theme/citra/img/user.png') !!}" class="img-responsive img-thumbnail img-lazy" style="width:200px; height:150px;">
							@endif
							<input style="border-radius:0;width:100%;height:100%;background: rgba(0, 88, 34, 0.3);" class="btn btn-widget" type="button" value="Lihat Profile" onclick="location.href='{!! route('partner-show',[$row->code]) !!}';"/></div>
							<h5>
							<a href="{!! route('partner-show',[$row->code]) !!}">{!! $row->name !!}</a>
							</h5>
							<p>
							<i title="Tipe Member" class="fa fa-user"></i>&nbsp;|{!! $row->roles->first()->name !!}<br>
							<i title="Alamat"class="fa fa-building"></i>&nbsp;|{!! $row->city->name !!}, {!! $row->city->province->name !!}
							</p>



						</div>
					</div>
					@endforeach
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
						{!! $data['content']->render() !!}
					</div>
				</div>
				<br><br>
			</div>
		</div>
	</div>
</div>
@include('theme.citra.partials.best-products')
@endsection

@section('custom-footer')
{!! HTML::script('theme/citra/plugin/selectize/js/standalone/selectize.js') !!}
{!! HTML::script('theme/citra/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
{!! HTML::script('theme/citra/plugin/lazyload/jquery.unveil.js') !!}
<script type="text/javascript">
	$(document).ready(function() {
		$('.img-lazy').unveil();
		$("#category").selectize();
		$("#product-slider").owlCarousel({
			items : 5,
			lazyLoad : true,
			navigation : true,
			pagination : false,
			navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
		}); 
	});
</script>

<style type="text/css">
	div.gambar-produk {
	    position: relative;
	    float:left;
	    margin:5px;}

	div.gambar-produk:hover input
	  {
	  display: block;
	  }

	div.gambar-produk input {
	    position:absolute;
	   left: 0;
	   top: 0;
	    display:none;
	}
	</style>


@endsection
@section('custom-head')
{!! HTML::style('theme/citra/plugin/selectize/css/selectize.css') !!}
{!! HTML::style('theme/citra/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection