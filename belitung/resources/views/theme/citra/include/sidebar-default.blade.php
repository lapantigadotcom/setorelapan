                         <div class="left-sidebar-title">
                            <h3>Artikel Terbaru</h3>
                        </div>
                        <div class="single-sidebar">
                           @if(isset($latest_article))
									@foreach($latest_article as $row)
									<p><a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><?php echo $row->title; ?></a></p>
									@endforeach
									@endif
                         </div>
                        <div class="left-sidebar-title">
                            <h3>Our FB Page</h3>
                        </div>
                        <!-- single-sidebar-start -->
                        <div class="single-sidebar">
                            <ul class="tags-list">
                             <aside id="facebook-likebox-3" class="widget widget_facebook_likebox">
                             	<h3 class="widget-title"><a href="https://www.facebook.com/citra.florist"></a></h3><iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FKeiskeiIndonesia&amp;width=270&amp;height=400&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;show_border=true&amp;header=false&amp;force_wall=false" style="border: none; overflow: hidden; width: 240px;  height: 230px; background: #fff" frameborder="0" scrolling="no"></iframe></aside>  
                            </ul>
                             
                        </div>
                        <!-- single-sidebar-end -->
                        <!-- single-sidebar-start -->
                        <div class="single-sidebar ds-none">
                            <div class="single-banner">
                                <!-- <a href="#"><img src="{!! asset('theme/citraflorist/img/banners/banner-left.jpg') !!}" alt="" /></a> -->
                            </div>
                        </div>
                        <!-- single-sidebar-end -->
 