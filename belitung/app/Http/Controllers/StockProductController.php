<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\StockProduct;
use App\Http\Requests\StockProductRequest;
use Input;

class StockProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['ms_product_id'] = Input::get('product');
		return view('pages.stock-product.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StockProductRequest $request)
	{
		StockProduct::create($request->all());
		return redirect()->route('ki-admin.product.edit', [$request->input('ms_product_id')]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = StockProduct::find($id);
		$data['ms_product_id'] = $data['content']->ms_product_id;
		return view('pages.stock-product.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(StockProductRequest $request, $id)
	{
		$stockProduct = StockProduct::find($id);
		$stockProduct->update($request->all());
		return redirect()->route('ki-admin.product.edit', [$request->input('ms_product_id')]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		StockProduct::destroy($id);
		return redirect()->back();
	}

}
