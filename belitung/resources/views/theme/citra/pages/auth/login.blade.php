@extends('theme.citra.app')
 
<title>Keiskei Indonesia | Login Anggota</title>

@section('content')
<div class="row">
    <div class="col-md-offset-1 col-md-10">
        <div class="banner-product text-center">
            <h2>Keiskei Indonesia</h2>
            <h5>Silahkan Login terlebih dahulu. </h5>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-1 col-md-10 filter-product">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 form-container">
                <div class="form-head text-center">
                    <h5>Silahkan masukkan detail member Anda.</h5>
                    <p></p>
                    @include('theme.'.$data['theme']->code.'.include.session')
                    <hr>
                </div>
                <div class="form-body">
                {!! BootstrapForm::open(array('store' => 'login.store')) !!}
                {!! BootstrapForm::text('username') !!}
                {!! BootstrapForm::password('password') !!}
                {!! BootstrapForm::checkbox('remember_me',' Ingat saya', true) !!}
                <div class="col-md-12 text-center form-action">
                    <div class="col-md-6 text-left">
                        {!! Form::submit('Masuk',array('class' => 'btn btn-keiskei')) !!}
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{ url('/password/email') }}"> Lupa Password?</a>
                    </div>
                </div>
                <br>
                {!! BootstrapForm::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-footer')
    <script type="text/javascript">
        $(document).ready(function() {
          $("#product-slider").owlCarousel({
            items : 5,
            lazyLoad : true,
            navigation : true,
            pagination : false,
            navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
          }); 
        });
    </script>
@endsection