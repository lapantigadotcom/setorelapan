@extends('layouts.app')

@section('content')
@include('theme.keiskei.scripts.price-product')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>PPN</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        @include('errors.session')
                        <div class="col-xs-12">
                            <div class="box">

                                <div class="box-body table-responsive">
                                <?php 
                                $total_full = 0;
                                ?>
                                @foreach($data['content'] as $row)
                                    @if($row->user()->count() > 0)
                                    <?php 
                                        $total = 0; ?>
                                        @if($row->detailOrders()->count() > 0)
                                        @foreach($row->detailOrders as $val)
                                        @if($val->product()->count() > 0)
                                        @if($val->product->prices()->count() > 0)
                                        <?php
                                        // $total_t = 0;
                                        // dd($row->user, $val->product, $val->product->prices);
                                        $total_t = priceDisplayAdmin($row->user, $data['default_role'], $val->product->prices); 
                                        if(!empty($val->product->discount))
                                        {
                                            $discount_t = ($total_t/100)*intval($val->product->discount);
                                            $total_t = $total_t - $discount_t;
                                        }
                                        $total_t = $total_t*$val->total;
                                        $total += $total_t;
                                        ?>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endif
                                        <?php 
                                        $total_full += $total ;
                                        ?>
                                    @endif
                                @endforeach
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3>
                                            {!! $total_full*10/100 !!}<sup style="font-size: 20px">Rp.</sup>
                                        </h3>
                                        <p>
                                            Total PPN
                                        </p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    
                                </div>
                                <?php 
                                $total_full = 0;
                                ?>
                                @foreach($data['bulan_ini'] as $row)
                                    @if($row->user()->count() > 0)
                                    <?php 
                                        $total = 0; ?>
                                        @if($row->detailOrders()->count() > 0)
                                        @foreach($row->detailOrders as $val)
                                        @if($val->product()->count() > 0)
                                        @if($val->product->prices()->count() > 0)
                                        <?php
                                        // $total_t = 0;
                                        // dd($row->user, $val->product, $val->product->prices);
                                        $total_t = priceDisplayAdmin($row->user, $data['default_role'], $val->product->prices); 
                                        if(!empty($val->product->discount))
                                        {
                                            $discount_t = ($total_t/100)*intval($val->product->discount);
                                            $total_t = $total_t - $discount_t;
                                        }
                                        $total_t = $total_t*$val->total;
                                        $total += $total_t;
                                        ?>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endif
                                        <?php 
                                        $total_full += $total ;
                                        ?>
                                    @endif
                                @endforeach
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3>
                                            {!! $total_full*10/100 !!}<sup style="font-size: 20px">Rp.</sup>
                                        </h3>
                                        <p>
                                            Total PPN Bulan Ini
                                        </p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    
                                </div>
                                <h4>Laporan PPN tiap bulan</h4>
                                <table id="dataTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Bulan</th>
                                            <th>Tahun</th>
                                            <th>Total PPN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $init_bulan = 6;
                                        $init_tahun = 2015;
                                        ?>
                                        @for($i=$init_tahun;$i<= date('Y');$i++)
                                            @for($j=1;$j<= 12;$j++)
                                            @if($i < date('Y') || ($i == date('Y') && $j<= date('n') ))
                                                
                                                    @if($i == $init_tahun && $j<$init_bulan)
                                                        <?php continue; ?>
                                                    @endif
                                                
                                            <tr>
                                                <td>
                                                    <?php 
                                                    echo date('F',strtotime($i.'-'.$j.'-01'));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo date('Y',strtotime($i.'-'.$j.'-01'));
                                                    ?>
                                                </td>
                                                <td>
                                                    
                                                
                                                <?php
                                                $temp = array($i, $j);
                                                $data_t = $data['content']->filter(function($v) use($temp)
                                                {
                                                    if($temp[0] == date('Y',strtotime($v->created_at)) && $temp[1]  == date('n',strtotime($v->created_at)))
                                                      return true;
                                                });
                                                ?>
                                                <?php 
                                                $total_full = 0;
                                                ?>
                                                @foreach($data_t as $row)
                                                    @if($row->user()->count() > 0)
                                                    <?php 
                                                        $total = 0; ?>
                                                        @if($row->detailOrders()->count() > 0)
                                                        @foreach($row->detailOrders as $val)
                                                        @if($val->product()->count() > 0)
                                                        @if($val->product->prices()->count() > 0)
                                                        <?php
                                                        // $total_t = 0;
                                                        // dd($row->user, $val->product, $val->product->prices);
                                                        $total_t = priceDisplayAdmin($row->user, $data['default_role'], $val->product->prices); 
                                                        if(!empty($val->product->discount))
                                                        {
                                                            $discount_t = ($total_t/100)*intval($val->product->discount);
                                                            $total_t = $total_t - $discount_t;
                                                        }
                                                        $total_t = $total_t*$val->total;
                                                        $total += $total_t;
                                                        ?>
                                                        @endif
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                        <?php 
                                                        $total_full += $total ;
                                                        ?>
                                                    @endif
                                                @endforeach
                                                Rp. {!! $total_full*10/100 !!}
                                                </td>
                                            </tr>
                                            @endif
                                            @endfor
                                        @endfor
                                    </tbody>
                                </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable();
            });
    </script>
@endsection