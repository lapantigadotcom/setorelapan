<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Payment;

class PaymentTableSeeder extends Seeder {

    public function run()
    {
        Payment::truncate();

        Payment::create([
            'name' => 'Bank Mandiri',
            'account_name' => 'PT. KeisKei',
            'account_number' => '1921681254',
        ]);
        

    }

}