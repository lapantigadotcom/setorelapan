@extends('layouts.app')

@section('content')
@include('theme.keiskei.scripts.price-product')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Folders</h3>
                  <div class='box-tools'>
                    <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  @include('pages.user.sidebar')
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Report</h3>   
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>{!! $data['content']->name !!} 
                        
                    </h3>
                    <h4>Group : {!! $data['content']->roles->first()->name !!}
                    <span class="mailbox-read-time pull-right">
                            {!! $data['content']->enabled=='1'?"<span class='label label-primary'>Enabled</span>":"<span class='label label-danger'>Disabled</span>" !!}
                            {!! $data['content']->approved=='1'?"<span class='label label-primary'>Approved</span>":"<span class='label label-danger'>Dispproved</span>" !!}
                            {!! $data['content']->safe=='1'?"<span class='label label-primary'>Safe</span>":"<span class='label label-danger'>Unsafe</span>" !!}
                        </span>
                    </h4>
                    <h5>Username : {!! $data['content']->username !!}</h5>
                    <h5>Email: {!! $data['content']->email !!} <span class="mailbox-read-time pull-right">Register at: {!! date('d-m-Y H:i:s', strtotime($data['content']->created_at)) !!}</span></h5>
                  </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">Order Personal</a></li>
                      @if($data['content']->roles->first()->require_file=='1')
                      <li><a href="#tab_2" data-toggle="tab">Order Perwakilan</a></li>
                      @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kode Invoice</th>
                                        <th>Waktu</th>
                                        <th>Keterangan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no=1; ?>
                                @foreach($data['content']->ordersReport as $row)
                                <tr>
                                    <td>
                                        <a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
                                            <b>{!! $row->code !!}</b>
                                        </a>
                                    </td>
                                    <td>{!! date('j M Y',strtotime($row->created_at)) !!}</td>
                                    <td>
                                        <b>{!! $row->statusOrder->name !!}
                                        @if($row->ms_status_order_id == '8')
                                        @if(Auth::user()->hasAccess('ki-admin.user.show'))<a href="{!! route('ki-admin.user.show',[$row->reseller->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail User">{{ $row->reseller->username }}</a>@else {{ $row->reseller->username }} @endif
                                        @endif
                                        </b>
                                        @foreach($row->transfers as $val)
                                        <p>
                                            <em>Konfirmasi pembayaran telah dilakukan: {!! $val->nominal !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
                                        </p>
                                        @endforeach
                                    </td>
                                    <td>
                                    <?php 
                                        $total = 0; ?>
                                        @foreach($row->detailOrders as $val)
                                        <?php $total_t = priceDisplayAdmin($data['content'],$data['default_role'], $val->product->prices); 
                                        if(!empty($val->product->discount))
                                        {
                                            $discount_t = ($total_t/100)*intval($val->product->discount);
                                            $total_t = $total_t - $discount_t;
                                        }
                                        $total_t = $total_t*$val->total;
                                        $total += $total_t;
                                        ?>
                                        @endforeach
                                        <?php 
                                        $ppn = $total*10/100;
                                        $total = $total + $row->shipping_price + $ppn;
                                        ?>
                                    {!! money_format('%(#10n', $total ) !!}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if($data['content']->roles->first()->require_file=='1')
                        <div class="tab-pane" id="tab_2">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kode Invoice</th>
                                        <th>Kustomer</th>
                                        <th>Waktu</th>
                                        <th>Keterangan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no=1; ?>
                                @foreach($data['content']->reseller as $row)
                                <tr>
                                    <td>
                                        <a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
                                            <b>{!! $row->code !!}</b>
                                        </a>
                                    </td>
                                    <td>
                                    @if(Auth::user()->hasAccess('ki-admin.user.show'))<a href="{!! route('ki-admin.user.show',[$row->reseller->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail User">{{ $row->user->username }}</a>@else {{ $row->user->username }} @endif
                                    </td>
                                    <td>{!! date('j M Y',strtotime($row->created_at)) !!}</td>
                                    <td>
                                        <b>{!! $row->statusOrder->name !!}</b>
                                        @foreach($row->transfers as $val)
                                        <p>
                                            <em>Konfirmasi pembayaran telah dilakukan: {!! $val->nominal !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
                                        </p>
                                        @endforeach
                                    </td>
                                    <td>
                                    <?php 
                                        $total = 0; ?>
                                        @foreach($row->detailOrders as $val)
                                        <?php $total_t = priceDisplayAdmin($data['content'],$data['default_role'], $val->product->prices); 
                                        if(!empty($val->product->discount))
                                        {
                                            $discount_t = ($total_t/100)*intval($val->product->discount);
                                            $total_t = $total_t - $discount_t;
                                        }
                                        $total_t = $total_t*$val->total;
                                        $total += $total_t;
                                        ?>
                                        @endforeach
                                        <?php 
                                        $total = $total + $row->shipping_price;
                                        ?>
                                    {!! $total !!}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
                    
                </section><!-- /.content -->
@include('scripts.delete-modal')
@include('scripts.reset-password-modal')
@endsection


@section('custom-head')
    {!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection