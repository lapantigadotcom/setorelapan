<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Payment;
use App\User;

class Transfer extends Model {

	protected $table = 'tr_transfers';
	protected $guarded = ['id'];
	protected $fillable = [
		'ms_payment_id',
		'ms_user_id',
		'tr_order_id',
		'custom',
		'nominal',
		'account_name',
		'account_number',
		'status',
		'information',
		'file',
		'transfer_date'
	];

	public function order()
	{
		return $this->belongsTo('App\Order', 'tr_order_id', 'id');
	}

	public function payment()
	{
		return $this->belongsTo('App\Payment', 'ms_payment_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}

}
