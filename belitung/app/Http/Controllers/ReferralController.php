<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ReferralRequest;
use App\Point;
use App\Role;
use Illuminate\Http\Request;

class ReferralController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Role::with('referralpoint','referralpoint.targetrole')->where('require_file','1')->get();
		return view('pages.referral.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	

	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ReferralRequest $request)
	{
		$role = Role::where('require_file','1')->get();
		// $role = Role::where('id','<>',$request->input('ms_role_id'))->where('require_file','1')->get();
		foreach ($role as $row) {
			$value = \App\ReferralPoint::create([
				'ms_role_id' => $request->input('ms_role_id'),
				'target_ms_role_id' => $row->id,
				'value' => $request->input('val'.$row->id)
			]);
		}
		return redirect()->route('ki-admin.referral.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Role::with('referralpoint','referralpoint.targetrole')->find($id);
		$data['role'] = Role::where('require_file','1')->get();
		// dd($data['content']->referralpoint()->count());
		return view('pages.referral.form',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ReferralRequest $request, $id)
	{
		$role = Role::where('require_file','1')->get();
		foreach ($role as $row) {
			$data = \App\ReferralPoint::where('ms_role_id' , $request->input('ms_role_id'))->where('target_ms_role_id' , $row->id)->first();
			$data->value=$request->input('val'.$row->id);
			$data->save();
		}
		return redirect()->route('ki-admin.referral.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
