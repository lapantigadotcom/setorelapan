<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Theme;

class ThemeMenu extends Model {

	protected $table = 'ms_theme_menus';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function theme()
    {
    	return $this->belongsTo('App\Theme', 'ms_theme_id', 'id');
    }

}
