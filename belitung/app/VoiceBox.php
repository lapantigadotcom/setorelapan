<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GCM;

class VoiceBox extends Model {

	protected $table = 'tr_voice_boxs';
	protected $guarded = ['id'];
	protected $filled = ['name', 'email', 'telp','title','description','photo','is_anonymous','ms_gcm_id','read_flag'];
	public $timestamps = true;
	public function gcm()
	{
		return $this->belongsTo('App\GCM', 'ms_gcm_id', 'id');
	}


}
