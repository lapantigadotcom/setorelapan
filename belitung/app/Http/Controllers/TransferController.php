<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TransferRequest;
use Auth;
use Illuminate\Http\Request;
use Image;
use Session;
use Input;

use App\Transfer;

class TransferController extends BasicController {

	protected $path;

	function __construct() {
		$this->path = 'data/user/transfer/';
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$type = Input::get('state');
		if(empty($type))
		{
			$type = '0';
			$data['content'] = Transfer::where('status',$type)->orWhere('status',null)->orderBy('created_at','desc')->get();
		}
		else
			$data['content'] = Transfer::where('status',$type)->orderBy('created_at','desc')->get();
		// dd($data);
		return view('pages.transfer.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$data['order'] = \App\Order::find($id);
		if($data['order']->ms_status_order_id != '2' || $data['order']->ms_user_id != Auth::id())
			return redirect()->route('home');
		$data['payment'] = \App\Payment::all();
		return view('theme.'.$this->theme->code.'.pages.user.transfer',compact('data'));	
	}

	public function store(TransferRequest $request, $id)
	{
		$order = \App\Order::find($id);
		$order->ms_payment_id = $request->input('ms_payment_id');
		$order->save();
		$data = \App\Transfer::create($request->all());
		$data->ms_user_id = Auth::id();
		$data->tr_order_id = $id;
		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
			$extension = $file->getClientOriginalExtension();
			$fileName = Auth::user()->code."_transfer_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$data->file = $fileName;
		}
		$data->save();
		Session::flash('success','Pembayaran berhasil ditambahkan. Silahkan Menunggu konfirmasi Admin KEISKEI');
		\App\LogActivity::create([
			'ms_user_id' => Auth::id(),
			'description' => 'User '.Auth::user()->username.' telah melakukan pembayaran Invoice '. $order->code.'. Silahkan Menunggu konfirmasi Admin KEISKEI.'
		]);
		return redirect()->route('user.report');
	}
	public function change($id = null)
	{
		$status = Input::get('state');
		if(empty($id) || empty($status))
			exit();
		$data = Transfer::find($id);
		$data->status = $status;
		if($status == '1')
		{
			\App\LogActivity::create([
				'ms_user_id' => $data->ms_user_id,
				'description' => 'Administrator telah menyetujui konfirmasi pembayaran Anda'
			]);	
			$user = \App\User::find($data->ms_user_id);
			$sms['content'] = '[KEISKEI.CO.ID]~'.'Administrator telah menyetujui konfirmasi pembayaran Anda. Terima Kasih.';
			$sms['receiver'] = $user->mobile;
			$this->sendNotif($sms);		
		}
		$data->save();
		return redirect()->route('ki-admin.transfer.index');
	}
	public function destroy($id)
	{
		$data = Transfer::find($id);
		if(File::exists($this->path.$data->file))
			File::delete($this->path.$data->file);
		$data->delete();
		return redirect()->route('ki-admin.transfer.index');
	}
	public function show($id = '1')
	{
		$data['content'] = Transfer::with('user','user.roles','payment')->find($id);
		return view('pages.transfer.show', compact('data'));
	}
}
