<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Image;
use File;

class VoiceBoxController extends BasicController {
	protected $GCM_API = 'AIzaSyDU78ChBtG2Wl0cdp8GQu567lOEA3EICZU';
	protected $path;
	function __construct() {
		
		$this->path = 'data/voicebox/';

		parent::__construct();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = \App\VoiceBox::with('gcm')->get();
		return view('pages.voicebox.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('1' => 'Group');
		$data['receiver'] = \App\User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('email', 'email');
		return view('pages.voicebox.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$inp = array();
		$inp['name'] = $request->input('name');
		$inp['email'] = $request->input('email');
		$inp['telp'] = $request->input('handphone');
		$inp['title'] = $request->input('title');
		$inp['description'] = $request->input('description');
		$inp['is_anonymous'] = $request->input('is_anonymous');
		$inp['read_flag'] = '0';
		$gcm_id = $request->input('gcm_id');
		$gcm = \App\GCM::where('gcm_id', $gcm_id)->first();
		$user_id = $request->input('user_id');
		$anonymous = $inp['is_anonymous'];
		if(empty($gcm)){
			$gcm = new \App\GCM;
			$gcm->gcm_id = $gcm_id;
			
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}else{
			$gcm->gcm_id = $gcm_id;
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}
		$photo = $request->input('photo');

		if(!empty($photo))
		{

			$photo = urldecode($photo);
			$decoded=base64_decode($photo);
			$filename = "_voicebox_".date('Ymd_Hsi').'.JPEG';

			file_put_contents($this->path.$filename,$decoded);

			$inp['photo']  = $filename;		
			
		}
		
		$inp['ms_gcm_id'] = $gcm->id;
		$vb = \App\VoiceBox::create($inp);
		$json = array();
		$json['RESP'] = "SCSVCBX";
		$json['MESSAGE'] = 'Kotak suara berhasil terkirim.';
		$json['DATA'] = $vb->toArray();
		return json_encode($json);
	}

	public function storetwo(Request $request)
	{

		$inp = array();
		$inp['name'] = $request->input('name');
		$inp['email'] = $request->input('email');
		$inp['telp'] = $request->input('handphone');
		$inp['title'] = $request->input('title');
		$inp['description'] = $request->input('description');
		$inp['is_anonymous'] = $request->input('is_anonymous');
		$gcm_id = $request->input('gcm_id');
		$gcm = \App\GCM::where('gcm_id', $gcm_id)->first();
		$user_id = $request->input('user_id');
		$anonymous = $inp['is_anonymous'];
		if(empty($gcm)){
			$gcm = new \App\GCM;
			$gcm->gcm_id = $gcm_id;
			
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}else{
			$gcm->gcm_id = $gcm_id;
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}
		// $photo = $request->input('photo');
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "_voicebox_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$inp['photo']= $fileName;
		}
	
		// if(!empty($photo))
		// {

		// 	$photo = urldecode($photo);
		// 	$decoded=base64_decode($photo);
		// 	$filename = "_voicebox_".date('Ymd_Hsi').'.JPEG';

		// 	file_put_contents($this->path.$filename,$decoded);

		// 	$inp['photo']  = $filename;		
			
		// }
		// var_dump("Sdfas");exit();
		$inp['ms_gcm_id'] = $gcm->id;
		$vb = \App\VoiceBox::create($inp);
		$json = array();
		$json['RESP'] = "SCSVCBX";
		$json['MESSAGE'] = 'Kotak suara berhasil terkirim.';
		$json['DATA'] = $vb->toArray();
		return json_encode($json);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = \App\VoiceBox::find($id);
		$data['content']->read_flag = '1';
		$data['content']->save();
		return view('pages.voicebox.show', compact('data'));
	}
	public function destroy($id)
	{
		$data = \App\VoiceBox::find($id);
		if(File::exists($this->path.$data->photo))
			File::delete($this->path.$data->photo);
		\App\VoiceBox::destroy($id);
		return redirect()->route('ki-admin.voicebox.index');
	}

	protected function sendNotif($data)
	{
		if(Mail::send('theme.'.$this->theme->code.'.include.voicebox',$data, function($message) use($data)
		{
			$message->subject($data['subject']);
			$message->to($data['receiver']);
		})) {
			return true;
		}
		else {
			return false;
		}
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}
}
