@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-2">
                          @include('pages.transfer.menu')
                        </div><!-- /.col -->
                        <div class="col-md-10">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Transfer Lists &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Kustomer</th>
                                                <th>Nominal</th>
                                                <th>Pembayaran</th>
                                                <th style="width:150px">Keterangan</th>
                                                <th style="width:130px">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>
                                                    <a href="{!! route('ki-admin.transfer.show', [$row->id]) !!}">
                                                        {{ $row->user->name }}
                                                    </a>
                                                </td>
                                                <td>Rp. {{ $row->nominal  }}</td>
                                                <td>{!! $row->payment->name.' '.$row->payment->account_number.' - a/n '.$row->payment->account_name !!}</td>
                                                <td>
                                                <b>{{ $row->account_name }}</b>
                                                <br>
                                                {{ $row->account_number }}
                                                <br>
                                                {!! date('j F Y', strtotime($row->transfer_date)) !!}
                                                <br>
                                                {!! !empty($row->status)?($row->status==1?"<span class='label label-primary'>Approved</span>":"<span class='label label-default'>Disapproved</span>"):"<span class='label label-warning'>Pending</span>" !!}
                                                </td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.transfer.change') || Auth::user()->hasAccess('ki-admin.transfer.delete'))
                                                <div class="btn-group">
                                                  <button class="btn btn-danger" type="button">Change</button>
                                                  <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                  </button>
                                                  <ul role="menu" class="dropdown-menu">
                                                  @if(Auth::user()->hasAccess('ki-admin.transfer.change'))
                                                    <li><a href="{!! route('ki-admin.transfer.change',[$row->id]).'?state=1' !!}">Approve</a></li>
                                                    <li><a href="{!! route('ki-admin.transfer.change',[$row->id]).'?state=2' !!}">Unapprove</a></li>
                                                  @endif
                                                    <li class="divider"></li>
                                                    @if(Auth::user()->hasAccess('ki-admin.transfer.delete'))
                                                    <li><a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.transfer.delete',[$row->id]) !!}">Delete</a></li>
                                                    @endif
                                                  </ul>
                                                </div>
                                              @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Kustomer</th>
                                                <th>Nominal</th>
                                                <th>Pembayaran</th>
                                                <th>Keterangan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
@include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection