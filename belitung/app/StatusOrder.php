<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class StatusOrder extends Model {

	protected $table = 'ms_status_orders';
	protected $guarded = ['id'];
    public $timestamps = false;

	public function orders()
	{
		return $this->hasMany('App\Order', 'ms_status_order_id', 'id');
	}

}
