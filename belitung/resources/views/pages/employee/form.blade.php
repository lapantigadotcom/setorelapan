@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if(isset($data['content']))
                        {!! Form::model($data['content'],array('route' => ['ki-admin.employee.update',$data['content']->id], 'method' => 'PUT')) !!}
                        @else
                        {!! Form::open(array('route' => 'ki-admin.employee.store', 'method' => 'POST')) !!}
                        @endif
                            <div class="box-body">
                                <div class="form-group">
                                    {!! Form::label('ms_role_id','Role') !!}
                                    {!! Form::select('ms_role_id',$data['role'],isset($data['content'])?$data['content']->roles->first()->id:null, array('class' => 'form-control')) !!}
                                </div>
                                    {!! BootstrapForm::text('name') !!}
                                    {!! BootstrapForm::text('username') !!}
                                    {!! BootstrapForm::text('email') !!}
                                    @if(!isset($data['content']))
                                    {!! BootstrapForm::password('password') !!}
                                    @endif
                                    {!! BootstrapForm::text('telephone') !!}
                                    {!! BootstrapForm::text('mobile') !!}
                                <div class="form-group">
                                    {!! Form::label('ms_city_id','City') !!}
                                    {!! Form::select('ms_city_id',$data['city'],null, array('class' => 'form-control')) !!}
                                    <span class="help-block">{{ $errors->first('ms_city_id') }}</span>
                                </div>
                                    {!! BootstrapForm::textarea('address','Address',null,array('id' => 'wysihtml5')) !!}
                                <div class="form-group">
                                    {!! Form::label('enabled','Status') !!}
                                    {!! Form::select('enabled',[ '0' => 'Disabled', '1' => 'Enabled'],null, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('approved','Approved') !!}
                                    {!! Form::select('approved',[ '0' => 'No', '1' => 'Yes'],null, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection