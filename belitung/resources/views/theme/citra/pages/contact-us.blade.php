@extends('theme.citra.app')



<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db
" />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
">
<meta name='robots' content='INDEX, FOLLOW' />


@section('content')
<title>citra Indonesia | Hubungi Kami</title>
<!-- breadcrumbs-area-area start -->
		 <div class="breadcrumbs-area log">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumbs-menu">
							<ul>
								<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
 								<li><a href="#">Hubungi Kami <span>></span></a></li>

						</div>
					</div>
				</div>
			</div>
		 </div>
    <!-- breadcrumbs-area-area end -->
<div class="map-area">
        <div id="googleMap" style="width:100%;height:410px;"></div>
    </div>

<div class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            @include('errors.session')

                    <div class="contact-desc">
                        <h3 class="contact-title"><i class="fa fa-user"></i>Workshop Kami</h3>
                        <ul>
                            <li><i class="fa fa-map-marker"></i><strong>Address</strong> {!! $data['general']->address !!}</li>
                            <li><i class="fa fa-envelope"></i><strong>Phone</strong> {!! $data['general']->telephone !!}</li>
                            <li><i class="fa fa-phone"></i><strong>Email</strong> {!! $data['general']->email !!}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="contact-form">
                        <h3 class="contact-title"><i class="fa fa-envelope"></i>Tulis Pesan</h3>
                        <form action="mail.php" method="POST">
                            <div class="single-form">
                         {!! Form::open(array('route' => 'contact-us.store', 'method' => 'POST')) !!}
                            {!! BootstrapForm::text('name','Nama', Auth::check()?Auth::user()->name:'') !!}                            </div>
                            <div class="single-form">
                            {!! BootstrapForm::text('email','Email', Auth::check()?Auth::user()->email:'') !!}
                            </div>
                            <div class="single-form">
                            {!! BootstrapForm::text('subject','Judul') !!}
                            </div>
                            <div class="single-form">
                            {!! BootstrapForm::textarea('message','Pesan') !!}
                            </div>
                             <div class="single-form">
                            {!! Recaptcha::render() !!}
                                <span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>                            </div>
                             <div class="single-form">
                            {!! Form::submit('Kirim',array('class' => 'cart-button floatright')) !!}
                            </div>
                            
                        </form>{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsgXqdL5ngsj3P4jOYEm-DaeuPyxy6MA"></script>
    <script>
        function initialize() {
            var mapOptions = {
                zoom: 18,
                scrollwheel: false,
                center: new google.maps.LatLng(-7.268725, 112.748351)
            };
            var map = new google.maps.Map(document.getElementById('googleMap'),
                mapOptions);
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                animation: google.maps.Animation.BOUNCE,
                icon: 'img/map-marker.png',
                map: map
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    @include('theme.citra.partials.service')

        @include('theme.citra.include.klien')

 @endsection

	@section('custom-footer')

@endsection
@section('custom-head')
	{!! HTML::style('theme/citra/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/citra/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection