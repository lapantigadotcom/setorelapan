<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Input;
use Auth;
use Session;
use App\Review;

class ReviewController extends BasicController {

	public function store(Request $request, $id)
	{
		$data = \App\Review::create([
			'description' => $request->input('description'),
			'ms_user_id' => Auth::id(),
			'ms_product_id' => $id,
			'rate' => $request->input('rate'),
			'enabled' => '0'
		]);
		Session::flash('success', 'Review telah berhasil ditambahkan');
		return redirect()->back();
	}
	public function index()
	{
		$type = Input::get('state');
		if(empty($type))
			$data['content'] = Review::with('user', 'product')->where('enabled',$type)->orWhere('enabled', null)->orWhere('enabled', '')->get();
		else
			$data['content'] = Review::with('user', 'product')->where('enabled',$type)->get();
		return view('pages.notif-review.index', compact('data'));
	}
	public function change($id = null)
	{
		$enabled = Input::get('state');
		if(empty($id) || empty($enabled))
			exit();
		$data = Review::find($id);
		$data->enabled = $enabled;
		// if($status == '1')
		// {
		// 	$user = \App\User::find($data->ms_user_id);
		// 	$user->approved = '1';
		// 	$user->save();
			
		// 	\App\LogActivity::create([
		// 		'ms_user_id' => $data->ms_user_id,
		// 		'description' => 'Administrator telah menyetujui Registration Fee Anda'
		// 	]);
		// }
		$data->save();
		return redirect()->route('ki-admin.notif-review.index');
	}
	public function destroy($id)
	{
		Review::destroy($id);
		return redirect()->route('ki-admin.notif-review.index');
	}
}
