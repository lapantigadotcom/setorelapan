<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\TypeAttribute;

class TypeAttributeTableSeeder extends Seeder {

    public function run()
    {
        TypeAttribute::truncate();

        TypeAttribute::create([
            'code' => 'SB',
            'name' => 'Select Box'
        ]);

        TypeAttribute::create([
            'code' => 'CB',
            'name' => 'Check Box'
        ]);

        TypeAttribute::create([
            'code' => 'RB',
            'name' => 'Radio Button'
        ]);

    }

}