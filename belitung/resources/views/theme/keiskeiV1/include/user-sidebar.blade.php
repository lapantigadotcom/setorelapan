


<div class="sidebar hidden-xs hidden-sm">
	<div class="sidebar-head ">
		<i class="fa fa-gear"></i>&nbsp; | &nbsp; AKUN SAYA
	</div>
	<div class="sidebar-body">
		<ul>
			<li><a href="{!! route('user.profil') !!}"><i class="fa fa-caret-right"></i> &nbsp;Profil Saya </a></li>
			<li><a href="{!! route('user.status') !!}"><i class="fa fa-caret-right"></i> &nbsp;Status</a></li>
			<li><a href="{!! route('user.report') !!}"><i class="fa fa-caret-right"></i> &nbsp;Laporan</a></li>
			@if(Auth::user()->roles->first()->require_file=='1')
			<li><a href="{!! route('user.affiliate') !!}"><i class="fa fa-caret-right"></i> &nbsp;Afiliasi</a></li>
			<!-- <li><a href="{!! route('user.withdraw') !!}"><i class="fa fa-caret-right"></i> &nbsp;Withdraw</a></li> -->
			@endif
		</ul>
	</div>
</div>
<div class="sidebar hidden-xs hidden-sm">
	<div class="sidebar-head hidden-xs hidden-sm">
		<i  class="fa fa-rss"></i>&nbsp; | &nbsp; BLOG TERBARU					</div>
		<div class="sidebar-body">
			<ul>
				@if(isset($latest_article))
				@foreach($latest_article as $row)
				<li>
					<a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><i class="fa fa-caret-right"></i> &nbsp;<?php echo $row->title; ?></a>
				</li>
				@endforeach
				@endif
			</ul>
		</div>
	</div>


	<div class="sidebar hidden-xs hidden-sm">
		
			<div class="sidebar-head hidden-xs hidden-sm">				 <i  class="fa fa-lock"></i>&nbsp; | &nbsp; KEAMANAN SITUS </div>


		<div class="sidebar-body">
			<ul style="background:#eee; border-radius:5px; padding:17px;">
				<li>				      		
<center>
					<script language="JavaScript" type="text/javascript">
TrustLogo("https://www.keiskei.co.id/theme/keiskei/img/comodo.jpg", "CL1", "none");
</script>
<a  href="https://ssl.comodo.com" id="comodoTL">SSL Certificate</a>
</center><p>
				</li>
				<li> Layanan SSL yang kami gunakan di situs ini sudah menggunakan 2.048 bit
					token keamanan disertai hingga 256 enkripsi, 
					ini adalah tingkat keamanan tertinggi SSL untuk melindungi data-data penting Anda di situs ini. 
					SSL ini juga menggunakan teknologi "Point-to-verify" 
					yang menunjukkan verifikasi data secara real-time dan akurat. 
					<li>
						
					</ul>

				</div></div>


					<div class="sidebar">
		<div class="sidebar-body">
						<img src="{!! asset('theme/keiskei/img/keiskei-sidebar.png') !!}" class="img-responsive">
		</div>
	</div>