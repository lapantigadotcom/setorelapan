 <div class="products-area-3">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <h2>Citra Florist</h2>
                        <h1>Produk Kami</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div>
                            <div role="tablist">
                                <!-- Nav tabs -->
                                <ul class="product-tab-menu" role="tablist">
                                    <li class="active"><a href="#shop-square" data-toggle="tab">Bunga Papan</a>
                                    </li>
                                    <li><a href="#shop-square2" data-toggle="tab">Bunga Parcel</a>
                                    </li>
                                    <li><a href="#shop-square3" data-toggle="tab">Bunga Artifical</a>
                                    </li>
                                     
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Tab panes -->
                    <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="shop-square">
                            <div class="single-carousel33">
                                 <!-- single-product start-->                       @foreach($data['latest'] as $row)

                                    <div class="col-lg-3">
                                        <div class="single-product">
                                            <a data-toggle="modal" title="lihat produk" href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="view-single-product"><i class="fa fa-expand"></i></a>
                                            <div class="product-thumb">
                                                <span class="label-pro-sale">Call</span>
                                                <span class="new">Now</span>
                                    @if($row->productImages()->count() > 0)
                                    @if(File::exists($row->productImages->first()->image))
                                    <img src="{!! asset($row->productImages->first()->image) !!}" alt="{!! $row->title !!} | {!! ucwords($row->categoryproduct->name) !!}" />
                                    @endif
                                    @endif                                            </div>
                                            <div class="product-desc">
                                                <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>
                                                <div class="price-box floatleft">
                                                    <p class="normal-price">Call for price</p>
                                                </div>
                                                <div class="product-action floatright">
                                                    <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-shopping-cart"></i></a>
                                                    <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-exchange"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>@endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="shop-square2">
                            <div class="single-carousel33">
                                 <!-- single-product start-->                       @foreach($data['latest'] as $row)

                                    <div class="col-lg-3">
                                        <div class="single-product">
                                            <a data-toggle="modal" title="lihat produk" href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="view-single-product"><i class="fa fa-expand"></i></a>
                                            <div class="product-thumb">
                                                    <span class="label-pro-sale">Call</span>
                                                <span class="new">Now</span>
                                    @if($row->productImages()->count() > 0)
                                    @if(File::exists($row->productImages->first()->image))
                                    <img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
                                    @endif
                                    @endif                                            </div>
                                            <div class="product-desc">
                                                <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>
                                                <div class="price-box floatleft">
                                                    <p class="normal-price">Call for price</p>
                                                </div>
                                                <div class="product-action floatright">
                                                    <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-shopping-cart"></i></a>
                                                     <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-exchange"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>@endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="shop-square3">
                            <div class="single-carousel33">
                                 <!-- single-product start-->                       @foreach($data['latest'] as $row)

                                    <div class="col-lg-3">
                                        <div class="single-product">
                                            <a data-toggle="modal" title="lihat produk" href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="view-single-product"><i class="fa fa-expand"></i></a>
                                            <div class="product-thumb">
                                                   <span class="label-pro-sale">Call</span>
                                                <span class="new">Now</span>
                                    @if($row->productImages()->count() > 0)
                                    @if(File::exists($row->productImages->first()->image))
                                    <img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
                                    @endif
                                    @endif                                            </div>
                                            <div class="product-desc">
                                                <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>
                                                <div class="price-box floatleft">
                                                    <p class="normal-price">$155.00</p>
                                                </div>
                                                <div class="product-action floatright">
                                                    <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-shopping-cart"></i></a>
                                                     <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-exchange"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>@endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>