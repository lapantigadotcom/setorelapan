<link rel="shortcut icon" type="image/x-icon" href="{!! asset('theme/citraflorist/img/favicon.ico') !!}">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400' rel='stylesheet' type='text/css' />
<link href='https://fonts.googleapis.com/css?family=Cabin:400,700' rel='stylesheet' type='text/css' />
<link href='https://fonts.googleapis.com/css?family=Yesteryear' rel='stylesheet' type='text/css' />
<meta content='Citra Florist Group' property='og:title'/>
<meta content='Silakan klik masing-masing banner untuk terhubung ke website jaringan kami lainnya.  Klik Next atau Previous Button dibawah untuk melihat ar...' property='og:description'/>
<meta name="p:domain_verify" content="05e6dcbffcea28ff89468960b463c7bc"/>
<meta name='robots' content='INDEX, FOLLOW' />
<meta charset="utf-8" />
<meta name="author" content="www.lapantiga.com">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- CSS citraflorist -->
{!! HTML::style('theme/citraflorist/css/bootstrap.min.css') !!}
{!! HTML::style('theme/citraflorist/css/animate.css') !!}
{!! HTML::style('theme/citraflorist/css/nivo-slider.css') !!}
{!! HTML::style('theme/citraflorist/css/jquery-ui.min.css') !!}
{!! HTML::style('theme/citraflorist/css/img-zoom/jquery.simpleLens.css') !!}
{!! HTML::style('theme/citraflorist/css/meanmenu.min.css') !!}
{!! HTML::style('theme/citraflorist/css/owl.carousel.css') !!}
{!! HTML::style('theme/citraflorist/css/font-awesome.min.css') !!}
{!! HTML::style('theme/citraflorist/style.css') !!}
{!! HTML::style('theme/citraflorist/css/responsive.css') !!}
{!! HTML::script('theme/citraflorist/js/vendor/modernizr-2.8.3.min.js') !!}
<!-- /CSS citraflorist -->
@yield('custom-head')
 