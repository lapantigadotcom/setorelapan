@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard Referall</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    @if($data['content']->referralpoint()->count()>0)
                    {!! Form::open(array('route' => ['ki-admin.referral.update',$data['content']->id], 'method' => 'PUT')) !!}
                    @else
                    {!! Form::open(array('route' => 'ki-admin.referral.store', 'method' => 'POST')) !!}
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                        @foreach($data['role'] as $row)
                        <h4>{{ $row->name }}</h4>
                        <div class="form-group">
                            {!! Form::label('val'.$row->id,'Nilai Referral') !!}
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                @if($data['content']->referralpoint()->count()>0)
                                @foreach($data['content']->referralpoint as $va)
                                    @if($row->id == $va->target_ms_role_id)
                                    {!! Form::text('val'.$row->id,$va->value,array('class'=>'form-control')) !!}
                                    @endif
                                @endforeach
                                @else
                                {!! Form::text('val'.$row->id,null,array('class'=>'form-control')) !!}
                                @endif
                            </div>
                        </div>
                        @endforeach
                            {!! Form::hidden('ms_role_id',$data['content']->id) !!}
                            <div class="form-group">
                                {!! BootstrapForm::submit('Simpan') !!}
                            </div>
                        </div>
                    
                    </div>
                    {!! Form::close() !!}
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection