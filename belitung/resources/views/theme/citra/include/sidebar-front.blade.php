<div class="col-sm-3">
	
					<div class="left-sidebar">
				@if(Auth::check() == false)
				@if($errors->any())
				<div class="row">
					<div class="col-md-12 text-center">
					<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js">  
	<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
 @foreach($errors->all() as $rowErrors)
							<li>
							{!! $rowErrors !!}
							</li>
							@endforeach
		</div> 
</div>
					</div>
				</div>		

				 
				@endif
						<div class="signup-form"><!--sign up form-->
							<h2>Register</h2>
 								{!! BootstrapForm::open(array('route' => 'register.front.store', 'id' => 'form-validate', 'method' => 'post')) !!}

						{!! Form::text('username',null, array('class' => 'form-control', 'placeholder' => 'Username')) !!}
						{!! Form::text('mobile',null, array('class' => 'form-control', 'placeholder' => 'No. HP (untuk aktivasi)')) !!}
						{!! Form::text('email',null, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
						<div class="form-group">
						{!! Form::select('ms_country_id',$data['country'], null, array('class' => 'form-control', 'placeholder' => 'Country','id' => 'ms_country_id')) !!}
						</div>
						<div class="form-group">
						{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id', 'placeholder' => 'City')) !!}
						</div>
						{!! Form::submit('DAFTAR',array('class' => 'hvr-underline-from-left btn btn-primary')) !!}
						{!! BootstrapForm::close() !!}

 						</div><!--/sign up form-->

					</div>
					@else 
						<div class="shopper-info">
							<p>Akun <span style="color:green">Saya </span></p>
							<form> 

								<input id="disabledInput" placeholder="{!! Auth::user()->name !!}" type="text" disabled>
								<input id="disabledInput" placeholder="{!! Auth::user()->code!=''?Auth::user()->code:'-' !!}" type="text" disabled>	@if(Auth::user()->ms_bank_id != '')
								@endif
								<input id="disabledInput" placeholder="{!! Auth::user()->address!=''?Auth::user()->address:'-' !!}" type="text" disabled>
								<input id="disabledInput" placeholder="{!! Auth::user()->bank->name !!} - {!! Auth::user()->account_bank !!}" type="text" disabled>
								@if(Auth::user()->approved=='1')<input placeholder="Terverifikasi" type="text" disabled>@else
								<input id="disabledInput" placeholder="Belum Terverifikasi" type="text" disabled>@endif
							</form>
							<a class="btn btn-primary" href="{!! route('user.dashboard') !!}">My citra</a>
 						</div>
							
					</div>	@endif	<br><br>
			 

			<div class="left-sidebar">
						<h2>Artikel</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
					 <div class="panel panel-default">
					 	<!--/category-products-->
								<div class="panel-heading">
												@if(!empty($data['latest_one']))			
								<br>	
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											{!! strlen($data['latest_one']->title > 20)?substr($data['latest_one']->title,0,20).'...':$data['latest_one']->title !!}
										</a>

									</h4><br>	 	
											<p> {!! strlen(strip_tags($data['latest_one']->description))>100?substr(strip_tags($data['latest_one']->description),0,100).'...':strip_tags($data['latest_one']->description) !!}
						</p>	<div class="col-md-12 text-center">
						<a href="{!! route('articles',[$data['latest_one']->id.'-'.$data['latest_one']->title]) !!}" class="btn btn-default get" style="width:120px">Selengkapnya</a>									 
					</div>
								</div>


							</div>
							 @endif


						</div>
						<!--/category-products--> 	<div class="shipping text-center"><!--shipping-->
							 <img src="{!! asset('theme/citra_V2/images/artikel/front-artikel.jpg') !!}" alt="">
						</div><!--/shipping-->		


 	</div>				
					 				 
 </div>
				<script type="text/javascript">
			$(function () {
				var value = $("#ms_country_id").val();
				$('.img-lazy').unveil();
				cityField(value);
			});
			function cityField( value)
			{
				var general_country_id = {!! $data['general']->ms_country_id !!};
				if(value == general_country_id)
				{
					$('#city-container').show();
				}else{
					$('#city-container').hide();
				}
			}
			$(document).ready(function() {
				$('#ms_country_id').selectize();      
				$('#ms_city_id').selectize();      
				$('#ms_country_id').change(function()
				{
					cityField($(this).val());
				});
				
				$('#form-validate').validate({
					errorClass : "help-block error",
					rules: {
						username : {
							required: true,
							minlength: 3
						},
						email :{
							required: true,
							email : true
						}
					},
					messages: {
						username : {
							required : "Mohon masukkan username Anda",
							minlength : $.validator.format("Username Anda harus minimal {0} karakter")
						},
						email : {
							required : "Mohon masukkan email Anda",
							email : "Mohon cek kembali alamat email Anda"
						}
					}
				});
			});

		</script>