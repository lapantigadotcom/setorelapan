<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ThirdParty\Ongkir;

use Illuminate\Http\Request;
use Auth;

class ShippingController extends BasicController {

	function getService(Request $request)
	{
		$courier_id = $request->input('courier_id');
		$courier = \App\Courier::find($courier_id);
		$ongkir = new Ongkir($this->api_shipping);
		$user = \App\User::find(Auth::id());
		$order = \App\Order::
			with('detailOrders','detailOrders.product')
			->where('ms_status_order_id','1')
			->where('ms_user_id',Auth::id())
			->first();
		$weight = 0;
		foreach ($order->detailOrders as $row) {
			$weight += $row->total * $row->product->weight;
		}
		$ongkir = new Ongkir($this->api_shipping);
		$service = $ongkir->getCost(intval($this->origin),$user->ms_city_id,intval($weight*1000),$courier->code);
		$min_obj = array();
		foreach ($service->{'rajaongkir'}->{'results'} as $row) {
			foreach ($row->{'costs'} as $val) {
				$tmp = array();
				array_push($tmp, $val->service);
				array_push($tmp, intval($val->cost[0]->value));
				array_push($tmp, intval($val->cost[0]->etd));
				array_push($min_obj, $tmp);
			}
		}
		return response()->json($min_obj);
	}

	public function setService(Request $request)
	{
		$service = $request->input('service');
		$price = $request->input('price');
		$courier_id = $request->input('courier_id');
		$order = \App\Order::with('courier')->where('ms_status_order_id',1)->where('ms_user_id', Auth::id())->first();
		$order->service_courier = $service;
		$order->ms_courier_id = $courier_id;
		$order->shipping_price = $price;
		$order->save();
		return response()->json($order);
	}

	public function trakingJNE(){
		$data = $this->data;
		return view('theme.'.$this->theme->code.'.pages.tracking',compact('data'));

 	}

 
	public function posTraking(Request $request){
		$panah = $request->input('resi');
		$data = $this->data;
		$data['result'] = $this->apiTracking($panah);
		return view('theme.'.$this->theme->code.'.pages.tracking',compact('data'));
	}


	public function apiTracking($td){
		$small = new Ongkir($this->api_shipping);
		$luffy = $small->mainmain($td);
		$apiRajaongkir = array();
		if(!empty($luffy)){
			$apiRajaongkir['status_code'] = $luffy->{'rajaongkir'}->{'status'}->{'code'};
			if ($apiRajaongkir['status_code'] == 200) {
				$apiRajaongkir['delivered'] = $luffy->{'rajaongkir'}->{'result'}->{'delivered'};
				$apiRajaongkir['courier_name'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'courier_name'};
				$apiRajaongkir['service_code'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'service_code'};
				$apiRajaongkir['waybill_number'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'waybill_number'};
				$apiRajaongkir['status'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'status'};
 				$apiRajaongkir['pod_receiver'] = $luffy->{'rajaongkir'}->{'result'}->{'delivery_status'}->{'pod_receiver'};
 				$apiRajaongkir['pod_date'] = $luffy->{'rajaongkir'}->{'result'}->{'delivery_status'}->{'pod_date'};
 				$apiRajaongkir['pod_time'] = $luffy->{'rajaongkir'}->{'result'}->{'delivery_status'}->{'pod_time'};


				$apiRajaongkir['waybill_time'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'waybill_date'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'waybill_time'};
				$apiRajaongkir['shipper_name'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'shipper_name'};
				$apiRajaongkir['shipper_address'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address1'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address2'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address3'};

				$apiRajaongkir['receiver_name'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_name'};
				$apiRajaongkir['receiver_address'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_city'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_address2'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_address3'};
				$tmp = array();
				foreach ($luffy->{'rajaongkir'}->{'result'}->{'manifest'} as $row) {
					
					$temp=array(	"description" 		=> $row->manifest_description, 
											"time" 	=> $row->manifest_date." ".$row->manifest_time,
											"city" 	=> $row->city_name
											);
					array_push($tmp, $temp);
				}
				$apiRajaongkir['manifest'] = $tmp;
			}
		}
		return $apiRajaongkir;
	}
function interDesti(){
		// $arrayName = array();
		// array_push($arrayName, "internationalOrigin");
		// array_push($arrayName, "");
		// $small = new Ongkir($this->api_shipping);
		// $luffy = $small->cinta($arrayName);
		// foreach($luffy->{'rajaongkir'}->{'results'} as $row){
		// 	$data = new \App\CitySupport;
		// 	$data->city_id = $row->city_id;
		// 	$data->name = $row->city_name;
		// 	$data->save();
		// }
		// dd($luffy);
		$arrayName = array();
		array_push($arrayName, "internationalDestination");
		array_push($arrayName, "");
		$small = new Ongkir($this->api_shipping);
		$luffy = $small->gakserius($arrayName);
		
		foreach($luffy->{'rajaongkir'}->{'results'} as $row){
			$data = new \App\Country;
			$data->country_id = $row->country_id;
			$data->name = $row->country_name;
			$data->save();
		}
		dd($luffy);
		
	}

	public function getOngkirInt()
	{
		$data['city'] = \App\CitySupport::all();
		$data['province'] = \App\Province::all();
		$data['country'] = \App\Country::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir-international",compact('data'));
	}

	public function postOngkirInt(Request $request)
	{
		$source = $request->input('source');
		$destination = $request->input('destination');
		$weight = intval($request->input('weight'));
		$courier = 'pos';
		$source_obj = \App\CitySupport::where('city_id',$source)->first();
		$destination_obj = \App\Country::where('country_id',$destination)->first();


		$temp = $this->calculateShippingInt($source, $destination, $weight, $courier);
		if(!empty($temp)){
			$data['results']['cost'] = $temp[0];
			$data['results']['currency'] = $temp[1];
		}else{
			$data['results'] = null;
		}
		
		$data['context'] = "Hasil pengecekan dari <b>".$source_obj->name."</b> ke <b>".$destination_obj->name." </b> @<b>".$weight."</b> gram<br><br> <b>POS INDONESIA</b>";
		$data['isian_source'] = $source;
		$data['isian_destination'] = $destination;
		$data['isian_courier'] = $courier;
		$data['isian_weight'] = $weight;

		$data['city'] = \App\CitySupport::all();
		$data['country'] = \App\Country::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir-international",compact('data'));
	}
}
