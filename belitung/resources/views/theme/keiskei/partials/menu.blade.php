<header id="header"><!--header-->
		<div class="header_top" style="background:#6CC263"> <!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> {!! $general->telephone !!}
</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> {!! $general->email !!}</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
							<li><a href="https://www.facebook.com/citra.florist" target="_blank"><i  title="FB: citra_florist" class="fa fa-facebook-square fa-lg"  ></i></a></li>
							<li><a href="https://twitter.com/citra_florist" target="_blank"><i  title="@citra_florist" class="fa fa-twitter-square fa-lg"  ></i></a></li>
 							<li><a href="https://instagram.com/citraflorist" target="_blank"><i title="citra_florist" class="fa fa-instagram fa-lg" ></i></a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
 			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12">
						<div class="shop-menu pull-left">
							<ul class="nav navbar-nav">			
				 
								@if(Auth::check() == true)
								<li><a href="{!! route('user.dashboard') !!}"><i class="fa fa-dashboard fa-lg"></i> Dashboard <i style="font-size:11px;color:red">(View Profile)</i></a></li>
								<li><a href="{!! route('user.logout') !!}"><i class="fa fa-power-off fa-lg"></i> Log Out</a></li>
								@else
								<li><a href="{!! route('login') !!}"><i class="fa fa-lock"></i> Login</a></li>
								<li><a href="{!! route('register') !!}"><i class="fa fa-user"></i> Pendaftaran</a></li>
								@endif
							 
							</ul>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12">
                                <!--logo start-->
                                <div class="logo text-center">
						<img  src="{!! asset('theme/citraflorist/images/citra_florist_logo.png') !!}"  >
                                    </a>
                                </div>
                                <!--logo end-->
                            </div>

					<div class="col-lg-4 col-md-4 col-sm-12">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">			
				  
								<li><a href="{{ url('/password/email') }}"><i class="fa fa-key"></i> Reset Password</a></li>
								<li><a href="{!! route('contact-us') !!}"><i class="fa fa-envelope-o"></i> Hubungi Kami</a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
 	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="{!! URL::to('/') !!}" class="active">Home</a></li>
								@if(isset($articles[0]['id']))<li><a href="{!! route('articles',[$articles[0]['id'].'-'.$articles[0]['title']]) !!}">Tentang Kami</a></li> @endif
								<li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}">Produk</a></li>

								<li class="dropdown"><a href="#">Panduan<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
								@if(isset($articles[11]['id']))<li><a href="{!! route('register') !!}">Pendaftaran</a></li>@endif
								@if(isset($articles[9]['id']))<li><a href="{!! route('articles',[$articles[9]['id'].'-'.$articles[9]['title']]) !!}">Pemesanan</a></li>@endif
								@if(isset($articles[10]['id']))<li><a href="{!! route('articles',[$articles[10]['id'].'-'.$articles[10]['title']]) !!}">Pembayaran</a></li>@endif
								@if(isset($articles[12]['id']))<li><a href="{!! route('articles',[$articles[12]['id'].'-'.$articles[12]['title']]) !!}">Konfirmasi</a></li>@endif
								@if(isset($articles[13]['id']))<li><a href="{!! route('articles',[$articles[13]['id'].'-'.$articles[13]['title']]) !!}">Withdraw</a></li>@endif
    								</ul>
 										<li><a href="{!! route('blogs') !!}">Blog</a></li>
                                </li> 
                        		@if(isset($articles[2]['id']))<li><a href="{!! route('articles',[$articles[2]['id'].'-'.$articles[2]['title']]) !!}">Bisnis K-Partner</a></li>@endif

						<li><a href="{!! route('partner-area') !!}">Partner Area</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class=" pull-right">
							  <a href="{!! route('user.cart') !!}" style="color:#0C3121;font-size:16px"><i class="fa fa-shopping-cart"></i> Cart
							  	 </a> 

 						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->