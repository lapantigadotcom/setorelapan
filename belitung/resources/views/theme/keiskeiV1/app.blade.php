<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
 
 





  @include('theme.keiskei.partials.head')
  <style>
	.goog-te-gadget-simple {background-color:#F5F5F5;border:none;font-size:11px;}
	.goog-te-gadget-simple img{display:none;}
</style>
<!-- COMODO SSL KEISKEI -->
<script type="text/javascript">
//<![CDATA[
var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "https://www.trustlogo.com/");
document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>



</head>
<body>
	@include('theme.keiskei.partials.menu')
	<div class="container-fluid">
		<div class="row">
			
		</div>
		<div id="page-content">
		@yield('content')
		</div>
		@include('theme.keiskei.partials.footer')
	</div>
  @include('theme.keiskei.partials.script')		
</body>
</html>