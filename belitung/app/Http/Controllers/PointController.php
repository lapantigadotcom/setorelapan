<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PointRequest;
use App\Http\Requests\RegistrationFeeRequest;
use App\Point;
use App\Role;
use Illuminate\Http\Request;

class PointController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Role::with('point')->where('require_file','1')->get();
		return view('pages.point.index',compact('data'));
	}
	public function registrationFee()
	{
		$data['content'] = Role::with('point')->where('require_file','1')->get();
		return view('pages.registrationfee.index',compact('data'));
	}
	public function registrationFeeEdit($id)
	{
		$data['content'] = Role::with('point')->find($id);
		return view('pages.registrationfee.form',compact('data'));
	}
	public function registrationFeeStore(RegistrationFeeRequest $request)
	{		
		Point::create($request->all());
		return redirect()->route('ki-admin.registrationfee.index');
	}
	public function registrationFeeUpdate(RegistrationFeeRequest $request, $id)
	{
		$data = Point::find($id);
		$data->update($request->all());
		return redirect()->route('ki-admin.registrationfee.index');
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PointRequest $request)
	{
		Point::create($request->all());
		return redirect()->route('ki-admin.point.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Role::with('point')->find($id);
		return view('pages.point.form',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(PointRequest $request, $id)
	{
		$data = Point::find($id);
		$data->update($request->all());
		return redirect()->route('ki-admin.point.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
