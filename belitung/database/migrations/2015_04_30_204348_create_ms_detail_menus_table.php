<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsDetailMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_detail_menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 64)->nullable();
			$table->string('name', 64)->nullable();
			$table->integer('ms_menu_id')->index('fk_ms_detail_menu_ms_menu1_idx');
			$table->integer('owned_id')->nullable();
			$table->string('owned_type', 64)->nullable();
			$table->integer('parent_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_detail_menus');
	}

}
