<!DOCTYPE html>
<html style="background-color: #e4e4e4; ">
 


	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		{!! $data['content']->code !!} | KeisKei Indonesia | INVOICE
	</title>


<link rel="stylesheet" type="text/css" href="{!! asset('theme/keiskei/css/invoice.css') !!}">
{!! HTML::style('theme/keiskei/plugin/fontawesome/css/font-awesome.min.css') !!}
	{!! HTML::script('theme/keiskei/js/jquery-1.11.2.min.js') !!}
	

 <style type="text/css">
 	.label {display: inline;
padding: 0.1em 0.1em 0.2em;
font-size: 14px;
line-height: 1;
color: #FFF;
text-align: center;
white-space: nowrap;
vertical-align: baseline;
border-radius: 0.25em;}

.label-success {background-color: #5CB85C}



 </style>

</head>
<body>
	@include('theme.keiskei.scripts.price-product')
	@if(Auth::user()->roles->first()->locked == '1')
	<?php 
	if($data['content']->user->roles->first()->level == '5')
		setlocale(LC_ALL, "en_US.UTF-8");
	?>
	@endif
	 <header class="clearfix">
      <div id="logo">
<a class="navbar-brand visible-lg visible-md" href="{!! URL::to('/') !!}">
	      	<img src="{!! asset('theme/keiskei/img/logo.png') !!}" class="img-responsive hidden-xs main-logo">
	      </a>      </div>
      <div id="company">
        <h2 class="name" style="text-transform:uppercase">PT. Punyakunik Maju Jaya</h2>
        <div> Jl. Raya Mutiara 223 / Kav.M No. 94A Ruko PPS, Manyar Gresik - Indonesia</div>
        <div>

        <b>   +62.811.330.9919

</b>
</div>
        <div><a href="mailto:info@keiskei.co.id">info@keiskei.co.id</a></div>
      </div>
      </div>
    </header>


     <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Kepada YTH:</div>
          <h2 class="name" style="text-transform:uppercase">{!! $data['content']->user->name !!}</h2>
          <div class="address">{!! $data['content']->user->address !!}<br>
            @if($data['content']->user->roles->first()->level!='5')
                {!! $data['content']->user->city->name !!}, <br>{!! $data['content']->user->city->province->name !!} <br>
                @endif
                Telp. {!! $data['content']->user->telephone !!}&nbsp; HP: {!! $data['content']->user->mobile !!} 
 </div>
          <div class="email"><a href="mailto:{!! $data['content']->user->email !!} ">{!! $data['content']->user->email !!} </a></div>


         </div>
        <div id="invoice">
          <h1>#{!! $data['content']->code !!}</h1>
          <div class="date">Invoice Date: {!! date('j F Y', strtotime($data['content']->created_at)) !!}
</div>
<a href="#"  onClick="window.print()"><b> <i class="fa fa-print"></i> Cetak</b></a>
				<div class="row">
					
				
				</div>	
				</div>
				
			</div>
			@if(!empty($data['content']->ms_payment_id))

	 <header class="clearfix"></header>
			<div class="row">
				<div class="clearfix">
					<b>
						Pembayaran ke :</b> {!! $data['content']->payment->name !!} {!! $data['content']->payment->account_number !!} - a.n. {!! $data['content']->payment->account_name !!}
					
					 
				</div>
				<div class="colspan">
					@if($data['content']->transfers->count() > 0)
					@foreach($data['content']->transfers as $val)
					<em>					  <span style="font-size:13px; color:red;font-style:italic">
Konfirmasi pembayaran telah dilakukan: {!! money_format('%(#10n', $val->nominal) !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</span></em>
					@if($val->status == '1') &nbsp;|&nbsp; Status:
					<span class="label label-success"> Disetujui </span>
					@elseif($val->status == '2')
					<b> Tidak Disetujui </b>
					@else
					<b> Pending </b>
					@endif
					<br>
					@endforeach
					@endif
				</div>
			</div>
			@endif
			<hr>
			<div class="row">
				<div class="date">
					 
Halo, <span style="font-size:18px; color:green;font-weight:bold;text-transform:uppercase">{!! $data['content']->user->username !!}</span>&nbsp;. 
					Terima kasih, Anda telah melakukan pemesanan sebagai berikut: 
				</div> <br>
				<table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">QTY</th>
            <th class="desc">NAMA PRODUK</th>
            <th class="unit">BERAT</th>
            <th class="qty">HARGA</th>
            <th class="total">SUBTOTAL</th>
          </tr>
        </thead>
        <tbody>

            <?php
              $total = 0;
              $weight = 0;
              ?>
              @foreach($data['content']->detailOrders as $row)
          <tr><tr>
								
									      <td class="no">
{!! $row->total !!}</td>
            <td class="desc">
									<h3>{!! $row->product->title !!}</h3>
										@if($row->detailAttributeProductDetailOrders->count() > 0)
										@foreach($row->detailAttributeProductDetailOrders as $val)
										<span>

											: 
											<b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b>
										</span>
										@endforeach
										@endif
									</td>
									<td class="unit">{!! $row->product->weight !!} kg</td>
									<td class="qty">
										<?php 
										$weight += $row->total*$row->product->weight;
										if(Auth::user()->roles->first()->locked != '1')
											$total_t = priceDisplay($data['default_role'], $row->product->prices); 
										else
										{

											$total_t = priceDisplayAdmin($data['content']->user, $data['default_role'], $row->product->prices); 
										}

										?>
										<span>
											{!! money_format('%(#10n', $total_t) !!}
										</span>
										<br>
										<span class="small">
											<del><?php $def = 0; if(Auth::user()->roles->first()->locked != '1') 
												$def= defaultProduct($data['default_role'], $row->product->prices);
												else
													$def = defaultProductAdmin($data['content']->user, $data['default_role'], $row->product->prices);
												?>
												<span style="font-size:10px">{!! money_format('%(#10n', $def) !!}</span>
											</del>
										</span>
										<?php 
										if(!empty($row->product->discount))
										{
											$discount_t = ($total_t/100)*intval($row->product->discount);
											echo "<br><b>Diskon ".$row->product->discount." %</b>";
											$total_t = $total_t - $discount_t;
										}
										$total_t = $total_t*$row->total;
										$total += $total_t;
										?>
									</td>
									<td class="total"> {!! money_format('%(#10n', $total_t) !!}										
									</td>
								
								</tr>
								<td colspan="2"></td>
            					 								<td colspan="2"></td>


        					  	</tr>

								<tr>
								@endforeach
								@if($data['content']->user->roles->first()->level!='5')
								<tr>					


 

			<td class="no"></td>
            <td class="desc"> {!! $data['content']->courier->name !!} -  {!! $data['content']->service_courier !!}

			</td>
            <td class="unit">{!! $weight !!} kg</td>
            <td class="qty"></td>

            <td class="total"> {!! money_format('%(#10n', $data['content']->shipping_price) !!}
			</td>
			

			</tbody>
			<tfoot>
			<td colspan="2"></td>
			<td colspan="2"></td>
			<td colspan="2"></td>
			 
			@endif
			<tr>
			<td colspan="2"></td>
			<td colspan="2"><h4>Total Pembayaran</h4></td>
			<td colspan="2">
			<span class="date">{!! money_format('%(#10n', $total+intval($data['content']->shipping_price)+ (($total)) )  !!}
			</span>
			</td>
			</tr>        
 			</tfoot>
			</table>

	@if($data['content']->ms_status_order_id == '8')
												 <div class="clear"></div>

					 <span class="date">Pembelian Anda dilayani Oleh </span><span style="font-size:12px; color:green;font-weight:bold">{!! $data['content']->statusOrder->name !!}</span>&nbsp;KEISKEI :
						
					 

					@if($data['content']->reseller()->count() > 0)
					 <br>
						 <span style="font-size:18px; color:green;font-weight:bold">{!! $data['content']->reseller->name !!}</span>
						<br>
						@if($data['content']->reseller->accountBank()->count())
						 
							<b>
								Akun Rekening :
							</b>
						 
						<ul>
							@foreach($data['content']->reseller->accountBank as $row)
							<li>
								BANK:{!! $row->bank->name !!} - {!! $row->account_name !!}
							</li>
							@endforeach
						</ul>
						<br>
						@endif
						{!! $data['content']->reseller->address !!}
						<br>
						{!! $data['content']->reseller->city->name !!}, {!! $data['content']->reseller->city->province->name !!}
						<br>
						Telp : {!! $data['content']->reseller->telephone !!}
						<br>{!! $data['content']->reseller->email !!}
						</div>
					</div>
						
					@endif
					@else
					  <span style="font-size:18px; color:green;font-weight:bold">
						Status Order :{!! $data['content']->statusOrder->name !!}
					</span>
					@endif
					
<div style="float:right">

<img src="{!! asset('theme/keiskei_V2/images/logobank.png') !!}">
</div>
					<footer>

Copyright © PT. Punyakunik Maju Jaya. All Rights Reserved. info@keiskei.co.id .<span style="color:green">{!! $data['content']->code !!}  </span>
</footer>
	</body>
	</html>