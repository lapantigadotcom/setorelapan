<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\NotifEmail;
use App\Http\Requests\NotifEmailRequest;
use Auth;
use Mail;
use App\User;
use App\Role;
use Input;

class NotifEmailController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = NotifEmail::with('recipient')->where('ms_user_id', '=', Auth::id())->get();
		return view('pages.notif-email.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('0' => 'Individual', '1' => 'Group');
		$data['receiver'] = User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('email', 'email');
		return view('pages.notif-email.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotifEmailRequest $request)
	{
		$data['ms_user_id'] = Auth::user()->id;
		$data['group'] = $request->input('group');
		$data['subject'] = $request->input('subject');
		$data['content'] = $request->input('content');
		if($data['group'] == '0') {
			$data['receiver'] = User::find($request->input('receiver'))->email;
			if($this->sendNotif($data)) {
				$data['sent'] = '1';
				$data['draft'] = '0';
				$data['ms_user_recipient_id'] = $request->input('receiver');
				NotifEmail::create($data);
			}
		}
		else if($data['group'] == '1') {
			$receivers = User::whereHas('roles', function($q) use($request) {
				$q->where('ms_roles.id', '=', $request->input('receiver'));
			})->get();
			foreach($receivers as $receiver) {
				$data['receiver'] = $receiver->email;
				if($this->sendNotif($data)) {
					$data['sent'] = '1';
					$data['draft'] = '0';
					$data['ms_user_recipient_id'] = $receiver->id;
					NotifEmail::create($data);
				}
			}
		}
		return redirect()->route('ki-admin.notif-email.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = NotifEmail::find($id);
		return view('pages.notif-email.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		NotifEmail::destroy($id);
		return redirect()->route('ki-admin.notif-email.index');
	}

	protected function sendNotif($data)
	{
		if(Mail::send('theme.'.$this->theme->code.'.include.notif-email',$data, function($message) use($data)
		{
			$message->subject($data['subject']);
			$message->to($data['receiver']);
		})) {
			return true;
		}
		else {
			return false;
		}
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}

}
