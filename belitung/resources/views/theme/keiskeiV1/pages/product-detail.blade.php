@extends('theme.keiskei.app')



@section('content')
<title>Keiskei Indonesia | {!! $data['content']->categoryproduct->name !!} | {!! $data['content']->title !!}</title>
<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="{!! $data['content']->description !!} " />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta property="og:title" content="Keiskei Indonesia | {!! $data['content']->categoryproduct->name !!} | {!! $data['content']->title !!}" />
<meta property="og:description" content="{!! $data['content']->description !!}" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="{!! $data['content']->description !!}">
<meta name='robots' content='INDEX, FOLLOW' />
@include('theme.keiskei.scripts.price-product')
@include('theme.keiskei.include.review-content',['id' => 'review-content', 'reviews' => $data['content']->reviews ])


	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>{!! $data['content']->title !!}
			</h2>
			<h5><strong>{!! $data['content']->categoryproduct->name !!} </strong></h5>
		</div>
	</div>


<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<ul id="etalage">
							@if($data['content']->productImages()->count() > 0)
							@foreach($data['content']->productImages as $row)
							@if(File::exists($row->image))
							<li>
								<!-- This is the large (zoomed) image source: -->
								<img class="etalage_source_image" src="{!! asset($row->image) !!}" title="© PT. Punyakunik Maju Jaya" />
								<!-- This is the thumb image source (if not provided, it will use the large image source and resize it): -->
								<img class="etalage_thumb_image" src="{!! asset($row->image) !!}" />
							</li>
							@endif

							@endforeach
							@endif
						</ul> 
						

					</div>


					<div class="col-md-6 product-detail">
						<div class="product-title">
							<h4>
								{!! $data['content']->title !!}
							</h4>
							<div class="row rate">
								<div class="star">
									@for($i=1; $i<=intval($data['avg_review']); $i++ )
									<i class="fa fa-star fa-lg"></i>
									@endfor
									@for($i=intval($data['avg_review']); $i<=5; $i++ )
									<i class="fa fa-star-o fa-lg"></i>
									@endfor
									<span> {!! $data['content']->reviews()->count() !!} Review</span>	
								</div>
								<span class="divider"></span>
							</div>
						</div>
						<div class="product-description">
							<p> Kategori : <strong>{!! $data['content']->categoryproduct->name !!}</strong> </p>
							<p> Ketersediaan : <span>Indent</span> </p>
				<!--			<p> Ketersediaan : <span>{!! $data['content']->statusproduct->name !!}</span> </p> -->
							<p> Kode Produk : {!! $data['content']->code !!} </p>
							<h4>Kemasan</h4>
							<p> Dimensi :  {!! $data['content']->length !!} cm</p>
							<p> Berat :  {!! $data['content']->weight !!} Kg</p>
							<h4>Kandungan</h4>
							<p> {!! $data['content']->bahan !!}</p>


							<span class="divider">&nbsp;</span>
						</div>
						{!! Form::open(array('route' => ['user.cart.store',$data['content']->id],'method' => 'post')) !!}
						<div class="product-attribute">
							<?php
							$id_att = -1;
							?>
							@foreach($data['content']->detailAttributeProducts as $row)
							@if($row->detailAttribute->ms_attribute_id != $id_att)

							<?php
							$id_att = $row->detailAttribute->ms_attribute_id;
							$d_attribute_t = $data['content']->detailAttributeProducts->filter(function($f) use($id_att) {
								if($f->detailAttribute->ms_attribute_id == $id_att && $f->enabled=='1')
								{
									return true;
								}
							});
							$arr_data = array();
							foreach ($d_attribute_t as $val) {

								$arr_data[$val->detailAttribute->id] = $val->detailAttribute->name;
							}
							?>
							<div class="form-group">
								{!! Form::label($row->detailAttribute->attribute->name, $row->detailAttribute->attribute->name, ['class' => 'form-label']) !!}
								@if($row->detailAttribute->attribute->typeattribute->code == 'SB')
								{!! Form::select('att'.$row->detailAttribute->attribute->id,$arr_data,null,['class' => 'form-control']) !!}
								@elseif($row->detailAttribute->attribute->typeattribute->code == 'CB')
								<div class="checkbox">
									@foreach($arr_data as $key => $val)
									<label>

										<input type="checkbox" name="{!! 'att'.$row->detailAttribute->attribute->id !!}[]" value="{!! $key !!}">
										{!! $val !!}&nbsp;&nbsp;
									</label>
									@endforeach
								</div>
								@elseif($row->detailAttribute->attribute->typeattribute->code == 'RB')
								<div class="radio">
									@foreach($arr_data as $key => $val)
									<label>
										{!! Form::radio('att'.$row->detailAttribute->attribute->id, $key) !!}
										{!! $val !!}
									</label>
									@endforeach
								</div>
								@endif
							</div>
							@endif
							@endforeach 
						</div>
						<div class="product-action">
							<h4 class="price">
								<?php
								if(Auth::check())
								if(Auth::user()->roles->first()->level == '5'){
							      setlocale(LC_ALL, "id_ID");
							    }else{
							      setlocale (LC_MONETARY, 'id_ID');
							    }
								?>
								Harga: {!! money_format('%(#10n', priceDisplay($data['default_role'], $data['content']->prices))  !!}
							</h4>
							<div class="action-list">
								<button type="submit " class="btn hvr-sweep-to-top"><i  title="Beli produk ini" class="fa fa-shopping-cart fa-lg"></i> Add to cart</button>
								<a class="btn btn-default hvr-sweep-to-top" title="Tulis Ulasan" data-toggle="modal" data-target="#review"><i class="fa fa-edit"></i></a>
								<a class="btn btn-default hvr-sweep-to-top" title="Daftar Ulasan produk" data-toggle="modal" data-target="#review-content"><i class="fa fa-exchange"></i></a>
								<a class="btn btn-default hvr-sweep-to-top"  title="FAQ produk" data-toggle="modal" data-target="#help"><i class="fa fa-question"></i></a>
								@if(isset($data['content']->files))
								@if(!empty($data['content']->files))
								@if(File::exists($data['content']->files))
								<a class="btn btn-default hvr-sweep-to-top"  title="Download File Product" target='_blank' href="{!! asset($data['content']->files) !!}"><i class="fa fa-download"></i></a>
								@endif
								@endif
								@endif
<!-- <p><p><p style="font-size:12px; color:green">(Harga Sudah termasuk PPN 10%)</p> -->
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />
							</div>
							<script type="text/javascript">

								buttons: [{
									text: "Ok",
									id : "btnModalDialog",
									Click : function () {                    
										var isOpen = $("#dialog-confirm").dialog("isOpen");
										if (isOpen == true) {
											okCallBack();
										}
										$("#dialog-confirm").dialog("close");
									}
								}]


							</script>
							<br><!-- sharing sosmed -->
 							<div class="addthis_sharing_toolbox"></div>
						</div>
						{!! Form::close() !!}
						@include('theme.keiskei.include.help',[ 'help' => $data['content']->help])
						@include('theme.keiskei.include.review',[ 'idP' => $data['content']->id,'id' => 'review'])
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist" id="myTab">
							<li role="presentation"><a href="#review-tab" aria-controls="review-tab" role="tab" data-toggle="tab">Ulasan</a></li>
							<li role="presentation"><a href="#specification-tab" aria-controls="specification-tab" role="tab" data-toggle="tab">Indikasi</a></li>
							<li role="presentation" class="active"><a href="#decription-tab" aria-controls="decription-tab" role="tab" data-toggle="tab">Deskripsi</a></li>

						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="decription-tab">
								<p>
									<br>{!! $data['content']->description !!}
								</p>
							</div>
						
						<div role="tabpanel" class="tab-pane" id="specification-tab">
							<p>
								<br>
								{!! $data['content']->specification !!}
							</p>
						</div>
						<div role="tabpanel" class="tab-pane" id="review-tab" >
							<hr>
							@foreach($data['content']->reviews as $review)
							<div class="review-detail">
								<div class="review-item">
									<div class="review-item-head">
										<div class="row">
											<div class="col-md-8 text-left review-item-inline">
												<span>{!! $review->user->name !!}</span>
												<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($review->created_at)) !!} <i class="fa fa-bell-o"></i> {!! date('l', strtotime($review->created_at)) !!}
												<i class="fa fa-calendar-o"></i>  {!! date('j F', strtotime($review->created_at)) !!}
											</div>
											<div class="col-md-4 text-right">
												<div class="rate">
													<div class="star">
														@for($i=0; $i<intval($review->rate); $i++ )
														<i class="fa fa-star fa-lg"></i>
														@endfor
														@for($i=intval($review->rate); $i<=5; $i++ )
														<i class="fa fa-star-o fa-lg"></i>
														@endfor
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="review-item-body">
										<p>
											{!! $review->description !!}
										</p>
									</div>
								</div>
							</div>

							@endforeach
						</div>
					</div>
					</div>
					<script>
						  $(function () {
						    $('#myTab a:last').tab('show')
						  })
						</script>
				</div>
			</div>

			<div class="col-md-4" style="max-width:24%;margin:20px">
				<div class="sidebar hidden-xs hidden-sm">
					<div class="sidebar-head">
						<i  class="fa fa-book"></i>&nbsp; | &nbsp; KATEGORI PRODUK
					</div>
					<div class="sidebar-body">
						<ul>
							@foreach($data['categoryproduct'] as $key => $value)
							<li>
								<a href="{!! route('product', [$key.'-'.$value]) !!}"><i class="fa fa-caret-right"></i> &nbsp;{!! $value !!} </a>
							</li>
							@endforeach
						</ul>
					</div>
				</div>

				  <div class="sidebar hidden-xs hidden-sm">
					<div class="sidebar-head ">
						<i  class="fa fa-youtube"></i>&nbsp; | &nbsp; Subscribe Us on Youtube
					</div>
					<div class="sidebar-body">
						<ul>
							<script src="https://apis.google.com/js/platform.js"></script>

						<div class="g-ytsubscribe" data-channelid="UCq198Uk7sLNLWPpwdsCDwyw" data-layout="full" data-count="default"></div>
						</ul>
					</div>
				</div> 

				<div class="sidebar hidden-xs hidden-sm">
					<div class="sidebar-head ">
						<i  class="fa fa-tag"></i>&nbsp; | &nbsp; Keiskei Banner Produk
					</div>
					<div class="sidebar-body">
						<ul>
						<img src="{!! asset('theme/keiskei/img/Stimulate 1.jpg') !!}" width="250px" style="border-radius:18px;"class="img-responsive">
		<!--			<br>	-->
						<img src="{!! asset('theme/keiskei/img/keiskei-sidebar.png') !!}" class="img-responsive">

						</ul>
					</div>
				</div> 

				





		</div>
		</div>
		</div></div>
		
@include('theme.keiskei.partials.best-products')


@endsection

@section('custom-footer')




<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

{!! HTML::script('theme/keiskei/plugin/etalage/js/jquery.etalage.min.js') !!}

<script type="text/javascript">
	$(document).ready(function() {
		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 300,
			source_image_width: 1200,
			source_image_height: 1600,
			zoom_area_width: 450,
		});
		$("#product-slider").owlCarousel({
			items : 5,
			lazyLoad : true,
			navigation : true,
			pagination : false,
			navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
		}); 
	});
</script>

@endsection

@section('custom-head')
{!! HTML::style('theme/keiskei/plugin/etalage/css/etalage.css') !!}


@endsection