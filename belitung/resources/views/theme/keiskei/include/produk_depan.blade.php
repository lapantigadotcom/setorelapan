         <div class="section-title">
                        <h2 style="font-size:4em">Citra Florist</h2>
                        <h3>Produk Kami</h3>
                    </div>
           <div class="col-lg-12">    
	          @foreach($data['latest'] as $row)
                <div class="col-lg-3">
                    <div class="single-product">
                        <a data-toggle="modal" title="Quick View" href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="view-single-product"><i class="fa fa-expand"></i></a>
                        <div class="product-thumb">
                            <span class="label-pro-sale">NEW</span>
                               @if($row->productImages()->count() > 0)
                                @if(File::exists($row->productImages->first()->image))
                                <img src="{!! asset($row->productImages->first()->image) !!}" alt="{!! $row->title !!}" style="max-height:233px"/>
                                @endif
                                @endif  
                        </div>
                        <div class="product-desc">
                            <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>

                            <div class="price-box floatleft">
                                <p class="normal-price">{!! ucwords($row->categoryproduct->name) !!}</p>
                            </div>
                             
                        </div>
                    </div><br>
                </div>  @endforeach 
            </div> 
                                     
        