<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Country;

class CountryTableSeeder extends Seeder {

    public function run()
    {
        Country::truncate();

        Country::create([
            'code' => 'ID',
            'name' => 'Indonesia',
        ]);
        Country::create([
            'code' => 'UK',
            'name' => 'England',
        ]);
    }

}