<div class="box-body">
	@if(isset($data['ms_attribute_id']))
		{!! Form::hidden('ms_attribute_id', $data['ms_attribute_id'],array('class' => 'form-control')) !!}
	@endif
	<div class="form-group">
		{!! Form::label('name','Name') !!}
		{!! Form::text('name',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>