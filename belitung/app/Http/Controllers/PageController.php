<?php namespace App\Http\Controllers;
 
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ContactUsRequest;
use Auth;
use App\General;
use Input;
use Session;

class PageController extends BasicController {
	public function index()
	{
		$data['theme'] = $this->theme;
		$data['general'] = $this->general;
		$data['latest'] = \App\Product::with('prices','brand','categoryproduct','productImages')->get()->take(10);
		$data['latest_one'] = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','<>','1');
				})->orderBy('created_at','desc')->orderBy('created_at','desc')->first();
		$data['country'] = Country::lists('name','id');		
		$data['city'] = \App\City::lists('name','id');
		$data['latest_article'] = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','<>','1');
				})->orderBy('created_at','desc')->orderBy('created_at','desc')->get()->take(4);
		return view('theme.'.$this->theme->code.'.pages.index',compact('data'));	
	}
	public function products($category = null)
	{
		$inputC = $category;
		if(empty($inputC)){
			$data['products'] = \App\Product::with('prices','brand','categoryproduct','productImages')->has('prices')->has('productImages')->paginate(12);
			$data['products']->setPath('products');
			Session::forget('categorySelect');
		}
		else{
			$idC = explode("-", $inputC);
			if($idC[0] != '0'){
				$data['products'] = \App\Product::
					with('prices','brand','categoryproduct','productImages')->
					has('prices')->
					has('productImages')->
					where('ms_category_product_id',$idC[0])
					->paginate(8);
				$data['products']->setPath($inputC);
			}
			else{
				$data['products'] = \App\Product::with('prices','brand','categoryproduct','productImages')->has('prices')->has('productImages')->paginate(8);
				$data['products']->setPath('products');
			}
			Session::put('categorySelect',$idC[0]);
		}
		
		$data['categoryproduct']= \App\CategoryProduct::lists('name','id');
		$data['default_role'] = \App\Role::where('default','1')->first();
		return view('theme.'.$this->theme->code.'.pages.product',compact('data'));
	}
	public function search()
	{
		$key = Input::get('search');
		$data['products'] = \App\Product::with('prices','brand','categoryproduct','productImages')->has('prices')->has('productImages')
			->where('title','like','%'.$key.'%')
			->where('description','like','%'.$key.'%')
			->paginate(8);
		$data['products']->setPath('products');
		$data['categoryproduct']= \App\CategoryProduct::lists('name','id');
		$data['default_role'] = \App\Role::where('default','1')->first();
		return view('theme.'.$this->theme->code.'.pages.product',compact('data'));
	}
	public function detailproduct($code, $name='')
	{
		$data['content'] = \App\Product::with(
			'detailAttributeProducts',
			'detailAttributeProducts.detailAttribute',
			'detailAttributeProducts.detailAttribute.attribute.typeAttribute',
			'prices',
			'prices.role',
			'brand',
			'categoryproduct',
			'productImages',
			'reviews',
			'reviews.user'
		)->where('code',$code)->first();
		if(empty($data['content']) || $data['content']->prices()->count() == 0)
			return redirect()->route('product');
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['categoryproduct']= \App\CategoryProduct::lists('name','id');
		$data['avg_review'] = \App\Review::where('ms_product_id', $data['content']->id)->where('enabled','1')->avg('rate');
		$data['latest_article'] = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','<>','1');
				})->orderBy('created_at','desc')->orderBy('created_at','desc')->get()->take(4);
		return view('theme.'.$this->theme->code.'.pages.product-detail',compact('data'));
	}

	public function page($page = 'index')
	{
		$data['country'] = Country::lists('name','id');		
		return view('theme.keiskei.pages.'.$page,compact('data'));
	}
	public function maintenance()
	{
		$data['content'] = General::first();
		return view('errors.503',compact('data'));
	}

	public function article($code = null)
	{
		if(empty($code))
			return redirect()->route('home');
		$a = explode('-', $code);
		$data['content'] = \App\Article::find($a[0]);
		return view('theme.'.$this->theme->code.'.pages.article',compact('data'));
	}
	public function contact()
	{
		$data = array();
		$data['general'] = \App\General::first();
		return view('theme.'.$this->theme->code.'.pages.contact-us',compact('data'));
	}
	public function contactPost(ContactUsRequest $request)
	{
		$data = \App\ContactUs::create($request->all());
		$this->sendEmail('4', $this->general->email, $data);
		Session::flash('success', 'Pesan anda telah berhasil dikirimkan');
		return redirect()->route('contact-us');
	}

	public function partner()
	{
		$data = array();
		$data['context'] = 'Partner Area';
		$data['role'] = \App\Role::where('require_file','1')->lists('name','code');
		$term = Input::get('group_type');
		if(empty($term))
		$data['content'] = \App\User::whereHas('roles',function($q){
			$q->where('require_file','1');
		})->where('enabled','1')->where('approved','1')->orderBy('created_at','desc')->paginate(8);
		else{
			switch ($term) {
				case 'ALL':
					$data['content'] = \App\User::whereHas('roles',function($q){
						$q->where('require_file','1');
					})->where('enabled','1')->where('approved','1')->orderBy('created_at','desc')->paginate(8);
					break;
				
				default:
					$data['content'] = \App\User::whereHas('roles',function($q) use($term){
						$q->where('require_file','1')->where('code',$term);
					})->where('enabled','1')->where('approved','1')->orderBy('created_at','desc')->paginate(8);
					break;
			}
			Session::put('group_type', $term);
		}
		return view('theme.'.$this->theme->code.'.pages.partner',compact('data'));
	}
	public function partnerShow($code = null)
	{
		if(empty($code))
		{
			return redirect()->route('home');
		}
		$data['context'] = 'Partner Area';
		$data['content'] = \App\User::where('code',$code)->first();
		if(empty($data['content']))
			return redirect()->route('home');
		return view('theme.'.$this->theme->code.'.pages.partner-show',compact('data'));	
	}
	public function blogs()
	{
		$data['content'] = \App\Article::whereHas('categoryArticle', function($q){
			$q->where('locked','<>','1');
		})->orderBy('created_at','desc')->paginate(4);
		$data['content']->setPath('blogs');
		return view('theme.'.$this->theme->code.'.pages.blog',compact('data'));
	}

	

}
