<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use Input;

class ChatMobileController extends BasicController {
	protected $GCM_API = 'AIzaSyDU78ChBtG2Wl0cdp8GQu567lOEA3EICZU';
	protected $path;
	function __construct() {
		
		$this->path = 'data/chat_mobile/';

		parent::__construct();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['list_unread'] = \App\ChatMobile::where('read_flag','<>','1')->where('is_admin','<>','1')->groupBy('ms_gcm_id')->lists('ms_gcm_id');

		$data['content'] = \App\ChatMobile::with('gcm')->where('parent', '0')->get();
		return view('pages.chatmobile.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('1' => 'Group');
		$data['receiver'] = \App\User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('email', 'email');
		return view('pages.chatmobile.create', compact('data'));
	}
	public function storeAdmin(Request $request)
	{
		$inp = array();
		$inp['description'] = $request->input('description');
		
		$gcm_id = $request->input('ms_gcm_id');
		$inp['is_admin'] = '1';
		$inp['ms_user_admin_id'] = Auth::user()->id;
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "_photo_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$inp['photo']= $fileName;
		}
		
		$inp['ms_gcm_id'] = $gcm_id;
		$parent_obj = \App\ChatMobile::where('ms_gcm_id', $gcm_id)->orderBy('created_at','desc')->first();
		if(empty($parent_obj))
		{
			$inp['parent'] = 0;
		}else{
			$inp['parent'] = $parent_obj->id;
		}
		$vb = \App\ChatMobile::create($inp);
		$chat_arr = $vb->toArray();
		$chat_arr['name'] = Auth::user()->name;
		$json = array();
		$json['RESP'] = "SCSCHT";
		$json['MESSAGE'] = 'Chat berhasil terkirim.';
		$json['DATA'] = $chat_arr;
		$gcm = \App\GCM::find($gcm_id);
		if(!empty($gcm))
		{
			$this->sendGCM($json, $gcm->gcm_id);
		}
		return redirect()->back();

	}
	public function store(Request $request)
	{

		$inp = array();
		$inp['description'] = $request->input('description');
		$inp['path_user'] = $request->input('path_user');
		$inp['name'] = $request->input('name');
		$inp['email'] = $request->input('email');
		$inp['handphone'] = $request->input('handphone');

		$gcm_id = $request->input('gcm_id');
		$gcm = \App\GCM::where('gcm_id', $gcm_id)->first();
		$user_id = $request->input('user_id');
		$anonymous = $request->input('is_anonymous');
		$inp['is_admin'] = '0';

		if(empty($gcm)){
			$gcm = new \App\GCM;
			$gcm->gcm_id = $gcm_id;
			
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}else{
			$gcm->gcm_id = $gcm_id;
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}

		$photo = $request->input('photo');
		if(!empty($photo))
		{
			$photo = urldecode($photo);
			$decoded=base64_decode($photo);
			$filename = "_ChatMobile_".date('Ymd_Hsi').'.JPEG';
			file_put_contents($this->path.$filename,$decoded);
			$inp['photo']  = $filename;		
			
		}
		$inp['ms_gcm_id'] = $gcm->id;
		$parent_obj = \App\ChatMobile::where('ms_gcm_id', $gcm->id)->orderBy('created_at','desc')->first();
		if(empty($parent_obj))
		{
			$inp['parent'] = 0;
		}else{
			$inp['parent'] = $parent_obj->id;
		}

		$vb = \App\ChatMobile::create($inp);
		

		$chat_arr = $vb->toArray();
		// $chat_arr['name'] = "Saya";
		$json = array();
		$json['RESP'] = "SCSCHT";
		$json['MESSAGE'] = 'Chat berhasil terkirim.';
		$json['DATA'] = $chat_arr;
		return json_encode($json);
	}
	public function storetwo(Request $request)
	{

		$inp = array();
		$inp['description'] = $request->input('description');
		$inp['name'] = $request->input('name');
		$inp['email'] = $request->input('email');
		$inp['handphone'] = $request->input('handphone');

		$gcm_id = $request->input('gcm_id');
		$gcm = \App\GCM::where('gcm_id', $gcm_id)->first();
		$user_id = $request->input('user_id');
		$anonymous = $request->input('is_anonymous');
		$inp['is_admin'] = '0';

		if(empty($gcm)){
			$gcm = new \App\GCM;
			$gcm->gcm_id = $gcm_id;
			
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}else{
			$gcm->gcm_id = $gcm_id;
			$gcm->ms_user_id = $user_id;
			$gcm->is_anonymous = $anonymous;
			$gcm->save();
		}


		// $photo = $request->input('photo');
		// if(!empty($photo))
		// {
		// 	$photo = urldecode($photo);
		// 	$decoded=base64_decode($photo);
		// 	$filename = "_ChatMobile_".date('Ymd_Hsi').'.JPEG';
		// 	file_put_contents($this->path.$filename,$decoded);
		// 	$inp['photo']  = $filename;		
			
		// }
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "_ChatMobile_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$inp['photo']= $fileName;
		}

		$inp['ms_gcm_id'] = $gcm->id;
		$parent_obj = \App\ChatMobile::where('ms_gcm_id', $gcm->id)->orderBy('created_at','desc')->first();
		if(empty($parent_obj))
		{
			$inp['parent'] = 0;
		}else{
			$inp['parent'] = $parent_obj->id;
		}

		$vb = \App\ChatMobile::create($inp);
		
		$chat_arr = $vb->toArray();
		// $chat_arr['name'] = "Saya";

		$json = array();
		$json['RESP'] = "SCSCHT";
		$json['MESSAGE'] = 'Chat berhasil terkirim.';
		$json['DATA'] = $chat_arr;
		return json_encode($json);
	}

	/**
	 * Display the specified resource. 
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$temp = \App\ChatMobile::find($id);
		\App\ChatMobile::where('ms_gcm_id', $id)->update(['read_flag' => '1']);
		$data['content'] = \App\ChatMobile::with('gcm')->where('ms_gcm_id', $id)->orderBy('id', 'asc')->get();
		$data['gcm_id'] = $id;
		return view('pages.chatmobile.show', compact('data'));
	}
	public function destroy($id)
	{
		$data = \App\ChatMobile::where('ms_gcm_id', $id)->get();
		foreach($data as $row)
		{
			if(File::exists($this->path.$row->photo))
			File::delete($this->path.$row->photo);
		}
		\App\ChatMobile::where('ms_gcm_id', $id)->delete();
		return redirect()->route('ki-admin.chatmobile.index');
	}


	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}
	protected function sendGCM( $data, $ids )
	{
	$url = 'https://android.googleapis.com/gcm/send';
	    $post = array(
	                    'registration_ids'  => array($ids),
	                    'data'              => array( "message" => $data),
	                    );
	// var_dump(json_encode($post));
	    //------------------------------
	    // Set CURL request headers
	    // (Authentication and type)
	    //------------------------------

	    $headers = array( 
	                        'Authorization: key=' . $this->GCM_API,
	                        'Content-Type: application/json'
	                    );

	    //------------------------------
	    // Initialize curl handle
	    //------------------------------

	    $ch = curl_init();

	    //------------------------------
	    // Set URL to GCM endpoint
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_URL, $url );

	    //------------------------------
	    // Set request method to POST
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_POST, true );

	    //------------------------------
	    // Set our custom headers
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	    //------------------------------
	    // Get the response back as 
	    // string instead of printing it
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

	    //------------------------------
	    // Set post data as JSON
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

	    //------------------------------
	    // Actually send the push!
	    //------------------------------

	    $result = curl_exec( $ch );

	    //------------------------------
	    // Error? Display it!
	    //------------------------------

	    if ( curl_errno( $ch ) )
	    {
	         echo 'GCM error: ' . curl_error( $ch );
	         exit();
	    }

	    //------------------------------
	    // Close curl handle
	    //------------------------------
		// var_dump($result);exit();
	    curl_close( $ch );

	    //------------------------------
	    // Debug GCM response
	    //------------------------------

	    
	    return;
	}

	public function getjson(){
		$gcm_id = Input::get('gcm_id');
		
		$data = \App\ChatMobile::with('gcm','gcm.user','admin')->where('ms_gcm_id', $gcm_id)->where('read_flag','<>','1')->get();
		\App\ChatMobile::where('ms_gcm_id', $gcm_id)->update(['read_flag' => '1']);
		return response()->json($data);
	}
}
