<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBank extends Model {

    protected $table = 'ms_account_bank_user';
    protected $guarded = ['id'];
    public $timestamps = false;
    public function bank()
    {
    	return $this->belongsTo('\App\Bank','ms_bank_id');
    }
    public function user()
    {
    	return $this->belongsTo('\App\User','ms_user_id');
    }
}
