<?php namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\General;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\GeneralRequest;
use Illuminate\Http\Request;
use Image;

class GeneralController extends Controller {
	protected $path;
	function __construct($foo = null) {
		$this->path = 'data/general/';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['city'] = City::lists('name','id');
		$data['country'] = Country::lists('name','id');
		$data['content'] = General::first();
		return view('pages.general.form',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(GeneralRequest $request, $id)
	{
		$data = General::find($id);
		$data->update($request->all());
		if($request->hasFile('logo'))
		{
			$file = $request->file('logo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "logo_".$data->site_title.'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$data->logo = $fileName;
			$data->save();
		}
		return redirect()->route('ki-admin.general.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
