<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\General;

class GeneralTableSeeder extends Seeder {

    public function run()
    {
        General::truncate();

        General::create([
            'site_title' => 'KeisKei Indonesia',
            'active' => '1',
            'ms_country_id' => '1',
            'ms_city_id' => '1',
            'telephone' => '(021) 1975123',
            'email' => 'alvarisi@live.com'
        ]);
    }

}