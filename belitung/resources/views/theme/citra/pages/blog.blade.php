@extends('theme.citra.app')

@section('content')
@include('theme.citra.partials.meta-blog')
 		<!-- breadcrumbs-area-area start -->
		 <div class="breadcrumbs-area log">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumbs-menu">
							<ul>
								<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
 								<li><a href="#">  <span>></span></a></li>

						</div>
					</div>
				</div>
			</div>
		 </div>
    <!-- breadcrumbs-area-area end -->	
    <div class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="left-sidebar">
        				@include('theme.citra.include.sidebar-default')</div>
	                </div>
                 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="toolbar">
                            
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="sorter">
                                    <div class="sort-by floatright">
                                        {!! $data['content']->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach($data['content'] as $row)
                        <div class="blog-all">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <div class="blogpost-thumb">
                                    <a href="#"><img src="img/blog/1.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                                <div class="posttitle">
                                    <div class="blog-top">
                                        <h2><a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><?php echo $row->title; ?></a></h2>
                                        <h3>{!! date('l, j F Y') !!}</h3>
                                    </div>
                                    <div class="blog-bottom">
                                        <p><?php $desc = explode("<!-- pagebreak -->", $row->description);
                                if(count($desc)>1)
                                    { ?>
                                <p>{!! $desc[0] !!}&nbsp;</p>      
                                <?php }else{ ?>
                                <p>{!! strip_tags(substr($row->description,0,400)).'...' !!}</p>
                                <?php
                            }
                            ?> </p>
                                        <a href="#">Read More</a>
                                    </div>
                                    <div class="posted-by2">
                                        Citra Florist
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

            </div>
        </div>
    </div> 
	@include('theme.citra.include.klien')

	@endsection
 	@section('custom-footer')
  
	@endsection
	 