@extends('theme.citra.app')
<title>
	Citra Florist | Form Pendaftaran
	</title>
@section('content')
 

 <!-- breadcrumbs-area-area start -->
		 <div class="breadcrumbs-area log">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumbs-menu">
							<ul>
								<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
 								<li><a href="{!! URL::to('/register') !!}">Registrasi<span>></span></a></li>

						</div>
					</div>
				</div>
			</div>
		 </div>
    <!-- breadcrumbs-area-area end -->
    <!-- login content section start -->
    <div class="login-area">
        <div class="container">
            <div class="row">
                 
                               <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="tb-login-form ">
                                    <h5 class="tb-title">Pendaftaran</h5>
                                    <p>Silahkan isi form berikut.</p>
                                    					@include('theme.'.$data['theme']->code.'.include.session')

                                    
                                    <form>
                                             {!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
                                            @if(!empty($data['affiliate_token']))
                                            {!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
                                            @endif
                                        <p class="checkout-coupon top log a-an">{!! BootstrapForm::text('username') !!}</p>
                                        <p class="checkout-coupon top-down log a-an">
                                        {!! BootstrapForm::text('mobile','Nomor Handphone (Untuk Aktivasi)') !!}
                                        </p>
                                         <p class="checkout-coupon top-down log a-an">
                                         {!! BootstrapForm::text('email') !!}
                                        </p>
                                        <p class="checkout-coupon top-down log a-an">
                                        	<label for="ms_country_id">
												Negara
											</label>
                                        {!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control','id' => 'ms_country_id')) !!}</p>
                                        <p class="checkout-coupon top-down log a-an">
                                        	<label for="ms_city_id">
												Kota
											</label>
                                        {!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id')) !!}</p>
                                        <p class="checkout-coupon top-down log a-an">
                                        	<label for="captcha">
												Recaptcha
											</label>
                                            {!! Recaptcha::render() !!}
                                            <span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
                                        </p>

                                        <p class="login-submit5">
                                            {!! Form::submit('Bergabung',array('class' => 'button-primary')) !!} 
                                        </p>{!! BootstrapForm::close() !!}
                                    </form>


                                </div>
                            </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="tb-login-form res">
                        <h5 class="tb-title">Catatan Penting !?</h5>
                        <p>Mohon diperhatikan</p>
                        
                        <div class="tb-info-login ">
                            <ul>
                                <li>1. Jangan Memberikan detail akun dan password <span style="color:green;font-weight: 500;">citra Florist</span> Anda kepada siapapun.</li>
                                <li>2. Pastikan URL yang Anda akses adalah <span style="font-weight: 500;color:green">http://www.citraflorist.com</span></li>
                                <li>3. Setelah ter-verifikasi, pastikan Anda mengisi detail profil Anda di <span style="font-weight: 500;color:green">Beranda - edit profile</span>.</li>
                                <li>4. Pihak <span style="font-weight: 500;color:green">citra Florist</span> tidak pernah menanyakan akun dan password Anda dengan alasan apapun, harap berhati-hati.</li>
                               <li>5. Pastikan <span style="font-weight: 500;color:green">Transfer / pembayaran</span> hanya ke rekening perusahaan kami.</li>
                            </ul>
                        </div>

                        <div class="tb-social-login">
                                        <a class="tb-facebook-login" style="background:#90133B" href="{!! URL::to('/login') !!}"> 
                                            <i class="fa fa-key"></i> Silahkan login
                                        </a>
                                        <a class="tb-twitter-login res" style="background:orange" href="{!! URL::to('/password/email') !!}">
                                            <i class="fa fa-user"></i> Reset Password
                                        </a>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login  content section end -->
 							 

 	@include('theme.citra.include.klien')

 
 @endsection

@section('custom-footer')
	{!! HTML::script('theme/citra/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/citra/plugin/validation/jquery.validate.min.js') !!}
	<script type="text/javascript">
		$(function () {
        	var value = $("#ms_country_id").val();
        	cityField(value);
      	});
		function cityField( value)
		{
			var general_country_id = {!! $data['general']->ms_country_id !!};
			if(value == general_country_id)
			{
				$('#city-container').show();
			}else{
				$('#city-container').hide();
			}
		}
	    $(document).ready(function() {

		   $('#ms_country_id').selectize();      
		   $('#ms_city_id').selectize();      
		   $('#ms_country_id').change(function()
		   {
		   		cityField($(this).val());
		   });
		   
		   $('#form-validate').validate({
		   		errorClass : "help-block error",
		   		rules: {
		   			username : {
		   				required: true,
		   				minlength: 3
		   			},
		   			mobile : {
		   				required: true,
		   				minlength: 3
		   			},
		   			email :{
		   				required: true,
		   				email : true
		   			}
		   		},
		   		messages: {
		   			username : {
		   				required : "Mohon masukkan username Anda",
		   				minlength : $.validator.format("Username Anda harus minimal {0} karakter")
		   			},
		   			mobile : {
		   				required : "Mohon masukkan nomor handphone Anda",
		   				minlength : $.validator.format("nomor handphone Anda harus minimal {0} karakter")
		   			},
		   			email : {
		   				required : "Mohon masukkan email Anda",
		   				email : "Mohon cek kembali alamat email Anda"
		   			}
		   		}
		   });
	    });
	</script>
@endsection