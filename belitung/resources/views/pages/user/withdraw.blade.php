@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Folders</h3>
                  <div class='box-tools'>
                    <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  @include('pages.user.sidebar')
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Withdraw User</h3>   
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>{!! $data['content']->name !!} 
                        
                    </h3>
                    <h4>Group : {!! $data['content']->roles->first()->name !!}
                    <span class="mailbox-read-time pull-right">
                            {!! $data['content']->enabled=='1'?"<span class='label label-primary'>Enabled</span>":"<span class='label label-danger'>Disabled</span>" !!}
                            {!! $data['content']->approved=='1'?"<span class='label label-primary'>Approved</span>":"<span class='label label-danger'>Dispproved</span>" !!}
                            {!! $data['content']->safe=='1'?"<span class='label label-primary'>Safe</span>":"<span class='label label-danger'>Unsafe</span>" !!}
                        </span>
                    </h4>
                    <h5>Username : {!! $data['content']->username !!}</h5>
                    <h5>Email: {!! $data['content']->email !!} <span class="mailbox-read-time pull-right">Register at: {!! date('d-m-Y H:i:s', strtotime($data['content']->created_at)) !!}</span></h5>
                  </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <table class="table" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun</th>
                                <th>Bulan</th>
                                <th>Point Referral</th>
                                <th>Point Bulanan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;
                        $total_active = 0;
                        $total = 0;
                        ?>
                        @foreach($data['content']->withdraw as $row)
                                <tr>
                                    <td>{!! $no++ !!}</td>
                                    <?php 
                                        $dateObj   = DateTime::createFromFormat('!m', $row->month);
                                        $monthName = $dateObj->format('F');
                                        unset($dateObj);
                                        $total+= intval($row->referral_value);
                                        $total+= intval($row->monthly_value);
                                        if($row->status != '1')
                                        {
                                            $total_active+= intval($row->referral_value);
                                            $total_active+= intval($row->monthly_value);
                                        }
                                    ?>
                                    <td>{!! $row->year !!}</td>
                                    <td>{!! $monthName !!}</td>
                                    <td>{!! intval($row->referral_value) !!}</td>
                                    <td>{!! intval($row->monthly_value) !!}</td>
                                    <td>{!! $row->status=='1'?"<span class='label label-default'>Inactive</span><p>".date('d M Y',strtotime($row->withdraw_date))."</p>":$row->status=='2'?"<span class='label label-success'>Paid</span>":"<span class='label label-primary'>Unpaid</span>" !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                        <td>Total yang dapat diambil</td>
                                        <td> : </td>
                                        <td> {!! $total_active !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Total yang telah diambil</td>
                                        <td> : </td>
                                        <td> {!! $total-$total_active !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Total Semua</td>
                                        <td> : </td>
                                        <td> {!! $total !!} </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
                    
                </section><!-- /.content -->
@include('scripts.delete-modal')
@include('scripts.reset-password-modal')
@endsection


@section('custom-head')
    {!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection