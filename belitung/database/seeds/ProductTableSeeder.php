<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductTableSeeder extends Seeder {

    public function run()
    {
        Product::truncate();

        Product::create([
            'code' => 'BD001',
            'title' => 'Product 1',
            'description' => 'Description product 1',
            'specification' => 'Specification product 1',
            'ms_brand_id' => '1',
            'ms_category_product_id' => '1',
            'ms_status_product_id' => '1',
            'length' => '1',
            'weight' => '1',
            'help' => 'Help Product 1'
        ]);

        Product::create([
            'code' => 'BD002',
            'title' => 'Product 2',
            'description' => 'Description product 2',
            'specification' => 'Specification product 2',
            'ms_brand_id' => '2',
            'ms_category_product_id' => '2',
            'ms_status_product_id' => '2',
            'length' => '2',
            'weight' => '2',
            'help' => 'Help Product 2'
        ]);

        Product::create([
            'code' => 'BD003',
            'title' => 'Product 3',
            'description' => 'Description product 3',
            'specification' => 'Specification product 3',
            'ms_brand_id' => '3',
            'ms_category_product_id' => '3',
            'ms_status_product_id' => '1',
            'length' => '3',
            'weight' => '3',
            'help' => 'Help Product 3'
        ]);

    }

}