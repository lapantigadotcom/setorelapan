<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsGeneralsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_generals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('site_title', 64)->nullable();
			$table->string('site_tagline', 128)->nullable();
			$table->string('site_description', 512)->nullable();
			$table->string('email', 64)->nullable();
			$table->string('address', 512)->nullable();
			$table->integer('ms_city_id')->nullable();
			$table->integer('ms_country_id')->nullable();
			$table->string('telephone', 64)->nullable();
			$table->string('mobile', 64)->nullable();
			$table->string('logo', 256)->nullable();
			$table->string('postcode', 64)->nullable();
			$table->string('fax', 64)->nullable();
			$table->string('facebook', 64)->nullable();
			$table->string('twitter', 64)->nullable();
			$table->string('instagram', 64)->nullable();
			$table->string('path', 64)->nullable();
			$table->string('googleplus', 45)->nullable();
			$table->string('active', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_generals');
	}

}
