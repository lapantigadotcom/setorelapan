<!--top-sales-area start-->
        <div class="top-sales-area">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <h2>Produk Populer</h2>
                        <h1>Citra Florist</h1>
                    </div>
                </div>
                <div class="row"> @if(isset($hot_product))
                    <div class="top-sales-products-carousel">
                        <!-- single-product start--> @foreach($hot_product as $row)

                        <div class="col-lg-12">
                            <div class="single-product">
                                <a data-toggle="modal" title="Quick View" href="#productModal" class="view-single-product"><i class="fa fa-expand"></i></a>
                                <div class="product-thumb">
                                    <span class="label-pro-sale">-9%</span>
                                    <span class="new">new</span>
                                @if($row->productImages()->count() > 0)
                                    @if(File::exists($row->productImages->first()->image))
 
                            <img class="img-responsive" src="{!! asset($row->productImages->first()->image) !!}"  title="{!! $row->title !!} | {!! ucwords($row->categoryproduct->name) !!}" > 
                                @endif
                                    @endif                                 
                                <div class="product-desc">
                                    <h2 class="product-name"><a href="#">{!! $row->title !!}</a></h2>
                                    <div class="price-box floatleft">
                                        <p class="old-price">{!! ucwords($row->categoryproduct->name) !!}</p>
                                     </div>
                                </div>
                            </div>
                        </div> @endforeach 
                        <!-- single-product end-->
                         
                    </div>@endif
                </div>
            </div>
        </div>
        <!--top-sales-area end-->
 
