@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if(isset($data['content']))
                        {!! Form::model($data['content'],array('route' => ['ki-admin.user.update',$data['content']->id], 'method' => 'PUT','files' => 'true' )); !!}
                        @else
                        {!! BootstrapForm::openStandard(array('route' => 'ki-admin.user.store', 'method' => 'POST','files' => 'true')); !!}
                        @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                                            <li><a href="#tab_2" data-toggle="tab">Gambar</a></li>
                                            @if(isset($data['content']))
                                            @if($data['content']->roles->first()->require_file == '1')
                                            <li><a href="#tab_3" data-toggle="tab">File Tambahan</a></li>
                                            @endif
                                            @endif
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        {!! Form::label('ms_role_id','Role') !!}
                                                        {!! Form::select('ms_role_id',$data['role'],isset($data['content'])?$data['content']->roles->first()->id:null, array('class' => 'form-control')) !!}
                                                    </div>
                                                        {!! BootstrapForm::text('company') !!}
                                                        {!! BootstrapForm::text('name') !!}
                                                        {!! BootstrapForm::text('username') !!}
                                                        {!! BootstrapForm::text('email') !!}
                                                    <div class="form-group">
                                                        {!! Form::label('password','Password',array('class' => 'form-label')) !!}
                                                        <div class="form-control">
                                                            <span><strong><em>Terlampir dalam email.</em></strong></span>
                                                        </div>
                                                    </div>
                                                        {!! BootstrapForm::text('telephone') !!}
                                                        {!! BootstrapForm::text('mobile') !!}
                                                        {!! BootstrapForm::text('pinbb') !!}

                                                    <div class="form-group">
                                                        {!! Form::label('ms_city_id','City') !!}
                                                        {!! Form::select('ms_city_id',$data['city'],null, array('class' => 'form-control')) !!}
                                                        <span class="help-block">{{ $errors->first('ms_city_id') }}</span>
                                                    </div>
                                                        {!! BootstrapForm::textarea('address','Address',null,array('id' => 'wysihtml5')) !!}
                                                    <div class="form-group">
                                                        {!! Form::label('enabled','Status') !!}
                                                        {!! Form::select('enabled',[ '0' => 'Disabled', '1' => 'Enabled'],null, array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('approved','Approved') !!}
                                                        {!! Form::select('approved',[ '0' => 'No', '1' => 'Yes'],null, array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('save','Safe') !!}
                                                        {!! Form::select('save',[ '0' => 'No', '1' => 'Yes'],null, array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                                    </div>
                                                </div>
                                            </div><!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <div class="box-body">
                                                    <div class="form-group {!! $errors->any()?($errors->first('photo')?' has-error':''):'' !!}">
                                                    {!! Form::label('photo', 'Photo', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(isset($data['content']))
                                                        @if(File::exists('data/user/photo/'.$data['content']->photo))
                                                        <div class="col-md-3">
                                                            <img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" class="img-thumbnail img-responsive">
                                                            <br>
                                                        </div>
                                                        @else
                                                        <span class="label label-default">Belum upload</span>
                                                        @endif
                                                        {!! Form::file('photo', array('class' => 'form-control')) !!}
                                                        <span>* kosongkan jika tidak ingin diubah</span>
                                                    @else
                                                        {!! Form::file('photo', array('class' => 'form-control')) !!}
                                                        <span>* kosongkan jika tidak ada.</span>
                                                    @endif    
                                                    </div>
                                                    <span class="help-block">{!! $errors->any()?($errors->first('photo')?$errors->first('photo'):''):'' !!}</span>
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_3">
                                                <div class="box-body">
                                                @if(isset($data['content']))
                                                @if($data['content']->userFiles()->count() > 0)
                                                <div class="form-group">
                                                    {!! Form::label('ktp','KTP', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(File::exists('data/user/'.$data['content']->userFiles->ktp))
                                                    <a target="_blank" href="{!! asset('data/user/'.$data['content']->userFiles->ktp) !!}" class="btn btn-danger btn-sm"> Download </a>
                                                    @else
                                                        <span class="label label-danger">Belum upload</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('siup','SIUP', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(File::exists('data/user/'.$data['content']->userFiles->siup))
                                                    <a target="_blank"  href="{!! asset('data/user/'.$data['content']->userFiles->siup) !!}" class="btn btn-danger btn-sm"> Download </a>
                                                    @else
                                                        <span class="label label-danger">Belum upload</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('tdp','TDP', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(File::exists('data/user/'.$data['content']->userFiles->tdp))
                                                    <a target="_blank"  href="{!! asset('data/user/'.$data['content']->userFiles->tdp) !!}" class="btn btn-danger btn-sm"> Download </a>
                                                    @else
                                                        <span class="label label-danger">Belum upload</span>
                                                    
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('npwp','NPWP', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(File::exists('data/user/'.$data['content']->userFiles->npwp))
                                                    <a target="_blank"  href="{!! asset('data/user/'.$data['content']->userFiles->npwp) !!}" class="btn btn-danger btn-sm"> Download </a>
                                                    @else
                                                        <span class="label label-danger">Belum upload</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('akte','AKTE', array('class' => 'form-label col-md-2')) !!}
                                                    <div class="col-md-10">
                                                    @if(File::exists('data/user/'.$data['content']->userFiles->akte))
                                                    <a target="_blank"  href="{!! asset('data/user/'.$data['content']->userFiles->akte) !!}" class="btn btn-danger btn-sm"> Download </a>
                                                    @else
                                                        <span class="label label-danger">Belum upload</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                @else
                                                <div class="alert alert-warning" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only"></span>
                                                    <b>Tidak ada file.</b> User terpilih belum melakukan verifikasi.
                                                </div>
                                                @endif
                                                @endif

                                                </div>
                                            </div>
                                        </div><!-- /.tab-content -->
                                    </div><!-- nav-tabs-custom -->
                                </div><!-- /.col -->
                            </div> <!-- /.row -->
                        {!! BootstrapForm::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    {!! HTML::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/fastclick/fastclick.min.js') !!}
    {!! HTML::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    <script type="text/javascript">
      $(function () {
        $("#wysihtml5").wysihtml5();
      });
    </script>
@endsection