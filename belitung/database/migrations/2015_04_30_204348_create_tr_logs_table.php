<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_user_id');
			$table->integer('historable_id')->nullable();
			$table->string('historable_type', 64)->nullable();
			$table->string('name', 64)->nullable();
			$table->string('action', 64)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_logs');
	}

}
