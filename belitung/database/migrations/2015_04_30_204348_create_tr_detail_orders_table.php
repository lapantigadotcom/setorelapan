<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrDetailOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_detail_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tr_order_id')->index('fk_tr_detail_order_tr_order1_idx');
			$table->integer('ms_product_id')->index('fk_tr_detail_order_ms_products1_idx');
			$table->integer('total')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_detail_orders');
	}

}
