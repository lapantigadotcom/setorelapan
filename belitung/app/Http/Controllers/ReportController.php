<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use PDF;
use App\User;
use App\RoleUser;
use App\Role;
use DateTime;
use DB;
use Carbon\Carbon;
use App\DetailOrder;
use App\Withdraw;

class ReportController extends BasicController {

	public function userIndex()
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['content'] = \App\Order::
			with(
				'detailOrders',
				'detailOrders.product',
				'detailOrders.product.prices'
			)
			->orWhere(function($query)
            {
                $query->where('ms_user_id', Auth::id())
                      ->where('ms_status_order_id', '<>', '1');
            })
            ->orWhere(function($query)
            {
                $query->where('ms_reseller_id', Auth::id())
                      ->where('ms_status_order_id', '<>', '1');
            })
            ->orderBy('created_at','desc')
			->paginate(15);
		return view('theme.'.$this->theme->code.'.pages.user.report',compact('data'));	
	}
	public function userInvoice($id, Request $request)
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['content'] = \App\Order::with(
			'detailOrders',
			'detailOrders.product',
			'detailOrders.product.prices',
			'detailOrders.product.productImages',
			'detailOrders.detailAttributeProductDetailOrders',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute.attribute',
			'statusOrder',
			'courier')
			->find($id);
		$data['user'] = \App\User::find($data['content']->ms_user_id);
		$print = $request->input('print');
		if(!empty($print))
		{
			$pdf = PDF::loadView('theme.'.$this->theme->code.'.pages.user.invoice',compact('data'));
			return $pdf->download('invoice.pdf');
		}
		return view('theme.'.$this->theme->code.'.pages.user.invoice',compact('data'));
	}

	public function userJalan($id, Request $request)
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['content'] = \App\Order::with(
			'detailOrders',
			'detailOrders.product',
			
			'detailOrders.product.productImages',
			'detailOrders.detailAttributeProductDetailOrders',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute.attribute',
			'statusOrder',
			'courier')
			->find($id);
		$data['user'] = \App\User::find($data['content']->ms_user_id);
		$print = $request->input('print');
		if(!empty($print))
		{
			$pdf = PDF::loadView('theme.'.$this->theme->code.'.pages.user.jalan',compact('data'));
			return $pdf->download('surat-jalan.pdf');
		}
		return view('theme.'.$this->theme->code.'.pages.user.jalan',compact('data'));
	}



	public function user()
	{
		$data['today'] = User::where(DB::raw('DAY(created_at)'), '=', date('d'))
		->where(DB::raw('MONTH(created_at)'), '=', date('m'))
		->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['month'] = User::where(DB::raw('MONTH(created_at)'), '=', date('m'))
		->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['year'] = User::where(DB::raw('YEAR(created_at)'), '=', date('Y'))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['week'] = User::whereBetween('created_at', array(Carbon::now()->subWeek(), Carbon::now()))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['active'] = User::whereHas('roles', function($q) {
			$q->where('enabled', '=', '1');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

	//	$data['employee'] = User::whereHas('roleUsers', function($q) {
	//		$q->where('ms_role_id', '<', '4');
	//	})->count();

		$data['user'] = User::whereHas('roleUsers', function($q) {
			$q->where('ms_role_id', '=', '4');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['agent'] = User::whereHas('roleUsers', function($q) {
			$q->where('ms_role_id', '=', '5');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['distributor'] = User::whereHas('roleUsers', function($q) {
			$q->where('ms_role_id', '=', '6');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['perusahaan'] = User::whereHas('roleUsers', function($q) {
			$q->where('ms_role_id', '=', '7');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['overseas'] = User::whereHas('roleUsers', function($q) {
			$q->where('ms_role_id', '=', '8');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		return view('pages.report.user', compact('data'));
	}
	
	public function afiliasi()
	{
		$data['top_contributor'] = DB::select("
SELECT u.id, u.name, t.total, u.username
FROM `ms_users` u,
	(SELECT `referraled_by`, count(`referraled_by`) as total FROM `ms_users` group by `referraled_by`) t,
	`ms_role_user` ru,
	`ms_roles` r
WHERE u.id = ru.ms_user_id AND ru.ms_role_id = r.id AND u.id = t.referraled_by AND r.locked <> 1 ORDER BY t.total DESC
			");
		$data['content'] = User::has('referraledBy')->whereHas('roles', function($q){
			$q->where('locked', '<>', '1');
		})->orderBy('created_at','desc')->get();
		return view('pages.report.afiliasi', compact('data'));
	}
	
	public function sales()
	{
		$data['sale_total'] = DetailOrder::with('order')->sum('total');
			

		$data['sale_today'] = DetailOrder::whereHas('order', function($q){
			$q->where(DB::raw('DAY(created_at)'), '=', date('d'))
			->where(DB::raw('MONTH(created_at)'), '=', date('m'))
			->where(DB::raw('YEAR(created_at)'), '=', date('Y'));
		})->sum('total');

		$data['sale_week'] = DetailOrder::whereHas('order', function($q){
			$q->whereBetween('created_at', array(Carbon::now()->subWeek(), Carbon::now()));
		})->sum('total');

		$data['sale_month'] = DetailOrder::whereHas('order', function($q){
			$q->where(DB::raw('MONTH(created_at)'), '=', date('m'))
			->where(DB::raw('YEAR(created_at)'), '=', date('Y'));
		})->sum('total');
		
		$data['sales_per_product'] = DB::select("SELECT p.title, sum(do.total) as total FROM ms_products as p, tr_detail_orders as do WHERE p.id = do.ms_product_id GROUP BY do.ms_product_id ORDER BY total DESC");
		
		return view('pages.report.sales', compact('data'));
	}
	
	public function bonus()
	{
		$total_referral_value = Withdraw::sum('referral_value');
		$total_monthly_value = Withdraw::sum('monthly_value');
		$data['total_bonus'] = $total_referral_value + $total_monthly_value;
		
		$total_bonus_dibayarkan_referral = Withdraw::where('status', '=', '1')->sum('referral_value');
		$total_bonus_dibayarkan_monthly = Withdraw::where('status', '=', '1')->sum('monthly_value');
		$data['total_bonus_dibayarkan'] = $total_bonus_dibayarkan_referral + $total_bonus_dibayarkan_monthly;
		
		$total_bonus_kredit_referral = Withdraw::where('status', '<>', '1')->sum('referral_value');
		$total_bonus_kredit_monthly = Withdraw::where('status', '<>', '1')->sum('monthly_value');
		$data['total_bonus_kredit'] = $total_bonus_kredit_referral + $total_bonus_kredit_monthly;
		
		$data['top_member_bonus'] = Withdraw::whereHas('user.roleUsers.role', function($q){
			$q->where('locked', '<>', '1');
		})->orderBy(DB::raw('sum(referral_value+monthly_value)'), 'desc')->first();
		if(empty($data['top_member_bonus']))
			$data['top_member_bonus'] = 'Not Available';
		else{
			if($data['top_member_bonus']->user()->count() > 0)
				$data['top_member_bonus'] = $data['top_member_bonus']->user->name;
			else
				$data['top_member_bonus'] = 'Not Available';
		}
			
		$data['content'] = \App\Withdraw::with('user')->select(DB::raw('sum(referral_value) as total_referral, sum(monthly_value) as total_bonus, ms_user_id'))
                     ->groupBy('ms_user_id')
                     ->orderBy('total_bonus', 'desc')
                     ->get();
		return view('pages.report.bonus', compact('data'));
	}

}
