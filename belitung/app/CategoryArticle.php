<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;

class CategoryArticle extends Model {

	protected $table = 'ms_category_articles';
	protected $guarded = ['id'];
    public $timestamps = false;

	public function articles()
	{
		return $this->hasMany('App\Article', 'ms_category_article_id', 'id');
	}

}
