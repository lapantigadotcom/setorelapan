<!-- jQuery 2.0.2 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        {!! HTML::script('js/jquery.min.js') !!}
        <!-- Bootstrap -->
        {!! HTML::script('js/bootstrap.min.js') !!}
        
        <!-- AdminLTE App -->
        {!! HTML::script('js/app.min.js') !!}

        @yield('custom-footer')