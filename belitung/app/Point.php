<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Role;

class Point extends Model {

	protected $table = 'ms_points';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo('App\Role', 'ms_role_id');
    }
}
