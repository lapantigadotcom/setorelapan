<div class="box-body">
	<div class="form-group">
		{!! Form::label('tracking_number','Nomor Tracking') !!}
		{!! Form::text('tracking_number',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('tujuan','Tujuan') !!}
		{!! Form::text('tujuan',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('ongkir','Biaya Kirim') !!}
		{!! Form::text('ongkir',null, array('class' => 'form-control')) !!}
	</div>

		<div class="form-group">
		{!! Form::label('petugas','Petugas') !!}
		{!! Form::text('petugas',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('ms_courier_id','Pilih Kurir') !!}
		{!! Form::select('ms_courier_id', $data['courier'] ,null, array('class' => 'form-control')) !!}
	</div>

	


	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>