<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class StatusProduct extends Model {

	protected $table = 'ms_status_products';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\Product', 'ms_status_product_id', 'id');
    }

}
