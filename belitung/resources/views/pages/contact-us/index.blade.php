@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            INBOX | 
            <small>Hubungi Kami</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Harap Segera direspond</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Judul</th>
                    <th>Waktu</th>
                    <th>Hapus</th>
                    <th>Balas</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td></td>
                      <td>{!! $row->name !!}</td>
                      <td>{!! $row->email !!}</td>
                      <td><a data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Pesan" href="{!! route('ki-admin.contact-us.show', [$row->id]) !!}">{!! $row->subject !!}</a></td>
                      <td>{!! $row->created_at !!}</td>
                      <td>
                          <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.contact-us.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                      </td>
                      <td>
                                        <a data-toggle="modal"  href="#" data-target="#balas" class="btn btn-success">Reply </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Judul</th>
                    <th>Waktu</th>
                    <th>Hapus</th>
                    <th>Balas</th>

                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>

<!-- Modal smart-->
<div class="modal fade" id="balas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
       <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Balas Email Kontak Keiskei</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                   
                  <div class="form-group"><div class="col-sm-10">
                                        <label>Email Tujuan</label>
                                        <input class="form-control" placeholder="{!! $row->email !!}" disabled="" type="text">
                                      </div></div>


<div class="form-group"><div class="col-sm-10">
                                        <label>Judul Pertanyaan</label>
                                        <input class="form-control" placeholder="{!! $row->subject !!}" disabled="" type="text">
                                      </div></div>

                  <div class="form-group"><div class="col-sm-10">
                      <label>Judul Balasan</label>
                      <input class="form-control" placeholder="Enter ..." type="text">
                                      </div></div>


                    <div class="form-group">
                      <div class="col-sm-10">
                      <label>Pesan</label>

                    <textarea class="form-control" rows="3" placeholder="Tulis Pesan disini....."></textarea>
                    </div>
 </div>
   <span class="text-red">* harap cek kembali sebelum kirim balasan</span>


                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                       
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning">Kirim Balasan</button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
    </div>
    <!-- /.modal-dialog -->
</div>



@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection