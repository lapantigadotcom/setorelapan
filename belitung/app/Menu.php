<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailMenu;

class Menu extends Model {

	protected $table = 'ms_menus';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function detailMenus()
    {
    	return $this->hasMany('App\DetailMenu', 'ms_menu_id', 'id');
    }

}
