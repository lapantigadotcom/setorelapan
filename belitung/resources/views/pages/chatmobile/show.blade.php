@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Chat Support
            <small>Mobile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Chatt Support</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="direct-chat-messages" style="height:450px">
                  @foreach($data['content'] as $row)
                  @if($row->is_admin != '1')
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">
                        @if($row->gcm->is_anonymous == '1')
                        {!! $row->name !!}
                        @else
                        {!! $row->gcm->user->name !!}
                        @endif
                        </span>
                        <span class="direct-chat-timestamp pull-right">{!! $row->created_at !!}</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="@if($row->gcm->is_anonymous == '1')
                        https://www.keiskei.co.id/data/im_user.png
                        @else
                        @if(File::exists('ws/data/user/photo/'.$row->gcm->user->photo) and !empty($row->gcm->user->photo))
                        https://www.keiskei.co.id/ws/data/user/photo/{!! $row->gcm->user->photo !!}
                        @else
                        https://www.keiskei.co.id/data/im_user.png
                        @endif
                      
                        @endif" alt="message user image">
                      <div class="direct-chat-text">
                        @if(!empty($row->description))
                          <p>{!! $row->description !!}</p>
                        @endif
                        @if(!empty($row->photo))
                        <p>
                          @if(File::exists('ws/data/chat_mobile/'.$row->photo))
                          <a href="{!! asset('ws/data/chat_mobile/'.$row->photo) !!}" target="_blank" class="btn btn-primary btn-sm">Download</a>
                          @endif
                        </p>
                    @endif
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                  @else
                  <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">{!! $row->admin->name !!}</span>
                        <span class="direct-chat-timestamp pull-left">{!! $row->created_at !!}</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="https://www.keiskei.co.id/img/avatar5.png" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        @if(!empty($row->description))
                          <p>{!! $row->description !!}</p>
                        @endif
                        @if(!empty($row->photo))
                        <p>
                          @if(File::exists('wdata/chat_mobile/'.$row->photo))
                          <a href="{!! asset('data/chat_mobile/'.$row->photo) !!}" target="_blank" class="btn btn-primary btn-sm">Download</a>
                          @endif
                        </p>
                    @endif
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                  @endif
                  @endforeach
                </div><!-- /.box-body -->
                <div class="box-footer">

                  {!! Form::open(array('route' => 'ki-admin.chatmobile.storeadmin', 'method' => 'POST', 'files' => true)) !!}
                  @include('pages.chatmobile.form',array('submit' => 'Send'))
                  {!! Form::close() !!}
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>
</section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
    setInterval("chatRealtime()", 3000);
  });
  $(".direct-chat-messages").animate({ scrollTop: $(document).height() }, "fast");
  function chatRealtime(){
  	$.ajax({
  			url:"{{ url('chat/getchat/json') }}",
			type:"GET",
			data:{
				gcm_id : '{!! $data['gcm_id'] !!}'
			}
		}).done(function(data, status){
			if(status=='success')
			{	
				console.log(data);
				if(jQuery.isEmptyObject(data))
				{
					
				}else
				{
					var html = '';
					var ind = 0;
					$.each(data, function(key, value)
					{

						if(value.is_admin == '1'){
							html += "<div class='direct-chat-msg right'><div class='direct-chat-info clearfix'><span class='direct-chat-name pull-right'>" + value.admin.name +"</span><span class='direct-chat-timestamp pull-left'>"+ value.created_at +"</span></div>                      <img class='direct-chat-img' src='https://www.keiskei.co.id/img/avatar5.png' alt='message user image'><div class='direct-chat-text'>" + value.description ;
                        if(value.photo != ""){
							html += "<a href='https://www.keiskei.co.id/data/chat_mobile/'" + value.photo + " target='_blank' class='btn btn-primary btn-sm'>Download</a>";
                        }
                    	html += "</div></div>";
                      ind++;
						}else{
              ind++;
							html += "<div class='direct-chat-msg'><div class='direct-chat-info clearfix'><span class='direct-chat-name pull-left'>";
							if(value.gcm.is_anonymous == '1'){
								html += value.name;
							}else{
								html += value.gcm.user.name;
							}
							html += "</span><span class='direct-chat-timestamp pull-right'>" + value.created_at + "</span></div>";
							if(value.gcm.is_anonymous == '1'){
								html += "<img class='direct-chat-img' src='https://www.keiskei.co.id/data/im_user.png'  alt='message user image' />";
							}else{
								html += "<img class='direct-chat-img' src='https://www.keiskei.co.id/data/user/photo/" + value.gcm.user.photo + "'  alt='message user image' />";
							}
							html += "<div class='direct-chat-text'> <p>" + value.description + "</p>";
							if(value.photo != ""){
								html += "<a href='https://www.keiskei.co.id/ws/data/chat_mobile/'" + value.photo + "' target='_blank' class='btn btn-primary btn-sm'>Download</a>";
                        	}
                        	html += "</div></div>";
						}

					});
					
					if(ind > 0)
					{
						$('.direct-chat-messages').append(html);
            $(".direct-chat-messages").animate({ scrollTop: $(document).height() }, "fast");
						// $.playSound('<?php echo asset('assets/qcare') ?>');
					}
					console.log(html);
				}
			}
		});
  }
  
</script>
@endsection