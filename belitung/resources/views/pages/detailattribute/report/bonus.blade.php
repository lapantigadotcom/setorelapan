@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Report
    <small>Bonus</small>
</h1>
<ol class="breadcrumb">
</ol>
</section>
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    {!! $data['total_bonus'] !!}<sup style="font-size: 20px">Rp.</sup>
                </h3>
                <p>
                    TOTAL Bonus
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
                Bonus TOTAL <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    {!! $data['total_bonus_dibayarkan'] !!}<sup style="font-size: 20px">Rp.</sup>
                </h3>
                <p>
                    Bonus Dibayarkan
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
                Withdraw Bonus <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    {!! $data['total_bonus_kredit'] !!}<sup style="font-size: 20px">Rp.</sup>
                </h3>
                <p>
                    Bonus Kredit
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
                unWithdraw Bonus <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    {!! $data['top_member_bonus'] !!}
                </h3>
                <p>
                    Perwakilan
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
                TOP member Bonus <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->
</div>
<div class="row">
    <div class="col-md-12 table-responsive">
        <h4 class="">Daftar bonus user</h4>
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Bonus Transaksi</th>
                    <th>Bonus Referral</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data['content'] as $row)
                <tr>
                    <td>{{ $row->user->name }}</td>
                    <td>{{ $row->total_bonus }}</td>
                    <td>{{ $row->total_referral }}</td>
                    <td>{{ $row->total_bonus+$row->total_referral }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nama</th>
                    <th>Total</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
</section>
@endsection

@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection