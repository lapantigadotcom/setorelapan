@extends('auth.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">
			<div class="panel panel-default">
				
				<div class="panel-body">
					<div class="login-header">
						<h4>MASUK KE SISTEM</h4>
					</div>
					
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Maaf!</strong> Terjadi kesalahan dalam input an Anda.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-12">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<input type="password" class="form-control" name="password"  placeholder="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 left">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Ingat saya
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<a class="btn btn-link" href="{{ url('/password/email') }}">Lupa Password?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
