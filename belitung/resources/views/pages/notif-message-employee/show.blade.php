@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Message</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{!! route('ki-admin.notif-message.inbox') !!}"><i class="fa fa-envelope-o"></i> Inbox</a></li>
                <li><a href="{!! route('ki-admin.notif-message.outbox') !!}"><i class="fa fa-envelope-o"></i> Outbox</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
        </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Show Message</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>Subject : {!! $data['content']->subject !!}</h3>
                    <h5>To: {!! $data['content']->recipient->name !!} <span class="mailbox-read-time pull-right">{!! $data['content']->created_at !!}</span></h5>
                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-read-message">
                    <h4>Message Content :</h4>
                    {!! $data['content']->content !!}
                  </div><!-- /.mailbox-read-message -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                </div><!-- /.box-footer -->
                <div class="box-footer">
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>
</section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection