<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductImageRequest;
use App\ProductImage;
use Input;
use File;
use App\Product;
use Image;

class ProductImageController extends Controller {

	protected $path;

	function __construct() {
		$this->path = 'data/product/';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('pages.productimage.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['ms_product_id'] = Input::get('product');
		return view('pages.productimage.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProductImageRequest $request)
	{
		$productImage = ProductImage::create($request->all());
		$data = Product::find($request->input('ms_product_id'));
		if ($request->hasFile('new-image'))
		{
		    $newImage = $request->file('new-image');
			$extension = $newImage->getClientOriginalExtension();
			$no = 1;
			if (!File::exists($this->path))
			{
				File::makeDirectory($this->path, $mode = 0777, true, true);
			}
			while(File::exists($this->path.$data->code.'_photo_'.sprintf("%03d", $no).'.'.$extension)) {
				$no++;
			}
			$fileName = $data->code."_photo_".sprintf("%03d", $no).'.'.$extension;
			Image::make($newImage->getRealPath())->save($this->path.$fileName);
			$productImage->image = $this->path.$fileName;
			$productImage->save();
		}
		return redirect()->route('ki-admin.product.edit', [$data->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = ProductImage::find($id);
		$data['ms_product_id'] = $data['content']->ms_product_id;
		return view('pages.productimage.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$productImage = ProductImage::find($id);
		if (File::exists($productImage->image)) {
			unlink($productImage->image);
		}
		$productImage->delete();
		return redirect()->back();
	}

}
