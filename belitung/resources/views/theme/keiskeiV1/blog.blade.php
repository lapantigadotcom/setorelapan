  
	<title>
	KeisKei Indonesia | {!! $data['content']->title !!}

	</title>
	<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@keiskeiashitaba" />
<meta name="twitter:creator" content="@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="{!! $data['content']->meta_description !!}" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="{!! $data['content']->meta_description !!}" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="{!! $data['content']->meta_description !!}" />
<meta name="DC.subject" content="{!! $data['content']->meta_keyword !!}" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="{!! $data['content']->meta_description !!}">
<meta name="keywords" content="{!! $data['content']->meta_keyword !!}"/>
<meta name='robots' content='INDEX, FOLLOW' />


  