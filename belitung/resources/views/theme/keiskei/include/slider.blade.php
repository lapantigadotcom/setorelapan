<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span style="color:red">Flower </span> Bouquets</h1>
									<h2>Tentang Nano Teknologi</h2>
									<p>Istilah Nano Teknologi pertama kali diresmikan oleh Prof Norio Taniguchi dari Tokyo Science University pada tahun 1974, dalam makalahnya yang berjudul “On The Basic Concept of Nano-Technology”</p>
									<button type="button" class="btn btn-default get">Lihat Artikel</button>
								</div>
								<div class="col-sm-6">
					<img src="{{ asset('theme/citraflorist/images/banner/slider1.jpg') }}" style="max-height:500px" class="img-lazy" alt="keiskei 17 Agustus">
									<img src="images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span style="color:red">10%</span>-Diskon</h1>
									<h2>Indent Sekarang</h2>
									<p>Berlaku 7 - 31 Mei 2016. Pengiriman Serentak pada 1 juni 2016 </p>
									<button type="button" class="btn btn-default get">Beli Produk</button>
								</div>
								<div class="col-sm-6">
					<img src="{{ asset('theme/citraflorist/images/banner/slider1.jpg') }}" style="max-height:500px" class="img-lazy" alt="keiskei 17 Agustus">
									<img src="images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span style="color:red">K</span>-PARTNER</h1>
									<h2>BISNIS K-PARTNER DENGAN SYSTEM TERBARU</h2>
									<p>Dapatkan berbagai kemudahan dan keuntungan menjadi partner kami. </p>
									<button type="button" href="{!! route('register') !!}" class="btn btn-default get">Daftar Sekarang</button>
								</div>
								<div class="col-sm-6">
					<img src="{{ asset('theme/citraflorist/images/banner/slider1.jpg') }}" style="max-height:500px" class="img-lazy" alt="keiskei 17 Agustus">
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->