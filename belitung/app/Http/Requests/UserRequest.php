<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$user = User::find($this->route()->getParameter('user'));
	    switch($this->method())
	    {
	        case 'GET':
	        case 'DELETE':
	        {
	            return [];
	        }
	        case 'POST':
	        {
	            return [
	                'username' => 'required|unique:ms_users',
					'email' => 'required|email|unique:ms_users',
					'ms_country_id' => 'required',
					'photo' => 'image'
	            ];
	        }
	        case 'PUT':
	        case 'PATCH':
	        {
	            return [
	                'username' 		=> 'required|unique:ms_users,username,'.$user->id,
					'email' 		=> 'required|email|unique:ms_users,email,'.$user->id,
					'photo' 		=> 'image'
	            ];
	        }
	        default:break;
	    }
	}

}
