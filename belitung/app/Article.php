<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryArticle;
use App\User;

class Article extends Model {

	protected $table = 'ms_articles';
	protected $guarded = ['id'];

	public function categoryArticle()
	{
		return $this->belongsTo('App\CategoryArticle', 'ms_category_article_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}
}
