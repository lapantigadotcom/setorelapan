@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Folders</h3>
                  <div class='box-tools'>
                    <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  @include('pages.user.sidebar')
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Affiliate User</h3>   
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>{!! $data['content']->name !!} 
                    </h3>
                    <h4>Code : {!! $data['content']->code !!}</h4>
                    <h4>Group : {!! $data['content']->roles->first()->name !!}
                    <span class="mailbox-read-time pull-right">
                            {!! $data['content']->enabled=='1'?"<span class='label label-primary'>Enabled</span>":"<span class='label label-danger'>Disabled</span>" !!}
                            {!! $data['content']->approved=='1'?"<span class='label label-primary'>Approved</span>":"<span class='label label-danger'>Dispproved</span>" !!}
                            {!! $data['content']->safe=='1'?"<span class='label label-primary'>Safe</span>":"<span class='label label-danger'>Unsafe</span>" !!}
                        </span>
                    </h4>
                    <h5>Username : {!! $data['content']->username !!}</h5>
                    <h5>Email: {!! $data['content']->email !!} <span class="mailbox-read-time pull-right">Register at: {!! date('d-m-Y H:i:s', strtotime($data['content']->created_at)) !!}</span></h5>
                  </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <table class="table" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Group</th>
                                <th>Email</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                        @foreach($data['content']->referral as $row)
                            <tr>
                                <td>{!! $no++ !!}</td>
                                <td><a href="{!! route('ki-admin.user.show',[$row->id]) !!}">{!! $row->name !!}</a></td>
                                <td>{!! $row->roles->first()->name !!}</td>
                                <td>{!! $row->email !!}</td>
                                <td>{!! $row->approved=='1'?"<span class='label label-primary'>Active</span>":"<span class='label label-default'>Inactive</span>" !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
                    
                </section><!-- /.content -->
@include('scripts.delete-modal')
@include('scripts.reset-password-modal')
@endsection


@section('custom-head')
    {!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection