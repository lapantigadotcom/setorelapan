<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\NotifMessage;
use App\User;
use App\Role;
use Input;
use Auth;
use App\Http\Requests\NotifMessageEmployeeRequest;

class NotifMessageEmployeeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = NotifMessage::with('recipient')->get();
		return view('pages.notif-message-employee.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('0' => 'Individual', '1' => 'Group');
		$data['receiver'] = User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('name', 'id');
		return view('pages.notif-message-employee.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotifMessageEmployeeRequest $request)
	{
		$data['ms_user_id'] = Auth::user()->id;
		$data['group'] = $request->input('group');
		$data['subject'] = $request->input('subject');
		$data['content'] = $request->input('content');
		if($data['group'] == '0') {
			$data['sent'] = '1';
			$data['draft'] = '0';
			$data['ms_user_recipient_id'] = $request->input('receiver');
			NotifMessage::create($data);
		}
		else if($data['group'] == '1') {
			$receivers = User::whereHas('roles', function($q) use($request) {
				$q->where('ms_roles.id', '=', $request->input('receiver'));
			})->get();
			foreach($receivers as $receiver) {
				$data['sent'] = '1';
				$data['draft'] = '0';
				$data['ms_user_recipient_id'] = $receiver->id;
				NotifMessage::create($data);
			}
		}
		return redirect()->route('ki-admin.notif-message.outbox');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = NotifMessage::with('recipient')->find($id);
		return view('pages.notif-message-employee.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		NotifMessage::destroy($id);
		return redirect()->route('ki-admin.notif-message.outbox');
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '=', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '=', '1')->get();
		}
		return response()->json($receiver);
	}

}
