<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
 
 





  @include('theme.keiskei.partials.head')
 



</head>
<body class="boxed">
<div class="boxed-container">
	@include('theme.keiskei.partials.menu')
 
 		@yield('content')
 		@include('theme.keiskei.partials.footer')
	</div>
  @include('theme.keiskei.partials.script')		
</body>
</html>