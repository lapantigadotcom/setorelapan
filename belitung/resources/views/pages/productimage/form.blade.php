<div class="box-body">
	<h3>Add image</h3>
	@if(isset($data['ms_product_id']))
		{!! Form::hidden('ms_product_id', $data['ms_product_id'], array('class' => 'form-control')) !!}
	@endif
	<div class="form-group">
		{!! Form::label('caption', 'Caption') !!}
		{!! Form::textarea('caption', null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('new-image', 'Choose file') !!}
		{!! Form::file('new-image') !!}
	</div>
	<div class="checkbox">
		<label>{!! Form::checkbox('featured', '1') !!} <b>Featured</b></label>
	</div>
	<div class="form-group">
		{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
	</div>
</div>