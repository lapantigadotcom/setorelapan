 <div class="services-area margin-top-60">
              <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class=" section-title">
                        <h2 style="font-size:4em">Citra Florist</h2><br><br> 
                            <p>Perusahaan yang bergerak di bidang sektor rangkaian dan karangan bunga yang berlokasi di Surabaya, Jawa Timur tepatnya di sentra florist Pasar Bunga Kayoon. </p>
                            <p>Berdiri sejak tahun 2008, bisnis kami berawal dari toko bunga di Surabaya (florist) yang menjual kreasi aneka bunga segar seperti Papan Bunga Ucapan, Bunga Parcel, Bunga Buket, Bunga Salib, Bunga Peti Mati, Bunga Artificial, Bunga Forhand, Bunga Krans, dll.</p>
                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-6">
                        <div class="services-right floatright">
                            <div class="box box-1">
                                <h1>Detail</h1>
                                <p>Memperhatikan detail warna, jenis bunga, aksesoris pelengkap, bentuk atau layout</p>
                            </div>
                            <div class="box box-2">
                                <h1>Beragam </h1>
                                <p>Menampilkan beragam tema rangkaian bunga, disesuaikan dengan permintaan</p>
                            </div>
                            <div class="box box-3">
                                <h1>Workarea</h1>
                                <p>Area layanan kami meliputi seluruh Jawa, Bali dan Madura</p>
                            </div>
                            <div class="box box-4">
                                <h1>Profesional</h1>
                                <p>Mengedepankan kualitas dan kepuasan pelanggan juga merupakan komitmen kami</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>