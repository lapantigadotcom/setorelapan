@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Transfer</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
          
          @include('pages.transfer.menu')
        </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Show Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>Customer : {!! $data['content']->user->name !!}</h3>
                    <h4>Role : {!! $data['content']->user->roles->first()->name !!} </h4>
                    <h5>Status : {!! $data['content']->status=='1'?"<span class='label label-primary'>Approved</span>":($data['content']->status=='2'?"<span class='label label-danger'>Unnaproved</span>":"<span class='label label-warning'>Pending</span>") !!} </h5>

                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-read-message">
                    <h4>Transfer Detail :</h4>
                    <table class="table">
                      <tr>
                        <td>Account Name</td>
                        <td>:</td>
                        <td>{!! $data['content']->account_name !!}</td>
                      </tr>
                      <tr>
                        <td>Account Number</td>
                        <td>:</td>
                        <td>{!! $data['content']->account_number !!}</td>
                      </tr>
                      <tr>
                        <td>Nominal</td>
                        <td>:</td>
                        <td>{!! $data['content']->nominal !!}</td>
                      </tr>
                      <tr>
                        <td>Transfer Date</td>
                        <td>:</td>
                        <td>{!! date('d-m-Y',strtotime($data['content']->transfer_date)) !!}</td>
                      </tr>
                      <tr>
                        <td>File</td>
                        <td>:</td>
                        <td>
                        @if(File::exists('data/user/transfer/'.$data['content']->file) && !empty($data['content']->file))
                          <a href="{!! asset('data/user/transfer/'.$data['content']->file) !!}" target="_blank" class="btn btn-primary btn-sm">Download</a>
                        @else
                          Not uploaded.
                        @endif

                        </td>
                      </tr>
                      <tr>
                        <td>Catatan</td>
                        <td>:</td>
                        <td>{!! $data['content']->information !!}</td>
                      </tr>
                    </table>
                  </div><!-- /.mailbox-read-message -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                </div><!-- /.box-footer -->
                <div class="box-footer">
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>
</section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection