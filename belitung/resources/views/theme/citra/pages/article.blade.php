@extends('theme.citra.app')

@section('content')
<title>citra Indonesia | {!! $data['content']->title !!}</title>  
<meta name="twitter:description" content="{!! $data['content']->meta_description !!}" />
<meta property="og:type" content="website" />
<meta name="DC.description" content="{!! $data['content']->meta_description !!}" />
<meta name="DC.subject" content="{!! $data['content']->meta_keyword !!}" />
<meta name="description" content="{!! $data['content']->meta_description !!}">
<meta name="keywords" content="{!! $data['content']->meta_keyword !!}"/>
<meta name='robots' content='INDEX, FOLLOW' />
 
<!-- breadcrumbs-area-area start -->
		 <div class="breadcrumbs-area log">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumbs-menu">
							<ul>
								<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
 								<li><a href="#">{!! $data['content']->title !!} <span>></span></a></li>

						</div>
					</div>
				</div>
			</div>
		 </div>
    <!-- breadcrumbs-area-area end -->

    <!-- shop-page-start -->
    <div class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="left-sidebar">
        				@include('theme.citra.include.sidebar-default')</div>
	                </div>
                 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="blog-all b-details">
                        
                        <div class="posttitle">
                            <div class="blog-top">
                                <h2>{!! $data['content']->title !!} </h2>
                                <h6> Published date:&nbsp; {!! date('l, j F Y') !!} &nbsp; | &nbsp; Author:&nbsp; Citra Florist
                            </div>
                            <div class="blog-bottom">
                                <p>{!! $data['content']->description !!} </p>
                            </div>
                            Share This: &nbsp; <div class="addthis_sharing_toolbox"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div> 
    @include('theme.citra.include.klien')
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>
 	@endsection
		
	 
  