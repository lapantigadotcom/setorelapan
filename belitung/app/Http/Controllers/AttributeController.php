<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Attribute;
use App\TypeAttribute;
use App\Http\Requests\AttributeRequest;

use Session;

class AttributeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Attribute::with('typeAttribute')->get();
		return view('pages.attribute.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['typeattribute'] = TypeAttribute::all();

		return view('pages.attribute.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AttributeRequest $request)
	{
		Attribute::create($request->all());
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('ki-admin.attribute.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['typeattribute'] = TypeAttribute::all();
		$data['content'] = Attribute::find($id);
		return view('pages.attribute.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AttributeRequest $request, $id)
	{
		$data = Attribute::find($id);
		$data->update($request->all());
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.attribute.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Attribute::
			with(
				'detailAttributes',
				'detailAttributes.detailAttributeProducts',
				'detailAttributes.detailAttributeProducts.detailAttributeProductDetailOrders'
			)
			->destroy($id);
		foreach ($data->detailAttributes as $row) {
			foreach ($row->detailAttributeProducts as $val) {
				foreach ($val->detailAttributeProductDetailOrders as $vall) {
					$vall->delete();
				}
				$val->delete();
			}
			$row->delete();
		}
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.attribute.index');
	}

}
