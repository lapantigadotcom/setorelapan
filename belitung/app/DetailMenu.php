<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Menu;

class DetailMenu extends Model {

	protected $table = 'ms_detail_menus';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function menu()
    {
    	return $this->belongsTo('App\Menu', 'ms_menu_id', 'id');
    }
    public function submenu()
    {
    	return $this->hasMany('App\Menu','parent_id');
    }
    public function owned()
    {
    	return $this->morphTo();
    }
}
