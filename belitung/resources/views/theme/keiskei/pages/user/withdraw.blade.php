@extends('theme.keiskei.app')
<title>Keiskei Indonesia | Withdraw Bonus Anggota </title>

@section('content')
@include('theme.keiskei.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Withdraw Bonus Anggota</h5>
		</div>
	</div>
</div>

				<section id="cart_items">
				 	<div class="container">
					<!-- #tabel perwakilan-->
										<div class="shopper-informations">
											<div class="row">
												@include('errors.session')
												<div class="col-sm-3">
												<div class="shopper-info">
													<p>Rincian Bonus</p>
												</div>
												</div>
											</div>
										</div>

									<div class="table-responsive cart_info">
								<table class="table table-condensed">
									<thead>
										<tr class="cart_menu">
											<td class="image">No</td>
											<td class="description">Tahun</td>
											<td class="price">Bulan</td>
											<td class="price">Bonus Afiliasi</td>
											<td class="price">Bonus Bulanan</td>
											<td class="price">Status</td>

											<td></td>
										</tr>
									</thead>
									<tbody>
										<tr><?php $no=1; ?>
											@foreach($data['content'] as $row)
											<td class="cart_product">
											{!! $no++ !!}
											</td>
											<?php 
											$dateObj   = DateTime::createFromFormat('!m', $row->month);
											$monthName = $dateObj->format('F');
											unset($dateObj);
											?>
											<td class="cart_description" style="font-size:14px">
											{!! $row->year !!}
				 							</td>
											<td class="cart_price" style="font-size:14px">
											 {!! $monthName !!}
											</td>

											<td>{!! money_format('%(#10n',$row->referral_value) !!}
												 
												</td>
												<td>
													 {!! money_format('%(#10n',intval($row->monthly_value)) !!}
												</td>
												<td>
												{!! $row->status=='1'?"<span class='label label-default'>Tidak Aktif</span><p>".date('d M Y',strtotime($row->withdraw_date))."</p>":$row->status=='2'?"<span class='label label-warning'>Konfirmasi</span>":"<span class='label label-primary'>Bonus Aktif</span>" !!}
												</td>
												
										</tr>@endforeach
										
									</tbody>
								</table>
							</div>
							<div class="row">
											<div class="col-md-12 text-center">
												@if(Auth::user()->approved == '1')
				 									<a href="{!! route('user.withdraw.get') !!}" class="btn btn-primary">Withdraw Bonus</a>
													@else
													<a href="javascript:void(0)" class="btn btn-primary" onclick="withdrawModal()">Withdraw Bonus</a>
													@endif
				 							</div>
										</div>
										   
					</div>
				</section><br><br>

<div class="modal fade" id="withdrawPoint" tabindex="-1" role="dialog" aria-labelledby="withdrawPoint1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="withdrawPoint1">Withdraw Point</h4>
      </div>
      <div class="modal-body">
        <h4>Maaf, Withdraw hanya dapat dilakukan pada akhir bulan.</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>


 @endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function withdrawModal () {
		    $('#withdrawPoint').modal({ backdrop: 'static', keyboard: false });
		    
		  }
	</script>
@endsection

@section('custom-head')
	
@endsection