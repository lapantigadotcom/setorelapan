<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use App\Http\Requests\NotificationRequest;

class NotificationController extends BasicController {
	protected $GCM_API = 'AIzaSyDU78ChBtG2Wl0cdp8GQu567lOEA3EICZU';
	protected $path;
	function __construct() {
		
		$this->path = 'data/notification/';

		parent::__construct();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = \App\Notification::with('recipient')->get();
		return view('pages.notification.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['group'] = array('1' => 'Group');
		$data['receiver'] = \App\User::whereHas('roles', function($q) {
			$q->where('locked', '<>', '1');
		})->lists('email', 'email');
		return view('pages.notification.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotificationRequest $request)
	{
		$data = array();
		// $data['ms_user_id'] = Auth::user()->id;
		$data['ms_role_id'] = $request->input('receiver');
		$data['title'] = $request->input('title');
		$data['description'] = $request->input('description');
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = "_photo_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$data['photo']= $fileName;
		}
		$data = \App\Notification::create($data);
		$group =  $request->input('group');

		$json = array();
		$json['RESP'] = "SCSNTF";
		$json['MESSAGE'] = 'Notification berhasil diterima.';
		$json['DATA'] = $data->toArray();
		if($group == '0') {
			// $data['receiver'] = \App\User::find($request->input('receiver'))->email;
			// if($this->sendNotif($data)) {
			// 	$data['sent'] = '1';
			// 	$data['draft'] = '0';
			// 	$data['ms_user_recipient_id'] = $request->input('receiver');
			// 	\App\Notification::create($data);
			// }
		}
		else if($group == '1') {
			$rcv = $request->input('receiver');
			if($rcv == '0')
			{
				$receivers = \App\GCM::all();
				foreach($receivers as $receiver) {
					$this->sendGCM( $json, $receiver->gcm_id );
				}
			}else{
				$receivers = \App\User::whereHas('roles', function($q) use($request) {
					$q->where('ms_roles.id', '=', $request->input('receiver'));
				})->get();
				foreach($receivers as $receiver) {
					if($receiver->gcm()->count() > 0)
					{
						$this->sendGCM( $json, $receiver->gcm->gcm_id );
					}
				}
			}
		}
		return redirect()->route('ki-admin.notification.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = \App\Notification::find($id);
		return view('pages.notification.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = \App\Notification::find($id);
		if(File::exists($this->path.$data->photo))
				File::delete($this->path.$data->photo);
		\App\Notification::destroy($id);
		return redirect()->route('ki-admin.notification.index');
	}

	protected function sendNotif($data)
	{
		if(Mail::send('theme.'.$this->theme->code.'.include.notification',$data, function($message) use($data)
		{
			$message->subject($data['subject']);
			$message->to($data['receiver']);
		})) {
			return true;
		}
		else {
			return false;
		}
	}

	public function getReceiverJson()
	{
		$tipe = Input::get('tipe');
		if($tipe == '0') {
			$receiver = User::whereHas('roles', function($q) {
							$q->where('locked', '<>', '1');
						})->get();
		}
		else if($tipe == '1') {
			$receiver = Role::where('locked', '<>', '1')->get();
		}
		return response()->json($receiver);
	}
	protected function sendGCM( $data, $ids )
	{
	$url = 'https://android.googleapis.com/gcm/send';
	    $post = array(
	                    'registration_ids'  => array($ids),
	                    'data'              => array( "message" => $data),
	                    );
	// var_dump(json_encode($post));
	    //------------------------------
	    // Set CURL request headers
	    // (Authentication and type)
	    //------------------------------

	    $headers = array( 
	                        'Authorization: key=' . $this->GCM_API,
	                        'Content-Type: application/json'
	                    );

	    //------------------------------
	    // Initialize curl handle
	    //------------------------------

	    $ch = curl_init();

	    //------------------------------
	    // Set URL to GCM endpoint
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_URL, $url );

	    //------------------------------
	    // Set request method to POST
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_POST, true );

	    //------------------------------
	    // Set our custom headers
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	    //------------------------------
	    // Get the response back as 
	    // string instead of printing it
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

	    //------------------------------
	    // Set post data as JSON
	    //------------------------------

	    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

	    //------------------------------
	    // Actually send the push!
	    //------------------------------

	    $result = curl_exec( $ch );

	    //------------------------------
	    // Error? Display it!
	    //------------------------------

	    if ( curl_errno( $ch ) )
	    {
	         echo 'GCM error: ' . curl_error( $ch );
	         exit();
	    }

	    //------------------------------
	    // Close curl handle
	    //------------------------------
		// var_dump($result);exit();
	    curl_close( $ch );

	    //------------------------------
	    // Debug GCM response
	    //------------------------------


	    return;
	}
}
