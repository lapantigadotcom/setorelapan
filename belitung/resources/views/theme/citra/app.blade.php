<!DOCTYPE html>
<html>
<head>
  @include('theme.citra.partials.head')
 </head>
<body>    
    <div class="home-page-1">

	@include('theme.citra.partials.menu')
	@yield('content')

	@include('theme.citra.partials.footer')
</div>

	@include('theme.citra.partials.script')		
</body>
</html>