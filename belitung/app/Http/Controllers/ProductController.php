<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Brand;
use App\Currency;
use App\CategoryProduct;
use App\Role;
use App\StatusProduct;
use App\Product;
use App\Http\Requests\ProductRequest;
use DB;
use App\Price;
use App\ProductImage;
use Image;
use File;
use App\Attribute;
use App\DetailAttribute;
use App\DetailAttributeProduct;
use Session;
use Input;

class ProductController extends Controller {

	protected $path;

	function __construct() {
		$this->path = 'data/product/';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Product::with('brand', 'categoryProduct', 'statusProduct', 'detailAttributeProducts', 'prices', 'productImages')->get();
		return view('pages.product.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['currency'] = Currency::all();
		$data['role'] = Role::where('locked', '<>', '1')->lists('name', 'id');
		$data['brand'] = Brand::lists('name', 'id');
		$data['categoryProduct'] = CategoryProduct::lists('name', 'id');
		$data['statusProduct'] = StatusProduct::lists('name', 'id');
		$data['message'] = 'You must save this product before adding other attribute';
		return view('pages.product.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProductRequest $request)
	{
		$product = Product::create($request->except('files'));
		$brandId = $request->input('ms_brand_id');
		$brand = Brand::find($brandId);
		$product->code = strtoupper(substr($brand->name, 0, 2)) . sprintf("%05d", $product->id);
		$product->save();
		if ($request->hasFile('files'))
		{
		    	$files = $request->file('files');
			$extension = $files->getClientOriginalExtension();
			if (!File::exists($this->path))
			{
				File::makeDirectory($this->path, $mode = 0777, true, true);
			}
			$fileName = $product->code."_files".$extension;
			$request->file('files')->move($files->getRealPath(), $this->path.$fileName);
			$product->files = $this->path.$fileName;
			$product->save();
		}
		Session::flash('success','Data berhasil disimpan');
		return redirect()->route('ki-admin.product.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['attribute'] = Attribute::with('detailAttributes')->get();
		$data['currency'] = Currency::all();
		$data['role'] = Role::where('locked', '<>', '1')->lists('name', 'id');
		$data['brand'] = Brand::lists('name', 'id');
		$data['categoryProduct'] = CategoryProduct::lists('name', 'id');
		$data['statusProduct'] = StatusProduct::lists('name', 'id');
		$data['content'] = Product::with('brand', 'categoryProduct', 'statusProduct', 'detailAttributeProducts', 'prices', 'productImages', 'prices.currency', 'stockProducts')->find($id);
		return view('pages.product.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ProductRequest $request, $id)
	{
		try {
			DB::transaction(function() use ($request, $id)
			{
				$data = Product::find($id);
				$data->update($request->except('files'));
				$roles = Role::where('locked', '<>', '1')->get();
				$currencies = Currency::all();
				foreach ($roles as $role) {
					foreach ($currencies as $currency) {
						$price = Price::where('ms_role_id', '=', $role->id)
									  ->where('ms_currency_id', '=', $currency->id)
									  ->where('ms_product_id', '=', $id)
									  ->first();
						if(empty($price)) {
							$price = new Price;
						}
						$inputPrice = str_replace(".","",$request->input('price-'.$role->id.'-'.$currency->id));
						$price->ms_role_id = $role->id;
						$price->ms_product_id = $id;
						$price->ms_currency_id = $currency->id;
						$price->price = $inputPrice;
						$price->save();
					}
				}
				$attributes = Attribute::all();
				foreach($attributes as $attribute) {
					foreach($attribute->detailAttributes as $detailAttribute) {
						$detailAttributeProduct = DetailAttributeProduct::where('ms_detail_attribute_id', '=', $detailAttribute->id)
						->where('ms_product_id', '=', $id)->first();
						$inputEnabled = $request->input('attribute-'.$attribute->id.'-'.$detailAttribute->id);
						$enabled = !empty($inputEnabled) ? $inputEnabled : '0';
						if(empty($detailAttributeProduct)) {
							$detailAttributeProduct = new DetailAttributeProduct;
						}
						$detailAttributeProduct->ms_detail_attribute_id = $detailAttribute->id;
						$detailAttributeProduct->ms_product_id = $id;
						$detailAttributeProduct->enabled = $enabled;
						$detailAttributeProduct->save();
					}
				}
				if ($request->hasFile('files'))
				{
				    $files = $request->file('files');
					$extension = $files->getClientOriginalExtension();					
					if (!File::exists($this->path))
					{
						File::makeDirectory($this->path, $mode = 0777, true, true);
					}
					$fileName = $data->code."_files".".".$extension;
					if(File::exists($data->files))
					File::delete($data->files);
					$request->file('files')->move($this->path, $fileName);
					$data->files = $this->path.$fileName;
					$data->save();
				}
				Session::flash('success','Data berhasil diperbarui');
			});
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Product::with('detailAttributeProducts','prices','detailOrders','detailOrders.detailAttributeProductDetailOrders','reviews','stockProducts')->find($id);
		foreach ($data->detailAttributeProducts as $row) {
			$row->delete();
		}
		foreach ($data->prices as $row) {
			$row->delete();
		}
		foreach ($data->reviews as $row) {
			$row->delete();
		}
		foreach ($data->stockProducts as $row) {
			$row->delete();
		}
		foreach ($data->detailOrders as $row) {
			foreach ($row->detailAttributeProductDetailOrders as $val) {
				$val->delete();
			}
			$row->delete();
		}
		foreach ($data->productImages as $row) {
			if(File::exists($row->image))
				File::delete($row->image);
			$row->delete();
		}
		if(File::exists($data->files)) {
			File::delete($data->files);
		}
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.product.index');
	}
	public function getjson()
	{
		$product_id = Input::get('product_id');
		$data = \App\Product::find($product_id);
		return response()->json($data);
	}
	
	public function downloadFile($id)
	{
		$data = Product::find($id);
		if(File::exists($data->files)) {
			return response()->download($data->files);
		}
		return redirect()->back();
	}
	
	public function deleteFile($id)
	{
		$data = Product::find($id);
		if(File::exists($data->files)) {
			File::delete($data->files);
			$data->files = null;
			$data->save();
		}
		return redirect()->back();
	}

}