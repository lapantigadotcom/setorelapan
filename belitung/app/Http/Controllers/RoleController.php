<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AdminRoleRequest;
use App\Http\Requests\RoleRequest;
use App\Role;
use Illuminate\Http\Request;
use Input;

class RoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['admin'] = Role::where('locked','1')->get();
		$data['user'] = Role::where('locked','<>','1')->get();
		return view('pages.role.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.role.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Role::find($id);
		return view('pages.role.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RoleRequest $request, $id)
	{
		$data = Role::find($id);
		$data->update($request->all());
		return redirect()->route('ki-admin.role.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

	public function getjson()
	{
		$id = Input::get('id');
		$data = Role::find($id);
		return response()->json($data);
	}
	public function editAdmin($id)
	{
		$data['adminmenu'] = \App\Adminmenu::lists('name','id');
		$data['permission'] = \App\Permission::lists('route','id');

		$data['content'] = Role::with('adminmenu','permission')->find($id);
		$data['adminmenu_t'] = $data['content']->adminmenu->lists('id');
		$data['permission_t'] = $data['content']->permission->lists('id');
		
		return view('pages.role.editadmin', compact('data'));
	}
	public function updateRoleAdmin(AdminRoleRequest $request, $id)
	{
		$data = Role::find($id);
		$data->name = $request->input('name');
		$tmp = $request->input('enabled');
		if(empty($tmp))
		{
			$data->enabled = '0';
		}else
		{
			$data->enabled = '1';
		}
		$data->save();
		$data->adminmenu()->detach();
		if(is_array($request->input('adminmenu')))
		{
			foreach ($request->input('adminmenu') as $row) {
				$data->adminmenu()->attach($row);
			}
		}
		$data->permission()->detach();
		if(is_array($request->input('permission')))
		{
			foreach ($request->input('permission') as $row) {
				$data->permission()->attach($row);
			}
		}
		return redirect()->route('ki-admin.role.index');
	}
}
