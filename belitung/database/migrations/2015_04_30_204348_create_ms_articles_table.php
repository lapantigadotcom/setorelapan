<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 64)->nullable();
			$table->string('description', 512)->nullable();
			$table->integer('ms_category_article_id')->index('fk_ms_articles_ms_category_article_idx');
			$table->integer('ms_user_id')->index('fk_ms_articles_ms_user_idx');
			$table->string('meta_description', 256)->nullable();
			$table->string('meta_keyword', 128)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_articles');
	}

}
