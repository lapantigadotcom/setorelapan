@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        {!! Form::model($data['content'],array('route' => ['ki-admin.role.update',$data['content']->id], 'method' => 'PUT')) !!}
                        {!! BootstrapForm::open(array('model' => $data['content'], 'store' => 'ki-admin.role.store', 'update' => 'ki-admin.role.update')) !!}
                            @include('pages.role.form',array('submit' => 'Perbarui'))
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection