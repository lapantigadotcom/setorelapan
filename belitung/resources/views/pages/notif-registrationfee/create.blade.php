@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Notification
    <small>Email</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">
      <a href="{!! route('ki-admin.notif-email.index') !!}" class="btn btn-primary btn-block margin-bottom">Back to Sent Folder</a>
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Folders</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="{!! route('ki-admin.notif-email.index') !!}"><i class="fa fa-envelope-o"></i> Sent</a></li>
          </ul>
        </div><!-- /.box-body -->
      </div><!-- /. box -->
    </div><!-- /.col -->
    <div class="col-md-9" >
      {!! Form::open(array('route' => 'ki-admin.notif-email.store', 'method' => 'POST')) !!}
      @include('pages.notif-email.form',array('submit' => 'Send'))
      {!! Form::close() !!}
    </div>
  </div>
</section>

@endsection


@section('custom-head')
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
<script type="text/javascript">
  $(document).ready(function() {
   $('#receiver').selectize();
   $('#group').change(function() {
    var tipe = $('#group').val();
    changeReceiver(tipe);
  });
 });
  $(function () {
    var tipe = $('#group').val();
    changeReceiver(tipe);
  });
  function changeReceiver(x)
  {
    $.get("{!! route('ki-admin.notif-email.getReceiverJson') !!}", {tipe : x}, function(data, status) {
      if(status == 'success') {
        var content = [];
        $.each(data, function(key, value) {
          var temp = {'text' : value.name, 'value' : value.id};
          content.push(temp);
        });
        console.log(content);
        var selectize = $("#receiver")[0].selectize;
        selectize.clear();
        selectize.clearOptions();
        selectize.load(function(callback) {
          callback(content);
        });
      }
    });
  };
</script>
@endsection