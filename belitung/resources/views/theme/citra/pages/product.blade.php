@extends('theme.citra.app')



@section('content')
 <meta content='Citra Florist Group' property='og:title'/>
<meta content='Silakan klik masing-masing banner untuk terhubung ke website jaringan kami lainnya.  Klik Next atau Previous Button dibawah untuk melihat ar...' property='og:description'/>
<meta content='https://3.bp.blogspot.com/-NlJeTFD3_2Y/VlxJvyai_pI/AAAAAAAABsg/UxVoms-Wq4s/w1200-h630-p-nu/banner-toko-bunga-surabaya-online-net.jpg' property='og:image'/>
<!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
<title>Toko Bunga Surabaya Online | Citra Florist Group</title> 
<meta name="p:domain_verify" content="05e6dcbffcea28ff89468960b463c7bc"/>
<meta name='robots' content='INDEX, FOLLOW' />
				<!-- breadcrumbs-area-area start -->
					 <div class="breadcrumbs-area log">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="breadcrumbs-menu">
										<ul>
											<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
			 								<li><a href="#">Katalog Produk <span>></span></a></li>

									</div>
								</div>
							</div>
						</div>
					 </div>
			    <!-- breadcrumbs-area-area end -->
 
   <div class="shop-product-area">
			<div class="container">
				<div class="row">
					 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="left-sidebar">
        				@include('theme.citra.include.sidebar-default')
        			</div>
	                </div>

					<div class="col-lg-9 col-md-9 col-sm-12">
					    <div class="shop-banner">
					        <img src="{!! asset('theme/citraflorist/img/banners/banner_produk_citraflorist.jpg') !!}" alt="">
					    </div>
						<div class="category-products">
							<div class="row"> 
								<div class="toolbar">
									<div class="col-lg-4 col-md-2 col-sm-3 col-xs-5">
								 <!-- Nav tabs -->
								<div class="view-mode">
									<ul role="tablist">
										<li class="active">
											<a href="#shop-square" data-toggle="tab"><i class="fa fa-th"></i></a>
										</li>
										<li>
											<a href="#shop-list" data-toggle="tab"><i class="fa fa-th-list"></i></a>
										</li>
									</ul>
								</div>
									</div>
									<div class="col-lg-4 col-md-5 col-sm-5 col-xs-7">
										<div class="limiter"><label>Pilih Kategori</label>
											<label><?php 
					array_unshift($data['categoryproduct'], 'Kategori');
					?>
					{!! Form::select('category',$data['categoryproduct'],Session::has('categorySelect')?Session::get('categorySelect'):null,['class' => 'form-control','id' => 'category','onchange' => 'changeCategory(this)']) !!}
										</label></div>
									</div>
									<div class="col-lg-4 col-md-5 col-sm-4 hidden-xs">
										<div class="sorter">
											<div class="sort-by floatright">
 										 <label>{!! Form::open(array('route' => 'search', 'method' => 'GET', 'class' => 'form-search')) !!}
{!! Form::text('search',null,['class' => 'form-control','placeholder' => 'Cari Produk ...']) !!}						

						 {!! Form::close() !!}</label>	
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="shop-square">
										
										@foreach($data['products'] as $row)
                                        <div class="col-lg-4 col-md-4 col-sm-6">
                                            <div class="single-product resnon">
                                                <a data-toggle="modal" title="Quick View" href="#productModal" class="view-single-product"><i class="fa fa-expand"></i></a>
                                                <div class="product-thumb">
                                                    <span class="label-pro-sale">Call</span>
                                                    <span class="new">Now</span>
                                                    @if($row->productImages()->count() > 0)
                                                @if(File::exists($row->productImages->first()->image))
                                            <img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
                                                @endif
                                                @endif

                                                </div>
                                                <div class="product-desc">
                                                    <h2 class="product-name">
                                                        <a class="product-name" href="{!! route('user.product.detail',$row->code, $row->title) !!}">
                                                            {!! $row->title !!}
                                                    </a></h2>
                                                    <div class="price-box floatleft">
                                                         <p class="normal-price">Call for Price</p>
                                                    </div>
                                                    <div class="product-action floatright">
                                                        <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-shopping-cart"></i></a>
                                                         <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-exchange"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            @include('theme.keiskei.include.review-content',['id' => 'review-content-'.$row->id, 'reviews' => $row->reviews ])
            @include('theme.keiskei.include.review',[ 'idP' => $row->id,'id' => 'review'.$row->id])
                                        @endforeach         
										
									</div>

									<div role="tabpanel" class="tab-pane" id="shop-list">
										<div class="col-md-12">
										    <div class="row">
                                                  
                                                @foreach($data['products'] as $row)
                                                <div class="li-item">
                                                    <div class="col-md-4 col-sm-4 col-xs-12 col-shop">
                                                        <div class="single-product shop6">
                                                            <div class="product-img">
                                                               @if($row->productImages()->count() > 0)
                                                @if(File::exists($row->productImages->first()->image))
                                            <img class="primary-images" src="{!! asset($row->productImages->first()->image) !!}" alt=" {!! $row->title !!}" />
                                                @endif
                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-sm-8 col-xs-12 col-shop">
                                                        <div class="f-fix">
                                                            <div class="content-box pro2">
                                                                <h2 class="product-name feil">
                                                                    <a href="{!! route('user.product.detail',$row->code, $row->title) !!}"> {!! $row->title !!}</a>
                                                                </h2>
                                                                <div class="shop-next">
                                                                  @if(isset($data['content']->files))
								@if(!empty($data['content']->files))
								@if(File::exists($data['content']->files))
								<a class="btn btn-default"  title="Download File Product" target='_blank' href="{!! asset($data['content']->files) !!}"><i class="fa fa-download"></i></a>
								@endif
								@endif
								@endif

                                                                </div>
                                                            </div>
                                                            <div class="p-box">
                                                                <span class="special-price">Call for price</span>
                                                            </div>
                                                            <p class="desc">{!! strip_tags(substr($row->description,0,200)).'...' !!}</p>
                                                            <div class="product-icon">
                                                                <a class="only" href="{!! route('user.product.detail',$row->code, $row->title) !!}"><i class="fa fa-shopping-cart"></i><span>Beli Produk</span></a>
                                                                 
                                                                <a class="on" href="{!! route('user.product.detail',$row->code, $row->title) !!}">
                                                                    <i class="fa fa-exchange"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @include('theme.keiskei.include.review-content',['id' => 'review-content-'.$row->id, 'reviews' => $row->reviews ])
            @include('theme.keiskei.include.review',[ 'idP' => $row->id,'id' => 'review'.$row->id])
                                                @endforeach 

                                            </div>
										</div>
									</div>
									<div class="pages">
                                        <ul>
                                              {!! $data['products']->render() !!} 
                                         </ul>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		 </div>
    @include('theme.citra.include.klien')


 @include('theme.keiskei.include.help',[ 'help' => 'Bantuan'])  
		 @endsection

@section('custom-footer')


	{!! HTML::script('theme/citraflorist/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/citraflorist/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/citraflorist/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function changeCategory(x)
	    {
	    	if(x.value==0)
	    	{
	    		window.location.replace("{!! route('product') !!}");
	    	}else
	    	{
		    	window.location.replace("{!! route('product') !!}/" + x.value + "-" + $(x).text());	    	
	    	}
	    }
	    function productHelp(x)
	    {
	    	var product_id = x.getAttribute("data-product");
	    	$.ajax(
		   	{
		   		url:"{{ route('product.getjson') }}",
		   		type: "GET",
		   		data:{
			      product_id : product_id
			    }
			}).done(function(data,status){
				if(status=='success')
			    {	
			    	console.log(data);
			    	$('#help-text').empty();
			    	$('#help-text').append(data.help);
			    }
			});
	    }
	</script>



@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}

	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection