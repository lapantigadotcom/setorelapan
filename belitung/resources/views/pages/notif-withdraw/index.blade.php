@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Withdrawal</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">
          @include('pages.notif-withdraw.menu')
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Lists</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>User</th>
                        <th>Month</th>
                        <th>Year</th>
                        <th>Referral Value</th>
                        <th>Monthly Value</th>
                        <th>Withdraw Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                        <td>&nbsp;</td>
                        <td>{{ $row->user->name }}</td>
                        <td>{{ $row->month }}</td>
                        <td>{{ $row->year }}</td>
                        <td>{{ $row->referral_value }}</td>
                        <td>{{ $row->monthly_value }}</td>
                        <td>{{ $row->withdraw_date }}</td>
                        <td>
                            {!! $row->status=='1'?"<span class='label label-primary'>Approved</span>":($row->status=='3'?"<span class='label label-default'>Unnaproved</span>":($row->status=='2'?"<span class='label label-warning'>Confirmation</span>":"<span class='label label-warning'>Confirmation</span>")) !!}
                        </td>
                        <td>
                          @if(Auth::user()->hasAccess('ki-admin.notif-withdraw.change') || Auth::user()->hasAccess('ki-admin.notif-withdraw.delete'))
                            <div class="btn-group">
                              <button class="btn btn-danger" type="button">Change</button>
                              <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul role="menu" class="dropdown-menu">
                              @if(Auth::user()->hasAccess('ki-admin.notif-withdraw.change'))
                                <li><a href="{!! route('ki-admin.notif-withdraw.change',[$row->id]).'?state=1' !!}">Approve</a></li>
                                <li><a href="{!! route('ki-admin.notif-withdraw.change',[$row->id]).'?state=3' !!}">Unapprove</a></li>
                              @endif
                                <li class="divider"></li>
                                @if(Auth::user()->hasAccess('ki-admin.notif-withdraw.delete'))
                                <li><a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.notif-withdraw.delete',[$row->id]) !!}">Delete</a></li>
                                @endif
                              </ul>
                            </div>
                          @endif
                          </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>
                        <th>User</th>
                        <th>Month</th>
                        <th>Year</th>
                        <th>Referral Value</th>
                        <th>Monthly Value</th>
                        <th>Withdraw Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection