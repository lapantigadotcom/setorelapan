{!! HTML::style('theme/keiskei/css/bootstrap.min.css') !!}

{!! HTML::script('theme/keiskei/js/jquery-1.11.2.min.js') !!}
{!! HTML::style('theme/keiskei/plugin/owl/owl.carousel.css') !!}
{!! HTML::style('theme/keiskei/plugin/owl/owl.theme.css') !!}
{!! HTML::style('theme/keiskei/plugin/fontawesome/css/font-awesome.min.css') !!}
{!! HTML::style('theme/keiskei/css/style-promo.css') !!}
@yield('custom-head')