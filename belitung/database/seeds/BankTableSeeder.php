<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Bank;

class BankTableSeeder extends Seeder {

    public function run()
    {
        Bank::truncate();

        Bank::create([
            'name' => 'BNI'
        ]);
        Bank::create([
            'name' => 'BCA'
        ]);
        Bank::create([
            'name' => 'BRI'
        ]);
        Bank::create([
            'name' => 'Mandiri'
        ]);
    }

}