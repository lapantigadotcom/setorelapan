<?php

use App\TransactionCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class TransactionCodeTableSeeder extends Seeder {

    public function run()
    {
        TransactionCode::truncate();
        TransactionCode::create([
            'name' => 'Saldo',
            'operand' => '=',
            'type' => 'Saldo'
        ]);
        TransactionCode::create([
            'name' => 'Setoran Deposit',
            'operand' => '+',
            'type' => 'Kredit'
        ]);
        TransactionCode::create([
            'name' => 'Transfer',
            'operand' => '+',
            'type' => 'Kredit'
        ]);
        TransactionCode::create([
            'name' => 'Poin Referral',
            'operand' => '+',
            'type' => 'Kredit'
        ]);
        TransactionCode::create([
            'name' => 'Transaksi Order',
            'operand' => '-',
            'type' => 'Debit'
        ]);
        TransactionCode::create([
            'name' => 'Penarikan Deposit',
            'operand' => '-',
            'type' => 'Debit'
        ]);

    }

}