<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\CategoryArticle;
use App\Article;
use App\Http\Requests\CategoryArticleRequest;

class CategoryArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = CategoryArticle::all();
		return view('pages.category-article.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.category-article.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CategoryArticleRequest $request)
	{
		CategoryArticle::create($request->all());
		return redirect()->route('ki-admin.category-article.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = CategoryArticle::find($id);
		return view('pages.category-article.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CategoryArticleRequest $request, $id)
	{
		$data = CategoryArticle::find($id);
		$data->update($request->all());
		return redirect()->route('ki-admin.category-article.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$categoryArticle = CategoryArticle::find($id);
		if($categoryArticle->locked == 0) {
			Article::where('ms_category_article_id', '=', $id)->delete();
			$categoryArticle->delete();
		}
		return redirect()->route('ki-admin.category-article.index');
	}

}
