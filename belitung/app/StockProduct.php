<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class StockProduct extends Model {

	protected $table = 'tr_stock_product';
	protected $guarded = ['id'];

    public function product()
    {
    	return $this->belongsTo('App\Product', 'ms_product_id', 'id');
    }

}
