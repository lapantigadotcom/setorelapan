@extends('theme.keiskei.app')

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Reset Password. </h5>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 form-container">
				<div class="row">
					<div class="col-md-12">
						@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif

						@if (count($errors) > 0)
						<div class="alert alert-danger">
						<strong>Perhatian!</strong> Terdapat kesalahan input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>
					
				</div>
				<div class="form-head text-center">
					<h5>Silahkan masukkan email akun Anda.</h5>
					<p></p>
					<hr>
				</div>
				<div class="form-body">
					{!! BootstrapForm::open(array('url' => url('/password/email'),'method' => 'POST')) !!}
					{!! BootstrapForm::text('email','Email',old('email') ) !!}
					<div class="form-group text-align form-action">
						{!! Form::submit('Reset',array('class' => 'btn btn-keiskei')) !!}
					</div>
					{!! BootstrapForm::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
