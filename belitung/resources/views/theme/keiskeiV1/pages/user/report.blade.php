@extends('theme.keiskei.app')

<title>Keiskei Indonesia | Laporan Anggota</title>


@section('content')
@include('theme.keiskei.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Beauty | Health | Cure </h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						@include('errors.session')
						<table class="table">
							<thead>
								<tr>
									<th>Kode Invoice</th>
									<th>Waktu</th>
									<th>Keterangan</th>
									<th>Total</th>
									<th>Opsi</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; ?>
							@foreach($data['content'] as $row)
							<tr>
								<td>
									<a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
										<b>{!! $row->code !!}</b>
									</a>
								</td>
								<td>{!! date('j M Y',strtotime($row->created_at)) !!}</td>
								<td>
									<b>{!! $row->statusOrder->name !!}</b>
									@foreach($row->transfers as $val)
									<p>
										<em>Konfirmasi pembayaran telah dilakukan: {!! $val->nominal !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
									</p>
									@endforeach
								</td>
								<td class="text-right">
								<?php 
									$total = 0; ?>
									@foreach($row->detailOrders as $val)
									<?php $total_t = priceDisplay($data['default_role'], $val->product->prices); 
									if(!empty($val->product->discount))
									{
										$discount_t = ($total_t/100)*intval($val->product->discount);
										$total_t = $total_t - $discount_t;
									}
									$total_t = $total_t*$val->total;
									$total += $total_t;
									?>
									@endforeach
									<?php 
									$total = $total + $row->shipping_price+ ($total*10/100)  ;
									?>
									{!! money_format('%(#10n', $total) !!}
								</td>
								<td>
									@if($row->ms_status_order_id=='2')
									<a href="{!! route('user.transfer.order',[$row->id]) !!}?invoice={!! $row->code !!}" class="btn btn-keiskei-orange"> 
										Bayar
									</a>
									@elseif($row->ms_status_order_id=='8' and $row->ms_user_id == Auth::id())
									<a href="{!! route('user.confirm-perwakilan',[$row->id]) !!}" class="btn btn-keiskei-orange"> 
										Konfirmasi
									</a>
									@endif
								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-12">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('theme.keiskei.include.user-sidebar')
			</div>
		</div>
	</div>
</div>
@include('theme.keiskei.partials.best-products')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection

@section('custom-head')
	
@endsection