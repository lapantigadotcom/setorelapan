<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model {

	protected $table = 'ms_banks';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('\App\User', 'ms_bank_id', 'id');
    }

}
