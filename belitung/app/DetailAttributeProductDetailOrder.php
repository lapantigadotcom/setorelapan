<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailAttributeProduct;
use App\DetailOrder;

class DetailAttributeProductDetailOrder extends Model {

	protected $table = 'tr_detail_attribute_product_detail_order';
	protected $guarded = ['id'];

	public function detailAttributeProduct()
	{
		return $this->belongsTo('\App\DetailAttributeProduct', 'ms_detail_attribute_product_id', 'id');
	}

	public function detailOrder()
	{
		return $this->belongsTo('\App\DetailOrder', 'tr_detail_order_id', 'id');
	}

}
