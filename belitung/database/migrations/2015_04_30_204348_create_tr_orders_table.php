<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 64)->nullable();
			$table->integer('ms_status_order_id');
			$table->integer('ms_user_id');
			$table->integer('ms_courier_id');
			$table->integer('ms_reseller_id');
			$table->integer('ms_payment_id');
			$table->integer('shipping_price');
			$table->string('service_courier', 64)->nullable();
			$table->date('due_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_orders');
	}

}
