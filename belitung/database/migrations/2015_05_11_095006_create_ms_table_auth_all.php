<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsTableAuthAll extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_roles', function($table)
        {
            $table->increments('id');
            $table->string('code', 16)->nullable();
			$table->string('name', 32)->nullable();
			$table->boolean('locked')->nullable();
			$table->boolean('enabled')->nullable();
			$table->boolean('default')->nullable();
			$table->boolean('require_file')->nullable();
            $table->integer('level')->nullable();
        });

        Schema::create('ms_adminmenus', function($table)
        {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->index()->nullable();
            $table->string('name')->index()->nullable();
            $table->string('route')->nullable();
            $table->integer('order_item')->nullable();
            $table->string('icon');
            $table->boolean('enabled')->nullable();
        });
        Schema::create('ms_permissions', function($table)
        {
            $table->increments('id');
            $table->string('route')->index()->nullable();
            $table->boolean('enabled')->nullable();
        });
        Schema::create('ms_role_user', function($table)
        {
            $table->increments('id');
            $table->integer('ms_user_id')->unsigned()->index()->nullable();
            $table->integer('ms_role_id')->unsigned()->index()->nullable();
        });
        Schema::create('ms_adminmenu_role', function($table)
        {
            $table->increments('id');
            $table->integer('ms_role_id')->unsigned()->index()->nullable();
            $table->integer('ms_adminmenu_id')->unsigned()->index()->nullable();
        });

        Schema::create('ms_permission_role', function($table)
        {
            $table->increments('id');
            $table->integer('ms_role_id')->unsigned()->index()->nullable();
            $table->integer('ms_permission_id')->unsigned()->index()->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('ms_roles');
        Schema::drop('ms_adminmenus');
        Schema::drop('ms_permissions');
        Schema::drop('ms_role_user');
        Schema::drop('ms_adminmenu_role');
        Schema::drop('ms_permission_role');
	}

}
