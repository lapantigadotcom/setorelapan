<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;

class PPNController extends Controller {

	public function index()
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['statusorder'] = \App\StatusOrder::where('id','<>','1')->lists('name','id');
		$data['content'] = \App\Order::where('ms_status_order_id','4')->orderBy('created_at','desc')->get();
		$data['bulan_ini'] = \App\Order::with('detailOrders','detailOrders.product','detailOrders.product.prices')->where('ms_status_order_id','4')->where(DB::raw('MONTH(created_at)'),date('n'))->where(DB::raw('YEAR(created_at)'), date('Y'))->get();
		return view('pages.ppn.index', compact('data'));
	}
}
