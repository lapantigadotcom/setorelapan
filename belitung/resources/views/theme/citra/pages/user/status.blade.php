@extends('theme.citra.app')

<title>citra Indonesia | Status Anggota</title>


@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>citra Indonesia</h2>
			<h5>Beauty | Health | Cure </h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12s">
						<table class="table">
							<thead>
								<tr>
									<th>No</th>
									<th>Waktu</th>
									<th>Deskripsi</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; ?>
							@foreach($data['content'] as $row)
								<tr>
									<td>{!! $no++ !!}</td>
									<td>{!! date('d M Y H:i:s',strtotime($row->created_at)) !!}</td>
									<td>{!! $row->description !!}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-12">
								{!! $data['content']->render() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('theme.citra.include.user-sidebar')
			</div>
		</div>
	</div>
</div>
@include('theme.citra.partials.best-products')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection

@section('custom-head')
	
@endsection