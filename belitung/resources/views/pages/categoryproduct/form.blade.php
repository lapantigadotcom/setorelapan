<div class="box-body">
	<div class="form-group">
		{!! Form::label('name','Name') !!}
		{!! Form::text('name',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('parent_id','Parent') !!}
		{!! Form::select('parent_id',$arrCategory,null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('description','Description') !!}
		{!! Form::textarea('description',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>