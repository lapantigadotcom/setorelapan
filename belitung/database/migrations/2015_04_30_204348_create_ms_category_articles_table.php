<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsCategoryArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_category_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 64)->nullable();
			$table->string('description', 512)->nullable();
			$table->boolean('locked');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_category_articles');
	}

}
