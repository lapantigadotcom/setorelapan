@extends('theme.citra.app')

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>citra Indonesia</h2>
			<h5>Beauty | Health | Cure </h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-1 col-md-10 form-container">
						<div class="form-head text-center">
							<h4>Konfirmasi Invoice citra PATNERS</h4>
							<hr>
						</div>
						<div class="form-body">
							<div class="col-md-12">
								<div class="row">
									@include('errors.session')
								</div>
								<div class="row">
									@if($errors->any())
									<ul class="alert alert-danger">
										@foreach($errors->all() as $row)
										<li>
											{!! $row !!}
										</li>
										@endforeach
									</ul>

									@endif
								</div>
								{!! Form::open(array('route' => ['user.confirm-perwakilan.store',$data['content']->id ], 'method' => 'POST','id' => 'form-validate')); !!}
								
								<div class="form-group">
									{!! Form::label('subject', 'Subject', array('class' => 'form-label')) !!}
									{!! Form::text('subject','Konfirmasi Invoice '.$data['content']->code.' [Perwakilan]', array('class' => 'form-control')) !!}
								</div>
								<div class="form-group">
									{!! Form::label('recipient', 'Tujuan Perwakilan', array('class' => 'form-label')) !!}
									{!! Form::text('recipient',$data['content']->reseller->name, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
								</div>
								<div class="form-group">
									{!! Form::label('content', 'Isi Pesan', array('class' => 'form-label')) !!}
									{!! Form::textarea('content',null, array('class' => 'form-control')) !!}
								</div>
							<div class="form-group text-align form-action">
								{!! Form::submit('Kirim',array('class' => 'btn  btn-citra')) !!}
								<a href="{!! route('user.report') !!}" class="btn btn-default">Kembali</a>
							</div>
							{!! BootstrapForm::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection

@section('custom-footer')
{!! HTML::script('theme/citra/plugin/validation/jquery.validate.min.js') !!}
<script type="text/javascript">
	$('#form-validate').validate({
		errorClass : "help-block error",
		rules: {
			subject : {
				required: true,
				minlength: 3
			},
			content : {
				required: true,
				minlength: 3
			}
		},
		messages: {
			subject : {
				required : "Mohon masukkan subjek",
				minlength : $.validator.format("Subjek harus minimal {0} karakter")
			},
			content : {
				required : "Mohon masukkan isi pesan",
				minlength : $.validator.format("isi pesan Anda harus minimal {0} karakter")
			}
		}
	});
	@endsection

	@section('custom-head')

	@endsection