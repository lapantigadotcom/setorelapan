@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>PRODUK LIST</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">produk-list</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            @include('errors.session')
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Product Lists &nbsp;&nbsp;&nbsp;
                                    @if(Auth::user()->hasAccess('ki-admin.product.create'))
                                        <a href="{!! route('ki-admin.product.create') !!}" class="btn btn-primary">Tambah Data </a>
                                        @endif
                                    </h3>                                    
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Gambar</th>
                                                <th>Kode</th>
                                                <th>Nama Produk</th>
                                                <th>Merk</th>
                                                <th>Status Produk</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    @if(isset($row->productImages->first()->image))
                                                        <img width="55px" height="55px" src="{!!asset($row->productImages->first()->image)!!}" />
                                                    @else
                                                        &nbsp;
                                                    @endif
                                                </td>
                                                <td>{{ $row->code }}</td>
                                                <td>{{ $row->title }}</td>
                                                <td>{{ $row->brand->name }}</td>
                                                <td>
                                                    {!! $row->ms_status_product_id==1?"<span class='label label-primary'>".$row->statusproduct->name."</span>":"<span class='label label-danger'>".$row->statusproduct->name."</span>" !!}
                                                    {!! $row->prices()->count() > 0?"":"<span class='label label-danger'>Harga Belum ada</span>" !!}
                                                    {!! $row->productImages()->count() > 0?"":"<span class='label label-danger'>Gambar Belum ada</span>" !!}
                                                </td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.product.edit'))
                                                    <a href="{!! route('ki-admin.product.edit',[$row->id]) !!}" class="fa fa-pencil-square-o"></a>
                                                @endif
                                                @if(Auth::user()->hasAccess('ki-admin.product.delete'))
                                                    <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.product.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Gambar</th>
                                                <th>Kode</th>
                                                <th>Nama Produk</th>
                                                <th>Merk</th>
                                                <th>Status Produk</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection