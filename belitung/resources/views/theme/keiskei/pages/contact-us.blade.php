@extends('theme.keiskei.app')





@section('content')

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db
" />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
">
<meta name='robots' content='INDEX, FOLLOW' />


<title>Keiskei Indonesia | Hubungi Kami</title>
<!-- breadcrumbs-area-area start -->
		 <div class="breadcrumbs-area log">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumbs-menu">
							<ul>
								<li><a href="{!! URL::to('/') !!}">Home <span>></span></a></li>
 								<li><a href="#">{!! $data['content']->title !!} <span>></span></a></li>

						</div>
					</div>
				</div>
			</div>
		 </div>
    <!-- breadcrumbs-area-area end -->
<div id="contact-page" class="container">
    	<div class="bg">

	    	<div class="row">    
				<div class="col-sm-12">    			   			
					<h2 class="title text-center">Kontak Kami <strong>Us</strong></h2>    			    				    				
					<div style="padding-bottom:80px">

					</div>
				</div>
			</div>
		

				<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center"></h2>
	    				@include('errors.session')
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				            {!! Form::open(array('route' => 'contact-us.store', 'method' => 'POST')) !!}
							{!! BootstrapForm::text('name','Nama', Auth::check()?Auth::user()->name:'') !!}
				            </div>
				            <div class="form-group col-md-6">
							{!! BootstrapForm::text('email','Email', Auth::check()?Auth::user()->email:'') !!}
				            </div>
				            <div class="form-group col-md-12">
							{!! BootstrapForm::text('subject','Judul') !!}
				            </div>
				            <div class="form-group col-md-12">
							{!! BootstrapForm::textarea('message','Pesan') !!}
				            </div>                        
				            <div class="form-group col-md-12">
								{!! Recaptcha::render() !!}
								<span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
							</div>

				            <div class="form-group col-md-12 pull-right">
							{!! Form::submit('Kirim',array('class' => 'btn btn-primary pull-right')) !!}
				            </div>
				        </form>{!! Form::close() !!}
	    			</div>
	    		</div>

	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Office:</h2>
	    				<address>		@include('errors.session')

	    					 <p> <strong>{{ $data['general']->site_title }}</strong></p>
					 <p> {!! $data['general']->address !!}</p>
					  <!--  {!! $data['general']->city->name !!}, {!! $data['general']->city->province->name !!}<br>  -->

					  <p> {!! $data['general']->telephone !!}</p>
						<p>Fax.+62-31-99102111</p>
					  <p>CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707</p>
					  <p>BBM: 55169066 / 53648437 / 2bee00db</p>

	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Social Networking</h2>
							<ul>
								<li><a href="https://www.facebook.com/KeiskeiIndonesia?ref=hl" target="_blank"><i  title="FB: keiskeiashitaba" class="fa fa-facebook-square fa-lg" style="color:#3b5998"></i></a></li>
					<li><a href="https://twitter.com/keiskeiashitaba" target="_blank"><i  title="@keiskeiashitaba" class="fa fa-twitter-square fa-lg" style="color:#00aced"></i></a></li>
					<li><a href="https://plus.google.com/101701632456013203883/posts" target="_blank"><i  title="keiskeiashitaba" class="fa fa-google-plus fa-lg" style="color:#dd4b39"></i></a></li>
					<li><a href="https://id.linkedin.com/pub/keiskei-indonesia/b7/1/b90" target="_blank"><i  title="keiskei-indonesia" class="fa fa-linkedin fa-lg" style="color:#007bb6"></i></a></li>
					<li><a href="https://instagram.com/keiskeiindonesia/" target="_blank"><i title="keiskei_indonesia" class="fa fa-instagram fa-lg" style="color:#517fa4"></i></a></li>

							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>



		</div>
</div>

 
 @endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection