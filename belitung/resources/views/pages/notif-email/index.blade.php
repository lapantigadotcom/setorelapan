@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Email</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">
          <a href="{!! route('ki-admin.notif-email.create') !!}" class="btn btn-primary btn-block margin-bottom">Compose New Email</a>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{!! route('ki-admin.notif-email.index') !!}"><i class="fa fa-envelope-o"></i> Sent</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Sent</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Subject</th>
                    <th>To</th>
                    <th>Tipe</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td></td>
                      <td><a href="{!! route('ki-admin.notif-email.show', [$row->id]) !!}">{!! $row->subject !!}</a></td>
                      <td>{!! $row->recipient->name !!}</td>
                      <td>{!! $row->group == '0' ? 'Individual' : 'Group' !!}</td>
                      <td>{!! $row->created_at !!}</td>
                      <td>
                          <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.notif-email.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Subject</th>
                    <th>To</th>
                    <th>Tipe</th>
                    <th>Time</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection