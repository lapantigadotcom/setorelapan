<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Log extends Model {

	protected $table = 'tr_logs';
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}
	public function historable()
	{
		return $this->morphTo();
	}
}
