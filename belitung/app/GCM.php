<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GCM extends Model {

	protected $table = 'ms_gcms';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('\App\User', 'ms_user_id', 'id');
    }

}
