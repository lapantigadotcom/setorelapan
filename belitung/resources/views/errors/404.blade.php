@extends('theme.keiskei.app')
 
@section('content')
<title>Opsss.....ERROR 404. Not Found !!</title>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center" style=" 
		padding-top:20%;border-radius:0;height:600px;background:#F7F7F7 url(/theme/keiskei/img/error404.jpg) no-repeat center 20%">
		<div style="color:#3e780a">
			<br><hr style="border-top:#3e780a 1px dashed"><h2>ERROR...404</h2> 
			<h5>Halaman tidak ditemukan | Silahkan kembali ke halaman sebelumnya</h5>
</div>
		</div>
	</div>
</div>
<div class="row">

 
 @endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
 

@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection