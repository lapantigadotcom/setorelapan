@extends('theme.keiskei.app')



@section('content')
 <meta content='Citra Florist Group' property='og:title'/>
<meta content='Silakan klik masing-masing banner untuk terhubung ke website jaringan kami lainnya.  Klik Next atau Previous Button dibawah untuk melihat ar...' property='og:description'/>
<meta content='https://3.bp.blogspot.com/-NlJeTFD3_2Y/VlxJvyai_pI/AAAAAAAABsg/UxVoms-Wq4s/w1200-h630-p-nu/banner-toko-bunga-surabaya-online-net.jpg' property='og:image'/>
<!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
<title>Toko Bunga Surabaya Online | Citra Florist Group</title> 
<meta name="p:domain_verify" content="05e6dcbffcea28ff89468960b463c7bc"/>
<meta name='robots' content='INDEX, FOLLOW' />
<section id="advertisement" >
		<div class="container">
		<img src="{{ asset('theme/keiskei_V2/images/banner/Banner_K-Business.jpg') }}" alt="keiskei 17 Agustus">
  
		</div> 
</section>
 
<br><br>
				<!--sidebar-->
				<div class="col-sm-3">
					<div class="left-sidebar">
  						
					 
						<div class="shipping text-center"><!--shipping-->
							<img src="{!! asset('theme/keiskei_V2/images/register-banner.png') !!}" alt="">
						</div><!--/shipping-->

						<div class="shipping text-center">
						<p>  Selalu terkoneksi dengan informasi dan berita terbaru dari kami, juga berbagai fitur menarik lainnya? Silakan download aplikasinya di gadget Anda. 
						</p>	
							<img src="{!! asset('theme/keiskei_V2/images/playstore-ic.png') !!}" alt="">
						</div>
						
					</div>
				</div>	<!--/sidebar-->


<div class="col-sm-9 padding-right">
	<div class="features_items"><!--features_items-->

											@foreach($data['products'] as $row)

		<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												@if($row->productImages()->count() > 0)
												@if(File::exists($row->productImages->first()->image))
												<img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
												@endif
												@endif
												<h2></h2>
												<p>{!! $row->title !!}</p>
												<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Lihat Produk</a>
											</div>
									</div> 
									<div class="choose">
														<ul class="nav nav-pills nav-justified">
															<li><a   style="color:#0C3121;cursor: pointer; cursor: hand; " title="Tulis Ulasan" data-toggle="modal" data-target="#{{ 'review'.$row->id }}"><i class="fa fa-edit"></i></a></li>
															<li> <a  style="color:#0C3121;cursor: pointer; cursor: hand; " title="Daftar Ulasan produk" data-toggle="modal" data-target="#{{ 'review-content-'.$row->id }}"><i class="fa fa-exchange"></i></a></li>
															<li><a title="FAQ produk" data-toggle="modal" data-target="#help"  style="color:#0C3121;cursor: pointer; cursor: hand; " onclick="productHelp(this)" data-product="{!! $row->id !!}"><i class="fa fa-question"></i></a></li>

														</ul>
									</div>
								</div>@include('theme.keiskei.include.review-content',['id' => 'review-content-'.$row->id, 'reviews' => $row->reviews ])
			@include('theme.keiskei.include.review',[ 'idP' => $row->id,'id' => 'review'.$row->id])
		</div>@endforeach 
	</div>
							
</div>
					
					 
					
				 
		<div class="row">
			<div class="col-md-12">
				{!! $data['products']->render() !!}
			</div>
		</div>
	


 @include('theme.keiskei.include.help',[ 'help' => 'Bantuan'])  
		 @endsection

@section('custom-footer')


	{!! HTML::script('theme/citraflorist/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/citraflorist/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/citraflorist/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function changeCategory(x)
	    {
	    	if(x.value==0)
	    	{
	    		window.location.replace("{!! route('product') !!}");
	    	}else
	    	{
		    	window.location.replace("{!! route('product') !!}/" + x.value + "-" + $(x).text());	    	
	    	}
	    }
	    function productHelp(x)
	    {
	    	var product_id = x.getAttribute("data-product");
	    	$.ajax(
		   	{
		   		url:"{{ route('product.getjson') }}",
		   		type: "GET",
		   		data:{
			      product_id : product_id
			    }
			}).done(function(data,status){
				if(status=='success')
			    {	
			    	console.log(data);
			    	$('#help-text').empty();
			    	$('#help-text').append(data.help);
			    }
			});
	    }
	</script>



@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}

	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection