<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class UserProfileRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$user = Auth::user();
		switch($this->method())
	    {
	        case 'GET':
	        case 'DELETE':
	        {
	            return [];
	        }
	        case 'POST':
	        {
	            return [
	                'username' => 'required|unique:ms_users',
					'email' => 'required|email|unique:ms_users',
					'ms_country_id' => 'required',
					'photo' => 'image'
	            ];
	        }
	        case 'PUT':
	        case 'PATCH':
	        {
	            return [
	                'name'	 		=> 'required',
					'email' 		=> 'required|email|unique:ms_users,email,'.$user->id,
					'telephone'		=> 'required',
					'address'		=> 'required',
					'photo' 		=> 'image'
	            ];
	        }
	        default:break;
	    }
	}

}
