<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsUserFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_user_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_user_id')->index('fk_ms_user_file_ms_users1_idx');
			$table->string('ktp', 128)->nullable();
			$table->string('siup', 128)->nullable();
			$table->string('tdp', 128)->nullable();
			$table->string('npwp', 128)->nullable();
			$table->string('akte', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_user_files');
	}

}
