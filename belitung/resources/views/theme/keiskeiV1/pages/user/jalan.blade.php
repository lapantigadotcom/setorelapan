<!DOCTYPE html>
<html style="background-color: #e4e4e4; ">
 


	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		 KeisKei Indonesia | SURAT JALAN : {!! $data['content']->code !!}
	</title>


<link rel="stylesheet" type="text/css" href="{!! asset('theme/keiskei/css/invoice.css') !!}">
{!! HTML::style('theme/keiskei/plugin/fontawesome/css/font-awesome.min.css') !!}
	{!! HTML::script('theme/keiskei/js/jquery-1.11.2.min.js') !!}
	

 <style type="text/css">
 	.label {display: inline;
padding: 0.1em 0.1em 0.2em;
font-size: 14px;
line-height: 1;
color: #FFF;
text-align: center;
white-space: nowrap;
vertical-align: baseline;
border-radius: 0.25em;}

.label-success {background-color: #444}

table .no {
  color: #FFFFFF;
  font-size: 1.6em;
  background: #444;
      box-shadow: inset 0 0 0 1000px #444;}

      table th,
table td {
  padding: 4px 8px 4px 8px;
    background: #EEEEEE;
  text-align: center;
  border-bottom: 1px solid #FFFFFF;
        box-shadow: inset 0 0 0 1000px #e1e1e1;

}

}


 </style>

</head>
<body style="height:21cm">
	@include('theme.keiskei.scripts.price-product')
	@if(Auth::user()->roles->first()->locked == '1')
	<?php 
	if($data['content']->user->roles->first()->level == '5')
		setlocale(LC_ALL, "en_US.UTF-8");
	?>
	@endif
	 <header class="clearfix">
      <div id="logo">
<a class="navbar-brand visible-lg visible-md" href="{!! URL::to('/') !!}">
	      	<img src="{!! asset('theme/keiskei/img/logo.png') !!}" class="img-responsive hidden-xs main-logo">
	      </a>      </div>
      <div id="company">
        <h2 class="name" style="text-transform:uppercase">PT. Punyakunik Maju Jaya</h2>
        <div>Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia</div>
        <div>

        <b>+62.31.99.00.6002</b>
</div>
        <div><a href="mailto:info@keiskei.co.id">info@keiskei.co.id</a></div>
      </div>
      </div>
    </header>


     <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Kepada YTH:</div>
          <h2 class="name" style="text-transform:uppercase">{!! $data['content']->user->name !!}</h2>
          <div class="address">{!! $data['content']->user->address !!}<br>
            @if($data['content']->user->roles->first()->level!='5')
                {!! $data['content']->user->city->name !!}, <br>{!! $data['content']->user->city->province->name !!} <br>
                @endif
                Telp. {!! $data['content']->user->telephone !!}&nbsp; HP: {!! $data['content']->user->mobile !!} 
 </div>
          <div class="email"><a href="mailto:{!! $data['content']->user->email !!} ">{!! $data['content']->user->email !!} </a></div>


         </div>
        <div id="invoice"><h1>LEMBAR PENGIRIMAN</h1>
          <div class="date">Tanggal Invoice: {!! date('j F Y', strtotime($data['content']->created_at)) !!}
</div>
 <div class="date"> Jasa Pengiriman:  {!! $data['content']->courier->name !!} -  {!! $data['content']->service_courier !!}


			</div>

<a href="#"  onClick="window.print()"><b> <i class="fa fa-print"></i> Cetak</b></a>
				<div class="row">
					
				
				</div>	
				</div><!-- <div style="float:right">

<img src="{!! asset('theme/keiskei/img/invoice-seal.jpg') !!}">
</div> -->
				
			</div>
			@if(!empty($data['content']->ms_payment_id))

				<div class="row">
				<div class="clearfix">
				
			
						<h2>Berdasarkan Invoice: <b>#{!! $data['content']->code !!}</b> - Paket ini berisikan :</h2>
							 
				</div>
						
									<div class="colspan">
										          

									</div>
			</div>
			@endif
			<hr>
			<div class="row">
			<div class="date">
					 
				</div> <br>
				<table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">QTY</th>
            <th class="desc">NAMA PRODUK</th>
            <th class="unit">BERAT</th>
          </tr>
        </thead>
        <tbody>

            <?php
              $total = 0;
              $weight = 0;
              ?>
              @foreach($data['content']->detailOrders as $row)
          <tr><tr>
								
			<td class="no">{!! $row->total !!}</td>

            <td class="desc">
									<h3>{!! $row->product->title !!}</h3>
										@if($row->detailAttributeProductDetailOrders->count() > 0)
										@foreach($row->detailAttributeProductDetailOrders as $val)
										<span>

											: 
											<b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b>
										</span>
										@endforeach
										@endif
									</td>
									<td class="unit">{!! $row->product->weight !!} kg</td>			</tr>
            					
            					

        					  	</tr>

								<tr>
								@endforeach
								@if($data['content']->user->roles->first()->level!='5')
								<tr>					


 

			

            
			

			</tbody>
			<tfoot>
			
			 
			@endif
			<tr>
			
			</tr>        
 			</tfoot>
			</table>

						

					<footer>

Copyright © PT. Punyakunik Maju Jaya. All Rights Reserved. info@keiskei.co.id .<span style="color:green">LEMBAR PENGIRIMAN</span>
</footer>
	</body>
	</html>