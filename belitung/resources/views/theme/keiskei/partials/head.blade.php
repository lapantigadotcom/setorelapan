{!! HTML::style('theme/citraflorist/css/bootstrap.min.css') !!}
{!! HTML::script('theme/citraflorist/css/price-range.css') !!}
{!! HTML::style('theme/citraflorist/css/animate.css') !!}
{!! HTML::style('theme/citraflorist/css/prettyPhoto.css') !!}
{!! HTML::style('theme/citraflorist/css/font-awesome.min.css') !!}
{!! HTML::style('theme/citraflorist/css/main.css') !!}
{!! HTML::style('theme/citraflorist/js/owl/owl.carousel.css') !!}
{!! HTML::style('theme/citraflorist/js/owl/owl.theme.css') !!}
{!! HTML::style('theme/citraflorist/css/tambahan.css') !!}
{!! HTML::style('theme/citraflorist/css/jquery-ui.min.css') !!}


@yield('custom-head')
 