@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Role Admin Lists &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <!-- <th>Locked</th> -->
                                                <th>Enabled</th>
                                                <!-- <th>Require File</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['admin'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $row->code }}</td>
                                                <td>{{ $row->name }}</td>
                                                <!-- <td>{{ $row->locked == 1 ? 'Yes' : 'No'  }}</td> -->
                                                <td>{{ $row->enabled == 1 ? 'Yes' : 'No'  }}</td>
                                                <!-- <td>{{ $row->require_file == 1 ? 'Yes' : 'No' }}</td> -->
                                                <td>
                                                    
                                                    <a href="{!! route('ki-admin.role.admin.edit',[$row->id]) !!}" class="fa fa-pencil-square-o">
                                                    
                                                </a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <!-- <th>Locked</th> -->
                                                <th>Enabled</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Role User Lists &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <!-- <th>Locked</th> -->
                                                <th>Enabled</th>
                                                <th>Require File</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['user'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $row->code }}</td>
                                                <td>{{ $row->name }}</td>
                                                <!-- <td>{{ $row->locked == 1 ? 'Yes' : 'No'  }}</td> -->
                                                <td>{{ $row->enabled == 1 ? 'Yes' : 'No'  }}</td>
                                                <td>{{ $row->require_file == 1 ? 'Yes' : 'No' }}</td>
                                                <td>
                                                    @if($row->locked != '1')
                                                    <a href="{!! route('ki-admin.role.edit',[$row->id]) !!}" class="fa fa-pencil-square-o">
                                                    @endif
                                                </a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <!-- <th>Locked</th> -->
                                                <th>Enabled</th>
                                                <th>Require File</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->

@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection